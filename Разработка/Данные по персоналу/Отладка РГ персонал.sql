--SELECT * 
--FROM vfactstaff S
--where Year = 2018 and Month = 4 and DAY in (1,2)
--AND SHOPiD in (7198)
--order by 6

----SELECT * FROM shops where id IN (7198)

--SELECT * FROM staffPlan

SELECT
	ShopId,
	RoleId,
	ShiftId,
	DATEPART(YEAR, DateFrom) AS Year,
	DATEPART(MONTH, DateFrom) AS Month,
	DATEPART(DAY, DateFrom) AS Day,
	Count(*) AS StaffFactCount
FROM (
	SELECT DISTINCT
		CAST(ch.date AS Date) AS DateFrom,
		desk.ShopId,
		cd.User_Id AS UserId,
		Role_id AS RoleId,
		Shift_id AS ShiftId
	FROM check_headers ch 
		INNER JOIN check_details cd
			ON cd.check_header_id = ch.id
		LEFT JOIN vDimCashDesk desk
			ON desk.CashDeskId = ch.cash_desk_id	
		LEFT JOIN StaffPlan pl
			ON CAST(ch.date AS Date) = pl.Date
				AND pl.Shop_id = desk.ShopId
				AND pl.User_id = cd.User_Id
	WHERE 
		cd.User_Id IS NOT NULL
	) fact

where DateFrom = '20180403' and shopID IN (7146)
GROUP BY 
	ShopId, RoleId, ShiftId, DateFrom	




--SELECT * FROM StaffPlan
SELECT * FROM vFactStaff where DateFrom = '20180403' and shopID IN (7146)


SELECT * FROM vFactStaff WHERE datefrom = '20180403' and shopID IN (7146)

--SELECT * FROM shops where id = 7146