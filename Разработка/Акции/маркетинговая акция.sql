--Таблица: Документ.МаркетинговаяАкция, Имя таблицы хранения: Document204, Назначение: Основная
--   Ссылка (ID)
--   ВерсияДанных (Version)
--   ПометкаУдаления (Marked)
--   Дата (Date_Time) (NumberPrefix)
--   Номер (Number)
--   Проведен (Posted)
--   НаименованиеАкции (Fld3949)
--   Описание (Fld3950)
--   ДатаНачалаДействия (Fld3951)
--   ДатаОкончанияДействия (Fld3952)
--   Комментарий (Fld3953)
--   Ответственный (Fld3954)
--   ДляВсехМагазинов (Fld3955)
--   ДляВсехМагазиновОдноРасписаниеСкидок (Fld3956)
--   ОбластьДанныхОсновныеДанные (Fld516)
----
--Таблица: Документ.МаркетинговаяАкция.СкидкиНаценки, Имя таблицы хранения: Document204.VT3960, Назначение: ТабличнаяЧасть
--   НомерСтроки (LineNo3961)
--   ДатаНачала (Fld3962)
--   ДатаОкончания (Fld3963)
--   Магазин (Fld3964)
--   СкидкаНаценка (Fld3965)
		DECLARE @Now datetime = GETDATE()

SELECT 
--*,
	m._Fld3949 AS Name,
	CASE 
		WHEN YEAR(CAST(m._Fld3951 AS datetime)) < 3900 
		THEN @Now
		ELSE DATEADD(YEAR, -2000, CAST(m._Fld3951 AS datetime)) 
	END AS DateFrom,
	CASE 
		WHEN YEAR(CAST(_Fld3952 AS datetime)) < 3900 
		THEN @Now
		ELSE DATEADD(YEAR, -2000, CAST(m._Fld3952 AS datetime)) 
	END AS DateTo,
	--@Now AS created_at,
	--m._IDRRef
	------------------
	'-',
	dd._LineNo3961,
	dd._Fld3964Rref AS PhopId,
	sh.name AS ShpoName,
	CASE 
		WHEN YEAR(CAST(dd._Fld3962 AS datetime)) < 3900 
		THEN @Now
		ELSE DATEADD(YEAR, -2000, CAST(dd._Fld3962 AS datetime)) 
	END AS DateFrom,
	CASE 
		WHEN YEAR(CAST(dd._Fld3963 AS datetime)) < 3900 
		THEN @Now
		ELSE DATEADD(YEAR, -2000, CAST(dd._Fld3963 AS datetime)) 
	END AS DateTo,
	'-',
	--dd._fld3965Rref,
	rr._Description,
	CAST(rr._Fld2631 AS float) AS DiscountValue
	--rr._IDRRef

FROM X.rt_smart.[dbo].[_Document204] m
	INNER JOIN X.rt_smart.[dbo]._Document204_VT3960 dd
		ON m._IDRRef = dd._Document204_IDRRef
	LEFT JOIN X.rt_smart.[dbo].[_Reference141] rr
		ON rr._IDRREF = dd._fld3965Rref
	LEFT JOIN shops sh 
		ON sh.row_id = dd._Fld3964Rref
--where 
--where m._Fld3949 = 'Футболка в подарок'
--m._Fld3951 <> dd._Fld3962
--		SELECT * FROM X.rt_smart.[dbo]._Document204_VT3960
--SELECT * FROM shops where row_id IN (0x827336FC8BD1B53211E68ADAEAD9A116, 0x92D3005056016EAA11E69E0928110422, 0x9A0E005056016EAA11E6CD09518A664E)
--SELECT * FROM X.rt_smart.[dbo].[_Document204]
--SELECT * FROM  X.rt_smart.[dbo].[_Reference141] r WHERE r._Description = 'Футболок в подарок'

--Market_actions - справочник акций _Document204 
--Discount - справочник скидок  _Reference141
--Market_action_Discount - связи акции со скидкой _Document204_VT3960

--SELECT * FROM X.rt_smart.[dbo]._Document204_VT3960 dd
--SELECT * FROM  X.rt_smart.[dbo].[_Reference141] r WHERE _IDRREF = 0x80FD00505601926011E7EAFF0D4C5D9A
	
----------------------------------
		--Таблица: Справочник.СкидкиНаценки, Имя таблицы хранения: Reference141, Назначение: Основная
		--   Ссылка (ID)
		--   ВерсияДанных (Version)
		--   ПометкаУдаления (Marked)
		--   ИмяПредопределенныхДанных (PredefinedID)
		--   Родитель (ParentID)
		--   ЭтоГруппа (Folder)
		--   Наименование (Description)
		--   ВалютаПредоставления (Fld2628)
		--   ВариантСовместногоПрименения (Fld2629)
		--   ВидЦены (Fld2630)
		--   ЗначениеСкидкиНаценки (Fld2631)
		--   ОбластьПредоставления (Fld2632)
		--   РеквизитДопУпорядочивания (Fld2633)
		--   СегментНоменклатурыПредоставления (Fld2634)
		--   СпособПредоставления (Fld2635)
		--   СтатусДействия (Fld2636)
		--   Управляемая (Fld2637)
		--   ПодарокИзСписка (Fld2638)
		--   ПодарокИзКорзиныПокупателя (Fld2639)
		--   КоличествоПодарковИзКорзиныПокупателя (Fld2640)
		--   УчитыватьПодарокКакПродажу (Fld2641)
		--   ТекстСообщения (Fld2642)
		--   МоментВыдачиСообщения (Fld2643)
		--   БонуснаяПрограммаЛояльности (Fld2644)
		--   ПериодДействия (Fld2645)
		--   КоличествоПериодовДействия (Fld2646)
		--   ПериодОтсрочкиНачалаДействия (Fld2647)
		--   КоличествоПериодовОтсрочкиНачалаДействия (Fld2648)
		--   КратноКоличествуУсловий (Fld2649)
		--   ОграничениеРазмераПодчиненныхСкидок (Fld2650)
		--   ВнешняяОбработка (Fld2651)
		--   ПараметрыВнешнейОбработки (Fld2652)
		--   ОбластьДанныхОсновныеДанные (Fld516)
--SELECT * FROM X.rt_smart.[dbo]._Document204_VT3960 dd
--SELECT * FROM  X.rt_smart.[dbo].[_Reference141] r WHERE _IDRREF = 'Футболок в подарок'

--return
--SELECT * FROM check_headers where id = 1028
--SELECT * FROM check_details where check_header_id = 1028
--SELECT * FROM check_discounts where check_header_id = 1028 --1028

  --SELECT * FROM [discounts]
--  SELECT * FROM check_details

--  SELECT	
--  *
--			--_Description,
--			--CAST(_Fld2631 AS float),
--			--@Now,
--			--_IDRRef
--		FROM X.rt_smart.[dbo].[_Reference141] r
--		--WHERE 
----			_Folder = 1
--			--AND r._IDRRef NOT IN (SELECT ISNULL(row_id, 0) FROM discounts)

--SELECT * FROM X.rt_smart.[dbo].[_Document204] r
--where _fld3949 LIKE '%7000%'

--SELECT _Fld2645Rref, * FROM X.rt_smart.[dbo].[_Reference141] r
--where _description LIKE '%7000%'
----order by 1

--SELECT * FROM X.rt_smart.[dbo]._Document204_VT3960

--Таблица: Документ.МаркетинговаяАкция, Имя таблицы хранения: Document204, Назначение: Основная
--- поля: 
--   Ссылка (ID)
--   ВерсияДанных (Version)
--   ПометкаУдаления (Marked)
--   Дата (Date_Time) (NumberPrefix)
--   Номер (Number)
--   Проведен (Posted)
--   НаименованиеАкции (Fld3949)
--   Описание (Fld3950)
--   ДатаНачалаДействия (Fld3951)
--   ДатаОкончанияДействия (Fld3952)
--   Комментарий (Fld3953)
--   Ответственный (Fld3954)
--   ДляВсехМагазинов (Fld3955)
--   ДляВсехМагазиновОдноРасписаниеСкидок (Fld3956)
--   ОбластьДанныхОсновныеДанные (Fld516)
--- индексы: 
--   ByDocNumPrefix
--      Номер + Ссылка (NumberPrefix + Number + ID)
--   ByDocNum
--      Номер + Ссылка (Number + ID)
--   ByDocDate
--      Дата + Ссылка + ПометкаУдаления (Date_Time + ID + Marked)

--Таблица: Документ.МаркетинговаяАкция.Магазины, Имя таблицы хранения: Document204.VT3957, Назначение: ТабличнаяЧасть
--- поля: 
--   НомерСтроки (LineNo3958)
--   Магазин (Fld3959)
--- индексы: 

--Таблица: Документ.МаркетинговаяАкция.СкидкиНаценки, Имя таблицы хранения: Document204.VT3960, Назначение: ТабличнаяЧасть
--- поля: 
--   НомерСтроки (LineNo3961)
--   ДатаНачала (Fld3962)
--   ДатаОкончания (Fld3963)
--   Магазин (Fld3964)
--   СкидкаНаценка (Fld3965)
--- индексы: 

--Таблица: Документ.МаркетинговаяАкция.НаборыЗначенийДоступа, Имя таблицы хранения: Document204.VT3966, Назначение: ТабличнаяЧасть
--- поля: 
--   НомерСтроки (LineNo3967)
--   НомерНабора (Fld3968)
--   ЗначениеДоступа (Fld3969)
--   Чтение (Fld3970)
--   Изменение (Fld3971)
--   Уточнение (Fld3972)
--- индексы: 

----   Таблица: Документ.ЧекККМ.Оплата, Имя таблицы хранения: Document273.VT5869, Назначение: ТабличнаяЧасть
----- поля: 
----   НомерСтроки (LineNo5870)
----   ВидОплаты (Fld5871)
----   ЭквайринговыйТерминал (Fld5872)
----   Сумма (Fld5873)
----   ПроцентКомиссии (Fld5874)
----   СуммаКомиссии (Fld5875)
----   СсылочныйНомер (Fld5876)
----   НомерЧекаЭТ (Fld5877)
----   НомерПлатежнойКарты (Fld5878)
----   ДанныеПереданыВБанк (Fld5879)
----   СуммаБонусовВСкидках (Fld5880)
----   КоличествоБонусов (Fld5881)
----   КоличествоБонусовВСкидках (Fld5882)
----   БонуснаяПрограммаЛояльности (Fld5883)
----   ДоговорПлатежногоАгента (Fld5884)
----   КлючСвязиОплаты (Fld5885)
----   _КредитныйДоговор (Fld10813)
----- индексы: 


----Таблица: Документ.ЧекККМ.СкидкиНаценки, Имя таблицы хранения: Document273.VT5909, Назначение: ТабличнаяЧасть
----- поля: 
----   НомерСтроки (LineNo5910)
----   КлючСвязи (Fld5911)
----   Сумма (Fld5912)
----   СкидкаНаценка (Fld5913)
----   ОграниченаМинимальнойЦеной (Fld5914)
----- индексы: 

