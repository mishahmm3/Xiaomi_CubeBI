﻿SELECT	top 100
			cd.id AS cash_desk_id,
			c.id AS card_id,
			ov.CheckOperationVidId,
			_Number AS Number,
			DATEADD(YEAR, -2000, CAST(_Date_Time as datetime)) AS [Date], -- Дата - 2000 лет
			st.CheckStatusId,
			_Fld5816,
			_Fld5817,
			--@Now AS created_at,
			_IDRRef AS row_id
		FROM X.rt_smart.[dbo].[_Document273] d
			INNER JOIN cash_desks cd 
				ON d._Fld5813RRef = cd.row_id
			LEFT JOIN cards 
				c ON d._Fld5811RRef = c.row_id
			-- mma здесь вроде distinct не нужен
			LEFT JOIN check_headers head 
				ON head.row_id = d._IDRRef
			LEFT JOIN CheckStatus st 
				ON st.row_id = d._Fld5825Rref
			LEFT JOIN CheckOperationVid ov 
				ON ov.row_id = d._Fld5809Rref
		WHERE
			d._Posted <> 0
			AND head.row_id IS NULL


SELECT TOP 1000 * FROM X.rt_smart.[dbo].[_Document273] d

SELECT	top 100
			cd.id AS cash_desk_id,
			c.id AS card_id,
			ov.CheckOperationVidId,
			_Number AS Number,
			DATEADD(YEAR, -2000, CAST(_Date_Time as datetime)) AS [Date], -- Дата - 2000 лет
			st.CheckStatusId,
			_Fld5816,
			_Fld5817,
			--@Now AS created_at,
			_IDRRef AS row_id
		FROM Mistex.rt_mixtech.[dbo].[_Document273] d
			INNER JOIN cash_desks cd 
				ON d._Fld5813RRef = cd.row_id
			LEFT JOIN cards 
				c ON d._Fld5811RRef = c.row_id
			-- mma здесь вроде distinct не нужен
			LEFT JOIN check_headers head 
				ON head.row_id = d._IDRRef
			LEFT JOIN CheckStatus st 
				ON st.row_id = d._Fld5825Rref
			LEFT JOIN CheckOperationVid ov 
				ON ov.row_id = d._Fld5809Rref
		WHERE
			d._Posted <> 0
			AND head.row_id IS NULL


SELECT TOP 1000 * FROM Mistex.rt_mixtech.[dbo].[_Document273] d