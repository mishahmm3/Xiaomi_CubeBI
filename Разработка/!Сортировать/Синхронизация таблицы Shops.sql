--BEGIN TRAN


update res 
set 
	res.updated_at = GETDATE(),
	res.City_Id = CityId,
	res.Shop_Type_Id = ShopTypeId,
	res.Division_Id = DivisionId

FROM [Xiaomi-BI_Prod].[dbo].[shops] res
INNER JOIN 
(SELECT 
	res.Id AS Id,
	cityRes.id AS CityId, 
	stRes.Id AS ShopTypeId,
	dRes.Id AS DivisionId
FROM [Xiaomi-BI_Prod].[dbo].[shops] res
	LEFT JOIN [Xiaomi-BI].[dbo].[shops] cur
		ON cur.name = res.name
			AND cur.company_id IS NOT NULL
	--
	LEFT JOIN [Xiaomi-BI].[dbo].[cities] city
		ON city.id = cur.city_id
	LEFT JOIN [Xiaomi-BI_Prod].[dbo].[cities] cityRes
		ON cityRes.Name = city.Name
	--
	LEFT JOIN [Xiaomi-BI].[dbo].[shop_types] st
		ON st.id = cur.shop_type_id
	LEFT JOIN [Xiaomi-BI_Prod].[dbo].[shop_types] stRes
		ON stRes.Name = st.Name
	LEFT JOIN [Xiaomi-BI].[dbo].[divisions] d
		ON d.id = cur.[division_id]
	LEFT JOIN [Xiaomi-BI_Prod].[dbo].[divisions] dRes
		ON dRes.Name = d.Name
) d
ON d.Id = res.Id 

--ROLLBACK TRAN
	--SELECT * FROM [Xiaomi-BI].[dbo].[shops] where Name LIKE '%66/003%'
	--SELECT * FROM [Xiaomi-BI_Prod].[dbo].[shops] where Name LIKE '%66/003%'



