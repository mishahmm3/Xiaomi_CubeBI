SELECT 
 date, summa, *
 FROM CheckPayment p 
	INNER JOIN check_headers h on h.id = p.check_header_id
where 
--Product_Id = 12
--and date < '2017-09-17 18:00:00'
--and
 h.id = 2275113
order by 1, 2

--SELECT 
-- date, summa, *
--FROM vFactCheckPayment where ProductId = 12
--and date < '2017-09-17 18:00:00'
--and checkheaderid = 2275113
--order by 1, 2

	SELECT 
		cp.CheckPaymentId, 
		DATEADD(HOUR, DATEDIFF(HOUR, '', ch.[date]), '') as [date],
		DATEPART(MINUTE, ch.[date]) AS DateMinuteId,
		ch.Id AS DocumentId, 
		DimDisc.DimDiscountId,
		cp.product_id AS ProductId,
		det.operation_id,
		ch.cash_desk_id,
		ch.card_id,
		cp.PaymentVidId,
		ch.CheckOperationVidId,
		det.SellerId,
		cp.Summa,
		cp.CommisionSumma,
		cp.CommisionPercent,
		d.amount AS DiscountSumma,
		cp.BonusSumma,
		ch.id AS CheckHeaderId,
		ch.row_id AS CheckHeaderRowId
	FROM check_headers ch WITH(NOLOCK) 
		INNER JOIN CheckPayment cp WITH(NOLOCK) 
			ON cp.check_header_id = ch.id
		LEFT JOIN check_discounts d WITH(NOLOCK) 
			ON d.check_header_id = cp.check_header_id
				AND d.product_id = cp.product_id
		LEFT JOIN cash_desks cd
			ON cd.id = ch.cash_desk_id
		LEFT JOIN vDimDiscount DimDisc
			ON DimDisc.DiscountId = d.discount_id 
				AND DimDisc.ShopId = cd.shop_id
		LEFT JOIN (
			SELECT 
				DISTINCT 
				check_header_id, operation_id, MAX(user_id) AS SellerId 
			FROM check_details WITH(NOLOCK) 
			GROUP BY 
				check_header_id, operation_id
		) det
			ON  det.check_header_id = ch.id
WHERE ch.id = 2275113
--and cp.product_id = 12
SELECT * FROM CheckPayment where check_header_id = 2275113

SELECT 
*			FROM check_details WITH(NOLOCK) 
WHERE check_header_id = 2275113

--0x9FE3F40669EE2C4811E79AEC1E16AA7E	3	2	��18-000377 -- 2017-09-16 18:16:42.000
SELECT * FROM products where id IN (12, 325, 1805)
--SELECT 
--				DISTINCT 
--				check_header_id, operation_id, MAX(user_id) AS SellerId 
--			FROM check_details WITH(NOLOCK) 
--WHERE check_header_id = 2275113
--			GROUP BY 
--				check_header_id, operation_id
