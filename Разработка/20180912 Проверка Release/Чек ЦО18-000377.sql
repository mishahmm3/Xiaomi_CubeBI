﻿-------------------------------------
-- Чек ЦО18-000377
-------------------------------------
IF 2 = 1
BEGIN
	DECLARE @RowId binary(16) = 0x9FE3F40669EE2C4811E79AEC1E16AA7E  
	DECLARE @Id int = (SELECT id FROM check_headers WHERE row_id = @RowId)


	SELECT 'Чеки', * FROM vFactCheck where checkheaderrowId = @RowId
	SELECT 'Платежи по чекам', * FROM vFactCheckPayment WHERE checkHeaderRowId  = @RowId

	--SELECT 'детали ', * FROM check_details where  check_header_id = @Id
	--SELECT 'платеж', * FROM CheckPayment where  check_header_id = @Id
END

-------------------------------------
-- Чек ЦО13-004997
-------------------------------------
IF 1 = 1
BEGIN
	DECLARE @RowId2 binary(16) = 0xB7FF1C1B0DB421EC11E84AE4DA443661  
	DECLARE @Id2 int = (SELECT id FROM check_headers WHERE row_id = @RowId2)

	SELECT 'Скидки', * FROM vFactCheckDiscount where checkheaderrowId = @RowId2

	SELECT 'Чеки', * FROM vFactCheck where checkheaderrowId = @RowId2
	SELECT 'Платежи по чекам', * FROM vFactCheckPayment WHERE checkHeaderRowId  = @RowId2
--SELECT * FROM vDimDocument where DocumentId = 3035288
	--SELECT 'детали ', * FROM check_details where  check_header_id = @Id2
	----SELECT 'платеж', * FROM CheckPayment where  check_header_id = @Id2

	--SELECT * FROM  X.rt_smart.[dbo].[_Document273_VT5909] WHERE _Document273_IDRRef = @RowId2	 

		--   Сумма (Fld5858)
		--   СуммаАвтоматическойСкидки (Fld5859)
		--   СуммаНДС (Fld5860)
		--   СуммаРучнойСкидки (Fld5861)
		--   СуммаСкидкиОплатыБонусом (Fld5862)
		--   Упаковка (Fld5863)
		--   Характеристика (Fld5864)
		--   Цена (Fld5865)
		--   Удалитьор_ЗаказПокупателя (Fld5866)
		--   Штрихкод (Fld5867)
		--   ор_СуммаСкидкиБС (Fld5868)
		--   _НомерСтраховки (Fld10708)

	--SELECT 'детали без возвратов',
	--	_Document273_IDRRef, 
	--	_LineNo5840 номерстроки, 
	--	_Fld5845 AS Количество, 
	--	_Fld5865 AS Цена, 
	--	_Fld5858 AS Сумма, 
	--	_Fld5859 AS СуммаАвтоматическойСкидки, 
	--	_Fld5861 AS СуммаРучнойСкидки, 
	--	_Fld5862 AS СуммаСкидкиОплатыБонусом, 
	--	_Fld5860 AS СуммаНДС, 
	--	_Fld5848RRef, _Fld5849RRef
	--FROM X.rt_smart.[dbo].[_Document273_VT5839] data
	--where _Document273_IDRRef	= 	0xB7FF1C1B0DB421EC11E84AE4DA443661	

END

--		SELECT 'детали без возвратов',
--			_Document273_IDRRef, _LineNo5840, _Fld5845, _Fld5865, _Fld5858, _Fld5859, _Fld5861, _Fld5862, _Fld5860, _Fld5848RRef, _Fld5849RRef
--		FROM X.rt_smart.[dbo].[_Document273_VT5839] data
--where _Document273_IDRRef	= 	0x9FE3F40669EE2C4811E79AEC1E16AA7E	

--SELECT 'возвраты', * 
--		FROM X.rt_smart.[dbo].[_Document187_VT3496] v
--				where  v._Fld3509RRef = 0xB7FF1C1B0DB421EC11E84AE4DA443661	



----Таблица: Документ.ЧекККМ.Товары, Имя таблицы хранения: Document273.VT5839, Назначение: ТабличнаяЧасть

----Таблица: Документ.ВозвратТоваровОтПокупателя.Товары, Имя таблицы хранения: Document187.VT3496, Назначение: ТабличнаяЧасть


----Таблица: Документ.ЧекККМ.Оплата, Имя таблицы хранения: Document273.VT5869, Назначение: ТабличнаяЧасть


