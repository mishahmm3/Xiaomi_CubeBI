--SELECT * FROM calendar
--SELECT * FROM vFactBonusPoint
--where DATEFROM = '20190606'



SELECT * FROM calendar


DECLARE @Dt datetime = '20190101'

while @Dt < '20200101'
BEGIN
SET @Dt = DATEADD(Hour, 1, @dt)

INSERT INTO calendar
	SELECT 
	@Dt, 
	DATEPART(YEAR, @Dt),
	DATEPART(Quarter, @Dt),
	DATEPART(Month, @Dt),
	DATEPART(Week, @Dt),
	DATEPART(WeekDay, @Dt),
	DATEPART(Day, @Dt),
	DATEPART(Hour, @Dt)
END