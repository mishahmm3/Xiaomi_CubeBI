--1)      РегистрНакопления.ДенежныеСредстваНаличные – Ведение учёта по кассе (сейф)
		--Таблица: РегистрНакопления.ДенежныеСредстваНаличные, Имя таблицы хранения: AccumRg8749, Назначение: Основная
		--   Период (Period)
		--   Регистратор (Recorder)
		--   НомерСтроки (LineNo)
		--   Активность (Active)
		--   ВидДвижения (RecordKind)
		--   Организация (Fld8750)
		--   Магазин (Fld8751)
		--   Касса (Fld8752)
		--   ДоговорПлатежногоАгента (Fld8753)
		--   Сумма (Fld8754)
		--   СтатьяДвиженияДенежныхСредств (Fld8755)
		--   ОбластьДанныхОсновныеДанные (Fld516)
		-- Получить вид операции по регистру РегистрНакопления.ДенежныеСредстваНаличные можете из справочника СтатьяДвиженияДенежныхСредств, в базе: _Fld8755RRef
		--Таблица: Справочник.СтатьиДвиженияДенежныхСредств, Имя таблицы хранения: Reference149, Назначение: Основная
		SELECT 
			DATEADD(YEAR, -2000, CAST(_Period as date)) AS DateFrom, -- Дата - 2000 лет
			sh.Id AS  ShopId,
			--SUM(_Fld8754) AS RemainsValue,
			*
			--vid._Code,
			--vid._Description,
			--sh.Name AS ShopName,
			--data._RecorderRRef AS Remains_row_id
		FROM X.rt_smart.[dbo].[_AccumRg8749] data
			LEFT JOIN shops sh
				ON sh.row_id = data._Fld8751RRef
			LEFT JOIN X.rt_smart.[dbo].[_Reference149] vid
				ON vid._IDRRef = data._Fld8755RRef
WHERE
sh.Id = 7307	
AND DATEADD(YEAR, -2000, CAST(_Period as date)) >= '20190203' AND DATEADD(YEAR, -2000, CAST(_Period as date)) <= '20190210'
		--GROUP BY 
		--	DATEADD(YEAR, -2000, CAST(_Period as date)), sh.Id

		--Таблица: РегистрНакопления.ДенежныеСредстваККМ, Имя таблицы хранения: AccumRg8735, Назначение: Основная
		--- поля: 
		--   Период (Period)
		--   Регистратор (Recorder)
		--   НомерСтроки (LineNo)
		--   Активность (Active)
		--   ВидДвижения (RecordKind)
		--   КассаККМ (Fld8736)
		--   ДоговорПлатежногоАгента (Fld8737)
		--   Сумма (Fld8738)
		SELECT 
			DATEADD(YEAR, -2000, CAST(_Period as date)) AS DateFrom, -- Дата - 2000 лет
			cd.ShopId,
			SUM(_Fld8738) AS RemainsValue
		
		FROM X.rt_smart.[dbo]._AccumRg8735 d
			LEFT JOIN vDimCashDesk cd
				ON cd.row_id = 	_Fld8736RRef
		WHERE 
			_RecordKind = 1
and cd.ShopId = 7307	
AND DATEADD(YEAR, -2000, CAST(_Period as date)) >= '20190203' AND DATEADD(YEAR, -2000, CAST(_Period as date)) <= '20190210'

		GROUP BY 
			DATEADD(YEAR, -2000, CAST(_Period as date)), cd.ShopId

SELECT * FROM vFactShopEncashment
where shopId = 7307
and Year = 2019 and month = 2
--SELECT * FROM vDimShop where shopname LIKE '%Магазин № 78/005 (ТЦ Мега %'
SELECT * FROM ShopEncashment
where shopId = 7307
and Datefrom >= '20190201'
