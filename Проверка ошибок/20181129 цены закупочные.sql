	SELECT 
	ov.Code,
	det.[count],
	cv.value, 
det.[count] * CASE WHEN ov.Code = '0' THEN 1 ELSE -1 END * ISNULL(cv.value, 0) AS PurchaseCost,

		-- Измерения
		det.id, 
		CAST(head.date AS date) AS DateOnly,
		DATEADD(HOUR, DATEDIFF(HOUR, '', head.[date]), '') AS [date],
		DATEPART(MINUTE, head.[date]) AS DateMinuteId
		--head.Id AS DocumentId, 
		--head.CheckStatusId,
		--head.CheckOperationVidId,
		--head.seller_id AS SellerId,
		--head.cash_desk_id,
		--d.shop_id AS ShopId,
		--head.card_id,
		--det.product_id, 
		--det.operation_id,
		---- Основные показатели 
		--det.[count] * CASE WHEN ov.Code = '0' THEN 1 ELSE -1 END AS SellVolume, 
		--det.Price AS SellPrice,
		--det.[count] * det.Price * CASE WHEN ov.Code = '0' THEN 1 ELSE -1 END AS SellCostWithoutDiscount,
		--det.amount * CASE WHEN ov.Code = '0' THEN 1 ELSE -1 END AS SellCost,
		--det.discount_auto AS DiscountAuto,
		--det.discount_manual AS DiscountManual,
		--det.discount_auto + det.discount_manual AS DiscountTotal,
		--det.bonus,
		--det.nds,
		---- Маржа = Продажа - закупка
		--det.amount - det.[count] * CASE WHEN ov.Code = '0' THEN 1 ELSE -1 END * ISNULL(cv.value, 0) AS MarginCost,
		---- Закупка 
		--det.[count] * CASE WHEN ov.Code = '0' THEN 1 ELSE -1 END * ISNULL(cv.value, 0) AS PurchaseCost,
		--ISNULL(cv.value, 0) AS PurchasePrice,
		---- Оборачиваемость
		--rem.RemainsCount,
		--CASE WHEN rem.RemainsCount != 0 
		--	THEN CAST(det.[count] AS float) / rem.RemainsCount 
		--	ELSE NULL 
		--END AS RemainsDivSellCount,
		--CASE WHEN (rem.RemainsCount * ISNULL(cv.value, 0)) != 0 
		--	THEN det.amount / (rem.RemainsCount * ISNULL(cv.value, 0))
		--	ELSE NULL 
		--END AS RemainsDivSellCost,
		--CASE WHEN (rem.RemainsCount * ISNULL(cv.value, 0)) != 0 
		--	THEN (det.amount - discount_auto - discount_manual) / (rem.RemainsCount * ISNULL(cv.value, 0))
		--	ELSE NULL 
		--END AS RemainsDivSellCostDiscount,
		--head.change_number, 
		--head.check_number, 
		--head.row_id AS CheckHeaderRowId,
		--det.row_id AS CheckDetailRowId
	FROM check_headers head WITH(NOLOCK) 
		INNER JOIN check_details det WITH(NOLOCK) 
			ON det.check_header_id = head.id
		LEFT JOIN dbo.CheckOperationVid ov WITH(NOLOCK) 
			ON ov.CheckOperationVidId = head.CheckOperationVidId
		LEFT JOIN dbo.cash_desks d WITH(NOLOCK) 
			ON head.cash_desk_id = d.id
		LEFT JOIN product_cost_values cv WITH(NOLOCK) 
			ON det.product_id = cv.product_id 
				AND head.[date] BETWEEN cv.date_from AND cv.date_to
		-- связь с остатками
		LEFT JOIN (
			SELECT
				res.[Date] AS DateFrom,
				res.product_id,
				wh.shop_id,
				SUM(res.count) AS RemainsCount 
			FROM product_calc_remains res WITH(NOLOCK) 
				LEFT JOIN warehouses wh WITH(NOLOCK) 
					ON res.warehouse_id = wh.id
			WHERE 
				res.count <> 0
			GROUP BY
				res.[Date], res.product_id, wh.shop_id
		) rem
			ON rem.DateFrom = CAST(head.[date] AS Date)
				AND rem.shop_id = d.shop_id
				AND rem.product_id = det.product_id
WHERE 
head.date >= '20180630'
and head.date <= '20180702'
order by head.date


select * from product_cost_values where date_from >= '20180330'


SELECT top 10 * FROM  X.rt_smart.[dbo].[_InfoRg8665]

--SELECT top 1 * FROM Mistex.rt_mixtech.[dbo].[_InfoRg8665]
--select * from product_cost_values where value = 10071.60