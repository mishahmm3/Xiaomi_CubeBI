﻿exec [Parser_FileLoad] 
@FileTypeCode = 'ProductGroup', 
@FileName = 'Группы продуктов.csv', @OriginalFileName = 'Группы продуктов.csv', @Path = '', @Description = '',
@Content  = 'Группа;Номенклатура
Xiaomi Mi 6 128GB;Смартфон Xiaomi Mi 6 Blue 128GB
Xiaomi Mi 6 64GB;Смартфон Xiaomi Mi 6 Black 64GB
Xiaomi Mi A1 32GB;Смартфон Xiaomi Mi A1 Black 32GB
Xiaomi Mi A1 32GB;Смартфон Xiaomi Mi A1 Gold 32GB
Xiaomi Mi A1 32GB;Смартфон Xiaomi Mi A1 Red 32GB
Xiaomi Mi A1 64GB;Смартфон Xiaomi Mi A1 Black 64GB
Xiaomi Mi A1 64GB;Смартфон Xiaomi Mi A1 Gold 64GB
Xiaomi Mi A1 64GB;Смартфон Xiaomi Mi A1 Red 64GB
Xiaomi Mi A1 64GB;Смартфон Xiaomi Mi A1 Rose Gold 64GB
Xiaomi Mi MIX 2 128GB;Смартфон Xiaomi Mi MIX 2 Special Edition White 128GB
Xiaomi Mi MIX 2 64GB;Смартфон Xiaomi Mi MIX 2 Black 64GB
Xiaomi Mi MIX 2s 64GB;Смартфон Xiaomi Mi MIX 2s Black 64GB
Xiaomi Mi Note 2 64GB;Смартфон Xiaomi Mi Note 2 Black 64GB
Xiaomi Mi Note 2 64GB;Смартфон Xiaomi Mi Note 2 Silver Black 64GB
Xiaomi Redmi 4A 16GB;Смартфон Xiaomi Redmi 4A Gold 16GB
Xiaomi Redmi 4A 16GB;Смартфон Xiaomi Redmi 4A Gray 16GB
Xiaomi Redmi 4A 16GB;Смартфон Xiaomi Redmi 4A Rose Gold 16GB
Xiaomi Redmi 4A 32GB;Смартфон Xiaomi Redmi 4A Gold 32GB
Xiaomi Redmi 4A 32GB;Смартфон Xiaomi Redmi 4A Gray 32GB
Xiaomi Redmi 4X 16GB;Смартфон Xiaomi Redmi 4X Black 16GB
Xiaomi Redmi 4X 16GB;Смартфон Xiaomi Redmi 4X Gold 16GB
Xiaomi Redmi 4X 16GB;Смартфон Xiaomi Redmi 4X Pink 16GB
Xiaomi Redmi 4X 32GB;Смартфон Xiaomi Redmi 4X Black 32GB
Xiaomi Redmi 4X 32GB;Смартфон Xiaomi Redmi 4X Gold 32GB
Xiaomi Redmi 5 16GB;Смартфон Xiaomi Redmi 5 16GB Black
Xiaomi Redmi 5 16GB;Смартфон Xiaomi Redmi 5 16GB Blue
Xiaomi Redmi 5 16GB;Смартфон Xiaomi Redmi 5 16GB Gold
Xiaomi Redmi 5 32GB;Смартфон Xiaomi Redmi 5 32GB Black
Xiaomi Redmi 5 32GB;Смартфон Xiaomi Redmi 5 32GB Blue
Xiaomi Redmi 5 32GB;Смартфон Xiaomi Redmi 5 32GB Gold
Xiaomi Redmi 5 Plus 32GB;Смартфон Xiaomi Redmi 5 Plus 32GB Black
Xiaomi Redmi 5 Plus 32GB;Смартфон Xiaomi Redmi 5 Plus 32GB Gold
Xiaomi Redmi 5 Plus 64GB;Смартфон Xiaomi Redmi 5 Plus 64GB Black
Xiaomi Redmi 5 Plus 64GB;Смартфон Xiaomi Redmi 5 Plus 64GB Gold
Xiaomi Redmi 5A 16GB;Смартфон Xiaomi Redmi 5A 16GB Gold
Xiaomi Redmi 5A 16GB;Смартфон Xiaomi Redmi 5A 16GB Gray
Xiaomi Redmi 5A 16GB;Смартфон Xiaomi Redmi 5A 16GB Rose Gold
Xiaomi Redmi Note 4 32GB;Смартфон Xiaomi Redmi Note 4 Black 32GB
Xiaomi Redmi Note 4 32GB;Смартфон Xiaomi Redmi Note 4 Gold 32GB
Xiaomi Redmi Note 4 32GB;Смартфон Xiaomi Redmi Note 4 Gray 32GB
Xiaomi Redmi Note 4 64GB;Смартфон Xiaomi Redmi Note 4 Black 64GB(QS)
Xiaomi Redmi Note 4 64GB;Смартфон Xiaomi Redmi Note 4 Gold 64GB(QS)
Xiaomi Redmi Note 4 64GB;Смартфон Xiaomi Redmi Note 4 Gray 64GB(QS)
Xiaomi Redmi Note 4 64GB;Смартфон Xiaomi Redmi Note 4 Silver 64GB
Xiaomi Redmi Note 4X 32GB;Смартфон Xiaomi Redmi Note 4X Gold 32GB(QS)
Xiaomi Redmi Note 5 32GB;Смартфон Xiaomi Redmi Note 5 32GB Black
Xiaomi Redmi Note 5 32GB;Смартфон Xiaomi Redmi Note 5 32GB Gold
Xiaomi Redmi Note 5 64GB;Смартфон Xiaomi Redmi Note 5 64GB Black
Xiaomi Redmi Note 5 64GB;Смартфон Xiaomi Redmi Note 5 64GB Blue
Xiaomi Redmi Note 5 64GB;Смартфон Xiaomi Redmi Note 5 64GB Gold
Xiaomi Redmi Note 5A 16GB;Смартфон Xiaomi Redmi Note 5A 16GB Gold
Xiaomi Redmi Note 5A 16GB;Смартфон Xiaomi Redmi Note 5A 16GB Gray
Xiaomi Redmi Note 5A 16GB;Смартфон Xiaomi Redmi Note 5A 16GB Rose Gold
Xiaomi Redmi Note 5A Prime 32 GB;Смартфон Xiaomi Redmi Note 5A Prime 32 GB  Rose Gold
Xiaomi Redmi Note 5A Prime 32 GB;Смартфон Xiaomi Redmi Note 5A Prime 32 GB Gold
Xiaomi Redmi Note 5A Prime 32 GB;Смартфон Xiaomi Redmi Note 5A Prime 32 GB Grey
Xiaomi Redmi Note 5A Prime 64 GB;Смартфон Xiaomi Redmi Note 5A Prime 64 GB Grey
Xiaomi Redmi S2 32GB;Смартфон Xiaomi Redmi S2 32GB Grey
Xiaomi Redmi S2 64GB;Смартфон Xiaomi Redmi S2 64GB Gold
Xiaomi Redmi S2 64GB;Смартфон Xiaomi Redmi S2 64GB Grey
'

exec Parser_FileParse 'ProductGroup'--, 1, 1
IF NOT EXISTS (SELECT * FROM vDimProduct WHERE productgroupname = 'Xiaomi Redmi S2 64GB')
BEGIN
	PRINT 'Shift - ERROR!!!!'
END
ELSE
	PRINT 'Shift - ok'



	