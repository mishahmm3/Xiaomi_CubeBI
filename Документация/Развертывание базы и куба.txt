﻿---------------------------------------
База для прода - Xiaomi-BI_Prod, Куб - Xiaomi-BI_Prod
---------------------------------------
1. Прогнать все change-скрипты из папки Database\Scripts (в дальнейшем планируется ведение учета прогона скриптов в базе)

2. Прогнать обновления объектов логики базы
 2.1. Запустить BuildDatabase_OLTP.cmd и BuildDatabase_DW.cmd  
 2.2. В папке _output появятся файлы со всеми объектами базы
 2.3. Прогнать эти файлы на базе: сначала OLTP-часть, потом DW-часть 

3.1 Вручную провести расчет остатков запуском EXEC [dbo].[Calc_ProductRemains] '20170101', 'дата + 3 месяца'
лучше делать по частям, например так
   EXEC [dbo].[Calc_ProductRemains] '20170101', '20170331' 
   EXEC [dbo].[Calc_ProductRemains] '20170401', '20170630' и т.д. 
3.2 Вручную провести расчет cross-check

4. Настроить Job Job_Cube_Process процессинга/расчета на нужные базы

5. Развернуть куб через проект куба в студии. 
Для этого:
 - выбрать нужную база для datasource данных куба
 - в настройках проекта раздела deployment указать нужное имя куба. 
 - выбрать команду "Развертывание"