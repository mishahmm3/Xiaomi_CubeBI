SET NOCOUNT ON;
GO

-- �������� �������� ����������
IF OBJECT_ID('tempdb..##TempTran') IS NOT NULL DROP TABLE ##TempTran
CREATE TABLE ##TempTran (Value int)

DECLARE @Count int = @@TRANCOUNT
INSERT INTO ##TempTran(Value) 
SELECT @Count
GO

#include "Views\!Views.sql"
#include "Functions\!Functions.sql"
#include "Procedures\!Procedures.sql"
#include "Data\!Data.sql"

-- �������� �������� ����������
DECLARE @CountRes int = (SELECT TOP 1 Value FROM ##TempTran)
DECLARE @CountReal int = @@TRANCOUNT

TRUNCATE TABLE ##TempTran

IF @CountRes <> @CountReal
	RAISERROR('������ ������� �������: �������� �������� ����������!', 16, 3)
GO



