﻿
PRINT ' Altering data - Dictionary_Init'
GO
	
-- Тип операции
DELETE FROM Operations WHERE id NOT IN (0, 1)	

UPDATE Operations SET Name = 'Приход', description = '' WHERE Id = 0 AND Name <> 'Приход'
UPDATE Operations SET Name = 'Расход', description = '' WHERE Id = 1 AND Name <> 'Расход'


-- Вид и тип платежа
UPDATE PaymentType SET Name = 'Оплата наличными' WHERE Code = '0'
UPDATE PaymentType SET Name = 'Оплата картой' WHERE Code = '1'
UPDATE PaymentType SET Name = 'Оплата в рассрочку' WHERE Code = '2'
UPDATE PaymentType SET Name = 'Оплата сертификатом' WHERE Code = '3'
UPDATE PaymentType SET Name = 'Оплата бонуса' WHERE Code = '4'
UPDATE PaymentType SET Name = 'Оплата картой БС' WHERE Code = '5'
UPDATE PaymentType SET Name = 'Оплата авансом' WHERE Code = '6'

-- статусы чека
UPDATE CheckStatus SET Name = 'Отложенный' WHERE Code = '0'
UPDATE CheckStatus SET Name = 'Аннулированный' WHERE Code = '1'
UPDATE CheckStatus SET Name = 'Пробитый' WHERE Code = '2'
UPDATE CheckStatus SET Name = 'Архивный' WHERE Code = '3'

-- статусы чека
UPDATE CheckOperationVid SET Name = 'Приход' WHERE Code = '0'
UPDATE CheckOperationVid SET Name = 'Возврат прихода' WHERE Code = '1'

-- Тип периода
IF NOT EXISTS(SELECT * FROM PeriodType WHERE Code = 'D')
BEGIN
	INSERT INTO PeriodType(Code, Name, Description)
	SELECT 'D', 'День', 'Величина относится ко дню в целом'
END

IF NOT EXISTS(SELECT * FROM PeriodType WHERE Code = 'M')
BEGIN
	INSERT INTO PeriodType(Code, Name, Description)
	SELECT 'M', 'Месяц', 'Величина относится к месяцу в целом'
END

IF NOT EXISTS(SELECT * FROM PeriodType WHERE Code = 'W')
BEGIN
	INSERT INTO PeriodType(Code, Name, Description)
	SELECT 'W', 'Неделя', 'Величина относится к неделе целиком'
END

IF NOT EXISTS(SELECT * FROM PeriodType WHERE Code = 'TD')
BEGIN
	INSERT INTO PeriodType(Code, Name, Description)
	SELECT 'TD', 'Декада', 'Декада месяца'
END

IF NOT EXISTS(SELECT * FROM PeriodType WHERE Code = 'Q')
BEGIN
	INSERT INTO PeriodType(Code, Name, Description)
	SELECT 'Q', 'Квартал', 'Величина относится к кварталу целиком'
END

IF NOT EXISTS(SELECT * FROM PeriodType WHERE Code = 'HY')
BEGIN
	INSERT INTO PeriodType(Code, Name, Description)
	SELECT 'HY', 'Полугодие', 'Величина относится к полугодию целиком'
END

IF NOT EXISTS(SELECT * FROM PeriodType WHERE Code = 'Y')
BEGIN
	INSERT INTO PeriodType(Code, Name, Description)
	SELECT 'Y', 'Год', 'Величина относится к году в целом'
END

IF NOT EXISTS(SELECT * FROM PeriodType WHERE Code = 'WeekDay')
BEGIN
	INSERT INTO PeriodType(Code, Name, Description)
	SELECT 'WeekDay', 'День недели', 'Величина относится к определенному дню недели'
END

IF NOT EXISTS(SELECT * FROM PeriodType WHERE Code = 'Any')
BEGIN
	INSERT INTO PeriodType(Code, Name, Description)
	SELECT 'Any', 'Произвольный', 'Величина относится к произвольному периоду времени'
END


-- Параметры
EXEC dbo.Rollout_Parameter 'KSeason',	'Коэффициент сезонности'

UPDATE [roles] SET name = 'Управляющий', description = 'Управляющий' WHERE description LIKE '%admin%'
UPDATE [roles] SET name = 'Продавец-консультант', description = 'Продавец-консультант' WHERE description LIKE '%manager%'


-- Параметры
EXEC dbo.Rollout_Parameter 'ShopStaff',	'Штатное расписание магазина', 'Shop'

-- Параметры
EXEC dbo.Rollout_Parameter 'EncashmentNorma',	'Норма инкассации', 'Shop'

------------------------------------------------------------------------------
-- Скидки и акции
------------------------------------------------------------------------------
IF NOT EXISTS(SELECT * FROM MarketAction WHERE Name = 'Округление')
BEGIN
	INSERT INTO MarketAction(Name, DateFrom, DateTo)
	SELECT 'Округление', '20010101', '20200101'
END

IF NOT EXISTS(SELECT * FROM Discounts WHERE Name = 'Округление в пользу клиента')
BEGIN
	INSERT INTO Discounts(Name, Value)
	SELECT 'Округление в пользу клиента', 0
END

DECLARE @DiscId int = (SELECT id FROM Discounts WHERE Name = 'Округление в пользу клиента')
DECLARE @ActionId int = (SELECT MarketActionId FROM MarketAction WHERE Name = 'Округление')
IF NOT EXISTS(SELECT * FROM MarketActionDiscount WHERE discount_id = @DiscId)
BEGIN
	INSERT INTO MarketActionDiscount(MarketActionId, discount_id, shop_id)
	SELECT @ActionId, @DiscId, id FROM shops
END

UPDATE Shift 
SET HourFrom = 10, HourTo = 20 
WHERE Code = 'I'

UPDATE Shift 
SET HourFrom = 12, HourTo = 22 
WHERE Code = 'III'
UPDATE Shift 
SET Name = 'Полная смена', HourFrom = 10, HourTo = 22 
WHERE Code = 'III'

