﻿IF NOT EXISTS(SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.Rollout_Parameter') AND type IN (N'P', N'PC'))
BEGIN
	EXEC ('CREATE PROCEDURE dbo.Rollout_Parameter AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO
-- =======================================================
-- Author:	Морозов М.А.
-- Create date: 20180620
-- =======================================================
-- Description:	Загружает/обновляет Параметр
-- =======================================================
ALTER PROCEDURE dbo.Rollout_Parameter 
	@Code varchar(100),
	@Name varchar(250),
	@EntityCode varchar(250) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ParameterId int = (SELECT ParameterId FROM Parameter WHERE Code = @Code)

	IF (@ParameterId IS NULL)
	BEGIN
		INSERT INTO Parameter(Code, Name, EntityCode)
		VALUES (@Code, @Name, @EntityCode)
	END
	ELSE
	BEGIN
		UPDATE Parameter
		SET 
			Name = @Name, EntityCode = @EntityCode
		WHERE 
			Code = @Code
	END
END
GO

--BEGIN TRAN

--EXEC dbo.Rollout_Parameter 'KSeason',	'Коэффициент сезонности'
--SELECT * FROM Parameter
--ROLLBACK TRAN

