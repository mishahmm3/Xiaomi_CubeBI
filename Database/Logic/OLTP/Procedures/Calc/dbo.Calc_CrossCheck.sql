﻿IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Calc_CrossCheck]') AND type IN (N'P', N'PC'))
BEGIN
	EXEC ('CREATE PROCEDURE [dbo].[Calc_CrossCheck]  AS RAISERROR(''NOT implemented yet.'', 16, 3);')
END
GO
PRINT ' Altering procedure [dbo].[Calc_CrossCheck]'
GO
-- =============================================
-- Author:		Ternovskiy A.V.
-- Create date: 2018-06-04
-- Description:	CROSS Check Calculation
-- =============================================
ALTER PROCEDURE [dbo].[Calc_CrossCheck]
	@DateFrom datetime = NULL,
	@DateTo datetime = NULL
AS
BEGIN
	SET NOCOUNT ON;	
	
	PRINT '   ' + CONVERT(varchar(30), GETDATE(), 114) + ' - Старт расчета кросс-чеков - Calc_CrossCheck'

	--------------------------------------------------------
	-- 
	--------------------------------------------------------
	IF 1 = 1
	BEGIN
		-- Если даты не переданы - то за вчера
		SET @DateFrom = CAST(ISNULL(@DateFrom, DATEADD(DAY, -1, GETDATE())) AS Date)  
		SET @DateTo = CAST(ISNULL(@DateTo, DATEADD(DAY, -1, GETDATE())) AS Date)  

		DELETE FROM cross_checks WHERE CAST(date AS date) BETWEEN @DateFrom AND @DateTo
	END

	--------------------------------------------------------
	-- Расчет
	--------------------------------------------------------
	IF 1 = 1
	BEGIN
		-- Все чеки по датам и по группам
		SELECT 
			[date] AS DateFrom,
			ch.id AS CheckId, 
			pgp.product_group_id AS ProductGroupId,
			shop.ShopId,
			COUNT(*) AS Count
		INTO #All
		FROM check_headers ch 
			INNER JOIN check_details cd 
				ON ch.id = cd.check_header_id
			INNER JOIN products p 
				ON cd.product_id = p.id
			INNER JOIN product_group_products pgp 
				ON p.id = pgp.product_id
			LEFT JOIN vDimCashDesk shop
				ON shop.CashDeskId = ch.cash_desk_id
		WHERE
			 CAST([date] AS date) BETWEEN @DateFrom AND @DateTo
		GROUP BY 
			ch.id, pgp.product_group_id, shop.ShopId, date

		-- Чеки по датам с несколькими группами
		SELECT 
			CheckId, DateFrom, ShopId
		INTO #MultiGroup 	
		FROM #All
		GROUP BY DateFrom, CheckId, ShopId
		HAVING COUNT(*) > 1

		SELECT 
			mg.DateFrom, mg.ShopId, mg.CheckId, ProductGroupId 
		INTO #Data
		FROM #MultiGroup mg
			INNER JOIN #All a
				ON a.DateFrom = mg.DateFrom
					AND a.CheckId = mg.CheckId
					AND a.ShopId = mg.ShopId

		SELECT 
			d1.DateFrom,  
			d1.ShopId,  
			d1.CheckId,
			d1.ProductGroupId AS ProductGroupId1,
			d2.ProductGroupId AS ProductGroupId2
		INTO #Result
		FROM #Data d1
			INNER JOIN #Data d2
				ON d2.DateFrom = d1.DateFrom
					AND d2.ShopId = d1.ShopId
					AND d2.CheckId = d1.CheckId
					AND d2.ProductGroupId <> d1.ProductGroupId
					AND d2.ProductGroupId < d1.ProductGroupId
	END

	--------------------------------------------------------
	-- Результат 
	--------------------------------------------------------
	IF 1 = 1
	BEGIN
		INSERT INTO cross_checks([date], ShopId, product_group1_id, product_group2_id, [count])
		SELECT 
			CAST(DateFrom AS Date) AS DateFrom,
			ShopId,
			ProductGroupId1,
			ProductGroupId2,
			COUNT(*) AS count
		FROM #Result 
		GROUP BY 
			CAST(DateFrom AS Date), ShopId, ProductGroupId1, ProductGroupId2 
	END	

	PRINT '   ' + CONVERT(varchar(30), GETDATE(), 114) + ' - Завершение расчета кросс-чеков - Calc_CrossCheck'
END
GO

----exec [dbo].[Calc_All]
-- exec [dbo].[Calc_CrossCheck] '20160101', '20180901'

----SELECT * FROM cross_check_fact -- order by 5 desc
----where 
------product_group2_id = 209 and 
----product_group1_id = 191


--SELECT sum(count) FROM cross_checks -- 1733