﻿IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Calc_ParametersForDay]') AND type IN (N'P', N'PC'))
BEGIN
	EXEC ('CREATE PROCEDURE [dbo].[Calc_ParametersForDay] AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
PRINT ' Altering procedure [dbo].[Calc_ParametersForDay]'
GO
-- =============================================
-- Author:		Морозов Михаил
-- Create date: 20180620
-- Расчет параметров попериодовок в дни
-- =============================================
ALTER PROCEDURE [dbo].[Calc_ParametersForDay]
	@DateFrom datetime = NULL,	
	@DateTo datetime = NULL	
AS
BEGIN

	--********************************************************
	-- Настройки и проверки
	--********************************************************
	IF 1 = 1
	BEGIN 
		IF @DateFrom IS NULL
			SET @DateFrom = '20160101'

		IF @DateTo IS NULL
			SET @DateTo = DATEADD(MONTH, 1, GETDATE())
	
		-- Всегда плюс год для полного диапазона периодов
		SET @DateTo = DATEADD(YEAR, 1, @DateTo)
	
		SELECT DISTINCT       
			CAST([Date] AS DATE) AS DateFrom
		INTO #DateMatrix	
		FROM calendar
		WHERE 
			[Date] BETWEEN @DateFrom AND @DateTo

		TRUNCATE TABLE ParameterDay
	
		-- Строки
		IF OBJECT_ID('tempdb..#ParamSum') IS NOT NULL DROP TABLE #ParamSum;
		CREATE TABLE #ParamSum
		(
			DateFrom datetime,
			ParameterId int, 
			EntityTableCode varchar(250),
			EntityId int,
			Value float
		)
	END
	
	--********************************************************
	-- Расчет параметров с жестким периодом
	--********************************************************
	IF 1 = 1
	BEGIN 
		INSERT INTO #ParamSum(DateFrom, ParameterId, EntityTableCode, EntityId, Value) 
		SELECT 
			DateFrom, ParameterId, EntityTableCode, EntityId, 
			COALESCE([D], [M], [Q], [HY], [Y]) AS Value
		FROM (
			SELECT 
				m.DateFrom, ParameterId, Period, EntityTableCode, EntityId, Value  
			FROM (
				SELECT 
					h.DateFrom,
					h.ParameterId,
					h.EntityTableCode,
					h.EntityId,
					h.PeriodTypeCode AS Period,
					h.Value
				FROM v_ParameterPeriodValue h WITH (NOLOCK)
				WHERE 
					h.EntityTableCode IS NOT NULL
					AND h.PeriodTypeCode IN ('D', 'M', 'Q', 'HY', 'Y')
					AND h.DateFrom BETWEEN @DateFrom AND @DateTo
				) AS t
				INNER JOIN #DateMatrix m
					ON m.DateFrom >= t.DateFrom
						AND m.DateFrom < DATEADD(DAY, 
						CASE Period 
							WHEN 'D' THEN 1
							WHEN 'M' THEN DATEDIFF(DAY, t.DateFrom, DATEADD(MONTH, 1, t.DateFrom))
							WHEN 'Q' THEN DATEDIFF(DAY, t.DateFrom, DATEADD(MONTH, 3, t.DateFrom))  
							WHEN 'HY'THEN DATEDIFF(DAY, t.DateFrom, DATEADD(MONTH, 6, t.DateFrom))   
							WHEN 'Y' THEN DATEDIFF(DAY, t.DateFrom, DATEADD(MONTH, 12, t.DateFrom))   
						END, t.DateFrom)
		) p1
		PIVOT(MAX(Value) FOR period IN (D, M, Q, HY, Y)) p2
	END 
	
	--********************************************************
	-- Расчет параметров с проивзольным периодом
	--********************************************************
	IF 1 = 1
	BEGIN 
		INSERT INTO #ParamSum(DateFrom, ParameterId, EntityTableCode, EntityId, Value) 
		SELECT 
			m.DateFrom,
			par.ParameterId,
			par.EntityTableCode,
			par.EntityId,
			val.Value 
		FROM #DateMatrix m
			CROSS JOIN
			(
				SELECT DISTINCT 
					ParameterId, EntityId, EntityTableCode 
				FROM v_ParameterPeriodValue
				WHERE 
					PeriodTypeCode = 'Any'
			) par
			OUTER APPLY
			(
				SELECT TOP 1 
					d.Value
				FROM v_ParameterPeriodValue d
				WHERE PeriodTypeCode = 'Any'
					AND d.ParameterId = par.ParameterId
					AND d.EntityId = par.EntityId
					AND d.DateFrom <= m.DateFrom
					ORDER BY d.DateFrom DESC
			) val
	END 

	--********************************************************
	-- Результат  
	--********************************************************
	IF 1 = 1
	BEGIN 
		INSERT INTO ParameterDay(DateFrom, ParameterId, EntityId, Value)				
		SELECT 
			d.DateFrom, 
			d.ParameterId,
			COALESCE(s.Id, c.Id) AS EntityId,
			d.Value
		FROM #ParamSum d
			LEFT JOIN shops s
				ON s.Id = d.EntityId AND d.EntityTableCode = 'Shop'
			LEFT JOIN Cities c
				ON c.Id = d.EntityId AND d.EntityTableCode = 'City'
	END
END 
GO

--BEGIN TRAN
--	exec [dbo].[Calc_ParametersForDay]
--	SELECT * FROM v_ParameterDay
--ROLLBACK TRAN
