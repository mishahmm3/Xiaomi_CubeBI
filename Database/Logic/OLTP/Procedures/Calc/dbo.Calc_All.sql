﻿IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Calc_All]') AND type IN (N'P', N'PC'))
BEGIN
	EXEC ('CREATE PROCEDURE [dbo].[Calc_All]  AS RAISERROR(''NOT implemented yet.'', 16, 3);')
END
GO
PRINT ' Altering procedure [dbo].[Calc_All]'
GO
-- =============================================
-- Author:		Морозов М.А.
-- Create date: 2018-06-06
-- Description:	Общая ХП расчета данных для куба
-- =============================================
ALTER PROCEDURE [dbo].[Calc_All]
	@DateFrom datetime = NULL,
	@DateTo datetime = NULL
AS
BEGIN
	SET NOCOUNT ON;	

	-- Если даты не переданы - то за предыдущий 1 день + текущий
	SET @DateFrom = CAST(ISNULL(@DateFrom, DATEADD(DAY, -1, GETDATE())) AS Date)  
	SET @DateTo = CAST(ISNULL(@DateTo, DATEADD(DAY, 0, GETDATE())) AS Date)  

	PRINT CONVERT(varchar(30), GETDATE(), 114) + ' - Старт расчета данных - Calc_All'
	PRINT 'Период: ' + CONVERT(varchar(30), @DateFrom, 112) + ' - ' + CONVERT(varchar(30), @DateTo, 112)		

	EXEC Calc_Tariff -- пока за весь период
	EXEC Calc_ParametersForDay
	
	EXEC dbo.Calc_CrossCheck @DateFrom, @DateTo
	
	EXEC dbo.Calc_ProductRemains @DateFrom, @DateTo

	PRINT CONVERT(varchar(30), GETDATE(), 114) + ' - Завершение расчета данных - Calc_All'
END
GO
