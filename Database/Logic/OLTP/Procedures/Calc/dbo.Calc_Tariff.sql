﻿IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Calc_Tariff]') AND type IN (N'P', N'PC'))
BEGIN
	EXEC ('CREATE PROCEDURE [dbo].[Calc_Tariff] AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
PRINT ' Altering procedure [dbo].[Calc_Tariff]'
GO
-- =============================================
-- Author:		Морозов Михаил
-- Create date: 20180620
-- =============================================
ALTER PROCEDURE [dbo].[Calc_Tariff]
	@DateFrom datetime = NULL,	
	@DateTo datetime = NULL	
AS
BEGIN
	IF @DateFrom IS NULL
		SET @DateFrom = '20170101'

	IF @DateTo IS NULL
		SET @DateTo = DATEADD(MONTH, 1, GETDATE())
	-- Всегда плюс год для полного диапазона периодов
	SET @DateTo = DATEADD(YEAR, 1, @DateTo)
	
	SELECT DISTINCT       
		CAST([Date] AS DATE) AS DateFrom
	INTO #DateMatrix	
	FROM calendar
	WHERE 
		[Date] BETWEEN @DateFrom AND @DateTo

	TRUNCATE TABLE TariffCalc

	--********************************************************
	-- Агрегации 
	--********************************************************
	IF 1 = 1
	BEGIN 
		IF OBJECT_ID('tempdb..#TariffSum') IS NOT NULL 
		DROP TABLE #TariffSum

		SELECT 
			DateFrom, ParameterCode,
			COALESCE([D], [M], [Q], [HY], [Y]) AS Value
		INTO #TariffSum
		FROM (
			SELECT 
				m.DateFrom, ParameterCode, Period, Value  
			FROM (
				SELECT 
					h.DateFrom,
					p.Code AS ParameterCode,
					per.Code AS Period,
					--per.MonthDuration,
					h.Value
				FROM ParameterPeriodValue h WITH (NOLOCK)
					INNER JOIN PeriodType per
						ON per.PeriodTypeId = h.PeriodTypeId
					INNER JOIN Parameter p
						ON p.ParameterId = h.ParameterId
				WHERE 
					p.Code IN ('KSeason')
					AND per.Code IN ('D', 'M', 'Q', 'HY', 'Y')
					AND h.DateFrom BETWEEN @DateFrom AND @DateTo
				) AS t
				INNER JOIN #DateMatrix m
					ON m.DateFrom >= t.DateFrom
						AND m.DateFrom < DATEADD(DAY, 
						CASE Period 
							WHEN 'D' THEN 1
							WHEN 'M' THEN DATEDIFF(DAY, t.DateFrom, DATEADD(MONTH, 1, t.DateFrom))
							WHEN 'Q' THEN DATEDIFF(DAY, t.DateFrom, DATEADD(MONTH, 3, t.DateFrom))  
							WHEN 'HY'THEN DATEDIFF(DAY, t.DateFrom, DATEADD(MONTH, 6, t.DateFrom))   
							WHEN 'Y' THEN DATEDIFF(DAY, t.DateFrom, DATEADD(MONTH, 12, t.DateFrom))   
						END, t.DateFrom)
		) p1
		PIVOT(MAX(Value) FOR period IN (D, M, Q, HY, Y)) p2
	END 

	--********************************************************
	-- Результат  
	--********************************************************
	IF 1 = 1
	BEGIN 
		INSERT INTO TariffCalc(DateFrom, KSeason)				
		SELECT 
			DateFrom,
			KSeason
		FROM 
		(	SELECT 
				ts.DateFrom, ts.ParameterCode, ts.Value
			FROM #TariffSum ts
		) p
		PIVOT(SUM(Value) FOR ParameterCode IN (KSeason)) pvt
	END
END 
GO

--SELECT * FROM periodType
--delete from [ParameterPeriodValue]
--insert INTO [ParameterPeriodValue](parameterId, periodTypeId, DateFrom, Value)
--select 3, 7, '20170101', 1.1 union all
--select 3, 5, '20170301', 1.3 union all
--select 3, 2, '20170601', 1.6 union all
--select 3, 1, '20170610', 1.0010 union all
--select 3, 7, '20180101', 2.1

--exec [dbo].[Calc_Tariff] '20170101', '20180101'

--SELECT * FROM vFactTariff 
--order by 1,2,3

--exec [dbo].[Calc_Tariff] '20170101', '20180101'

--SELECT * FROM TariffCalc order by 2

--SELECT * FROM [PeriodType]
--SELECT * FROM [Parameter]
--SELECT * FROM [ParameterPeriodValue]


------delete from ParameterPeriodValue
----insert INTO [ParameterPeriodValue](parameterId, periodTypeId, DateFrom, Value)
----select 3, 7, '20170101', 1.1 union all
----select 3, 5, '20170301', 1.3 union all
----select 3, 2, '20170601', 1.6 union all
----select 3, 1, '20170610', 1.010 union all
----select 3, 7, '20180101', 2.1


