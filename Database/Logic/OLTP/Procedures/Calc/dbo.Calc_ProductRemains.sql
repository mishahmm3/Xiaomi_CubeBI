﻿IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Calc_ProductRemains]') AND type IN (N'P', N'PC'))
BEGIN
	EXEC ('CREATE PROCEDURE [dbo].[Calc_ProductRemains]  AS RAISERROR(''NOT implemented yet.'', 16, 3);')
END
GO
PRINT ' Altering procedure [dbo].[Calc_ProductRemains]'
GO
-- =============================================
-- Author:		Морозов М.А.
-- Create date: 2018-06-07
-- Description:	Расчет остатков товаров по дням
-- =============================================
ALTER PROCEDURE [dbo].[Calc_ProductRemains]
	@DateFrom datetime = NULL,
	@DateTo datetime = NULL
AS
BEGIN
	SET NOCOUNT ON;	
	
	PRINT '   ' + CONVERT(varchar(30), GETDATE(), 114) + ' - Старт остатков товаров по дням - Calc_ProductRemains'

	--------------------------------------------------------
	-- Переменные
	--------------------------------------------------------
	IF 1 = 1
	BEGIN
		-- Если даты не переданы - то за предыдущие 1 дня
		SET @DateFrom = CAST(ISNULL(@DateFrom, DATEADD(DAY, -1, GETDATE())) AS Date)  
		SET @DateTo = CAST(ISNULL(@DateTo, DATEADD(DAY, -1, GETDATE())) AS Date)  

		-- Расчет валидный начиная с 20170102
		IF (@DateFrom <= '20170101')
		BEGIN
			SET @DateFrom = '20170102'
		END

		-- Дата перед периодом
		DECLARE @PrevDate date = DATEADD(DAY, -1, @DateFrom)
	END
	
	--------------------------------------------------------
	-- Подготовка к расчету
	--------------------------------------------------------
	IF 1 = 1
	BEGIN
		-- Определяем матрицу Дата-Товар-Склад
		SELECT DISTINCT
			DateFrom, WarehouseId, ProductId 
		INTO #Matrix
		FROM 
		(
			SELECT DISTINCT
				CAST(pr.date AS Date) AS DateFrom,
				warehouse_id AS WarehouseId, 
				product_id AS ProductId 
			FROM product_remains pr
			WHERE 
				CAST(pr.date AS Date) BETWEEN @PrevDate AND @DateTo 
		
			UNION ALL
		
			SELECT DISTINCT
				CAST(pr.date AS Date) AS DateFrom,
				warehouse_id, product_id 
			FROM product_calc_remains pr
			WHERE 
				CAST(pr.date AS Date) BETWEEN @PrevDate AND @DateTo 
		) d
		
		-- Вся матрица
		IF OBJECT_ID('tempdb..#MatrixAll') IS NOT NULL DROP TABLE #MatrixAll
		CREATE TABLE #MatrixAll
		(
			Id int IDENTITY(1,1) PRIMARY KEY,
			DateFrom datetime,
			ProductId int,
			WarehouseId int,
			ProductCount int
		)

		INSERT INTO #MatrixAll(DateFrom, ProductId, WarehouseId)
		SELECT 
			DateFrom, ProductId, WarehouseId 
		FROM 
		(
			(SELECT DISTINCT DateFrom FROM #Matrix) d1
			CROSS JOIN
			(SELECT DISTINCT ProductId FROM #Matrix) d2
			CROSS JOIN
			(SELECT DISTINCT WarehouseId FROM #Matrix) d3
		) 
		--не нужен CREATE INDEX IX_MatrixAll ON #MatrixAll (DateFrom, ProductId, WarehouseId)

		DECLARE @MatrixSize int = (SELECT count(*) FROM #MatrixAll)
		PRINT 'Размер матрицы = ' + CAST(@MatrixSize AS varchar(10))
	
	-- отладка
	--return
	--SELECT * FROM #MatrixAll where ProductId = 292 and WarehouseId = 225 order by 1
	-- SELECT count(*) FROM #MatrixAll
	--return
							
		-- Итоги движения внутри текущего дня
		-- Сразу по всем дням
		SELECT 
			r.OperationalDate AS DateFrom, 
			r.ProductId, 
			r.WarehouseId,
			SUM(CASE 
					WHEN r.OperationName = 'Приход' 
					THEN ISNULL(r.ProductCount, 0)
					ELSE -1 * ISNULL(r.ProductCount, 0)
				END) AS ProductDelta
		INTO #DayDelta
		FROM v_ProductRemains r
		WHERE
			r.OperationalDate BETWEEN DATEADD(DAY, -1, @DateFrom) AND @DateTo
		GROUP BY
			r.OperationalDate, r.ProductId, r.WarehouseId
	END
	
	--------------------------------------------------------
	-- Расчет по дням
	--------------------------------------------------------
	IF 1 = 1
	BEGIN
		DECLARE @CurrentDate Datetime = @DateFrom
		DECLARE @CurrentPrevDate Datetime

		IF OBJECT_ID('tempdb..#PrevData') IS NOT NULL DROP TABLE #PrevData
		CREATE TABLE #PrevData
		(
			DateFrom datetime,
			ProductId int,
			WarehouseId int,
			ProductCount int
		)

		WHILE @CurrentDate <= @DateTo
		BEGIN
			SET @CurrentPrevDate = DATEADD(DAY, -1, @CurrentDate)

			TRUNCATE TABLE #PrevData

			-- Остатки на предыдущий день
			INSERT INTO #PrevData(DateFrom, ProductId, WarehouseId, ProductCount)
			SELECT 
				m.DateFrom, 
				m.ProductId, 
				m.WarehouseId, 
				ISNULL(r.count, 0) AS ProductCount 
			FROM #MatrixAll m
				LEFT JOIN product_calc_remains r
					ON r.date = m.DateFrom
						AND r.Product_Id = m.ProductId
						AND r.Warehouse_Id = m.WarehouseId
			WHERE
				m.DateFrom = @CurrentPrevDate

			-- Грузим результат за день
			MERGE product_calc_remains AS tgt
			USING
			(
				SELECT 
					DATEADD(DAY, 1, prev.DateFrom) AS DateFrom,
					prev.ProductId,
					prev.WarehouseId,
					prev.ProductCount + ISNULL(cur.ProductDelta, 0)  AS ResultCount 
				FROM #MatrixAll m
					LEFT JOIN #PrevData prev
						ON prev.DateFrom = m.DateFrom
							AND prev.ProductId = m.ProductId
							AND prev.WarehouseId = m.WarehouseId
					LEFT JOIN #DayDelta cur
						ON cur.DateFrom = @CurrentPrevDate
							AND cur.DateFrom = prev.DateFrom
							AND cur.ProductId = prev.ProductId
							AND cur.WarehouseId = prev.WarehouseId
				WHERE 
					m.DateFrom = @CurrentPrevDate
			) AS src
			ON src.DateFrom = tgt.date
				AND src.ProductId = tgt.Product_Id
				AND src.WarehouseId = tgt.Warehouse_Id
			WHEN MATCHED AND tgt.count <> src.ResultCount THEN  
				UPDATE SET tgt.count = src.ResultCount
			WHEN NOT MATCHED THEN
				INSERT (Product_Id, Warehouse_Id, date, count)
				VALUES (src.ProductId, src.WarehouseId, src.DateFrom, src.ResultCount);

			SET @CurrentDate = DATEADD(DAY, 1, @CurrentDate)
		END
	END
	
	DROP TABLE #Matrix
	DROP TABLE #MatrixAll
	DROP TABLE #PrevData
	DROP TABLE #DayDelta

	PRINT '   ' + CONVERT(varchar(30), GETDATE(), 114) + ' - Завершение остатков товаров по дням - Calc_ProductRemains'
END
GO

--EXEC [dbo].[Calc_ProductRemains] '20170510', '20170630'
--EXEC [dbo].[Calc_ProductRemains] '20170701', '20170730'
--EXEC [dbo].[Calc_ProductRemains] '20170801', '20171231'

--BEGIN TRAN
--delete  FROM product_calc_remains where  date > '20170101'
--EXEC [dbo].[Calc_ProductRemains] '20170101', '20170102'
--ROLLBACK TRAN


--exec [dbo].[Calc_ProductRemains] '20170101', '20170105'

--EXEC [dbo].[Calc_ProductRemains] '20170101', '20170201'
--EXEC [dbo].[Calc_ProductRemains] '20170201', '20170401'
--EXEC [dbo].[Calc_ProductRemains] '20170401', '20170801'
--EXEC [dbo].[Calc_ProductRemains] '20170801', '20180101'

--EXEC [dbo].[Calc_ProductRemains] '20180101', '20180401'
--EXEC [dbo].[Calc_ProductRemains] '20180401', '20180801'

--SELECT count(*) FROM product_calc_remains