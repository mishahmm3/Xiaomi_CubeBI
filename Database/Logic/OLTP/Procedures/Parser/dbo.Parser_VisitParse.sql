﻿IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Parser_VisitParse]') AND type IN (N'P', N'PC'))
BEGIN
	EXEC ('CREATE PROCEDURE [dbo].[Parser_VisitParse]  AS RAISERROR(''NOT implemented yet.'', 16, 3);')
END
GO
PRINT ' Altering procedure [dbo].[Parser_VisitParse]'
GO
-- =============================================
-- Author:		Морозов М.А.
-- Create date: 20180629
-- Description:	Парсинг посещений
-- =============================================
ALTER PROCEDURE [dbo].[Parser_VisitParse]
	@FileLogId int, -- Id файла
	@IsPassErrors bit = NULL -- пропускать обработку ошибок
AS
BEGIN
	SET NOCOUNT ON
	
	SET @IsPassErrors = ISNULL(@IsPassErrors, 0)
	
	--********************************************************
	-- Первичная загрузка даных Content
	--********************************************************
	IF 1 = 1
	BEGIN 
		EXEC Parser_ContentParse @FileLogId, ','

		-- Конвертируем 
		DECLARE @ColName int = 1
		DECLARE @ColSerial int = 2
		DECLARE @ColDate int = 3
		DECLARE @ColCount int = 6

		SELECT 
			CASE 
				WHEN CHARINDEX('"', [1]) = 1	
				THEN SUBSTRING([1], 2, LEN([1]) - 2)
				ELSE [1]
			END AS Name,
			CASE 
				WHEN CHARINDEX('"', [3]) = 1	
				THEN CAST(SUBSTRING(SUBSTRING([3], 2, LEN([3]) - 2), 1, 16) AS DateTime)
				ELSE CAST(SUBSTRING([3], 1, 16) AS DateTime)
			END AS DateFrom,
			[6] AS VisitCount
		INTO #Data
		FROM #ParserData d
		WHERE 
			[3] IS NOT NULL
	END

	--********************************************************
	-- Обработка ошибок
	--********************************************************
	IF 1 = 1
	BEGIN 
		DECLARE @ErrorList varchar(8000) = '';

		SET @ErrorList= ''
		SELECT
			@ErrorList =
				CASE 
					WHEN LEN(@ErrorList) = 0 THEN '"' + CAST(error AS varchar(100)) + '"'
					WHEN LEN(@ErrorList) > 0 THEN (@ErrorList + ';' + '"' + CAST(error AS varchar(100))) + '"' 
				END
		FROM 
		(
			SELECT DISTINCT 
				d.Name AS error,
				sh.Id AS Id
			FROM #Data d
				LEFT JOIN Shops sh
					ON sh.Name = d.Name
			WHERE 
				sh.Id IS NULL
		) err	

		IF @ErrorList <> '' AND @IsPassErrors = 0
		BEGIN
			RAISERROR('Загрузка посещений (Parser_VisitParse). В базе не найдено соответствие имен для магазинов: %s.', 16, 1, @ErrorList)
			RETURN
		END	
	END
	
	--********************************************************
	-- Загрузка данных
	--********************************************************
	IF 1 = 1
	BEGIN 
		DECLARE @Now datetime = GETDATE()	

		MERGE visit_logs AS tgt
		USING
		(
			SELECT  
				d.DateFrom, 
				sh.Id AS ShopId,
				VisitCount
			FROM #Data d
				INNER JOIN Shops sh
					ON sh.Name = d.Name
		) AS src
		ON src.DateFrom = tgt.date
			AND src.ShopId = tgt.Shop_Id
		WHEN MATCHED AND tgt.count <> src.VisitCount THEN  
			UPDATE SET tgt.count = src.VisitCount,
					tgt.updated_at = @Now
		WHEN NOT MATCHED THEN
			INSERT (Shop_Id, date, Count, created_at)
			VALUES (src.ShopId, src.DateFrom, src.VisitCount, @Now);
	END
END
GO

--SELECT TOP 1000 FileStatusCode, FileTypeCode, FileLogId, UploadDate FROM vFilelog where FileStatusCode = 'Ready'
----BEGIN TRAN
------delete from file_logs where id >= 259 and id <= 275
----EXEC Parser_FileParse 'Visit', 1

----SELECT top 1000 FileTypeCode, FileLogId, FileStatusCode, UploadDate FROM vFileLog WHERE FileLogId > 229
--ROLLBACK TRAN

