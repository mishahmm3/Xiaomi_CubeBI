﻿IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Parser_FileLoad]') AND type IN (N'P', N'PC'))
BEGIN
	EXEC ('CREATE PROCEDURE [dbo].[Parser_FileLoad]  AS RAISERROR(''NOT implemented yet.'', 16, 3);')
END
GO
PRINT ' Altering procedure [dbo].[Parser_FileLoad]'
GO
-- =============================================
-- Author:		Морозов М.А.
-- Create date: 20180702
-- Description:	Загрузка данных о файле в базу
-- =============================================
ALTER PROCEDURE [dbo].[Parser_FileLoad]
	@FileTypeCode varchar(100), -- Код из справочника file_types
	@FileName varchar(255),
	@OriginalFileName varchar(255) = NULL,
	@Path varchar(max),
	@Description varchar(max) = NULL,
	@Content text
AS
BEGIN
	SET NOCOUNT ON
	
	--*********************************************************
	-- Проверка входных данных
	--*********************************************************
	IF 1 = 1
	BEGIN 
		DECLARE @FileTypeId int = (SELECT id FROM file_types WHERE code = @FileTypeCode)
		IF @FileTypeId IS NULL
		BEGIN
			RAISERROR('Не найден тип отчета с кодом %s!', 16, 1, @FileTypeCode)
			RETURN
		END	
		DECLARE @FileStatusId int = (SELECT id FROM file_status WHERE code = 'Ready')
	END

	--*********************************************************
	-- Загружаем отчет
	--*********************************************************
	INSERT INTO file_logs(file_type_id, file_status_id, file_name, original_file_name, path, description, created_at, upload_date, content)
	SELECT
		@FileTypeId, @FileStatusId, @FileName, @OriginalFileName, @Path, @Description, GETDATE(), GETDATE(), @Content
END
GO

--BEGIN TRAN

--exec [Parser_FileLoad] 
--@FileTypeCode = 'Grade', 
--@FileName = 'Грейды по магазинам за 2018 год', @OriginalFileName = '', @Path = '', @Description = '',
--@Content  = 'Магазин;Январь;Февраль;Март;Апрель;Май;Июнь;Июль;Август;Сентябрь;Октябрь;Ноябрь;Декабрь
--Магазин № 66/003 (Мега Екб);21;2;3;24;25;6;7;8;9;1;3;4
--Магазин № 62/001 (Рязань);22;3;4;5;26;7;8;9;10;2;4;5
--Магазин № 77/003 (ТЦ Авиапарк);27;28;29;10;11;12;13;14;15;7;9;10'

----ROLLBACK TRAN
