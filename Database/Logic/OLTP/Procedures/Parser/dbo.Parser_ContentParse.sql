﻿IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Parser_ContentParse]') AND type IN (N'P', N'PC'))
BEGIN
	EXEC ('CREATE PROCEDURE [dbo].[Parser_ContentParse]  AS RAISERROR(''NOT implemented yet.'', 16, 3);')
END
GO
PRINT ' Altering procedure [dbo].[Parser_ContentParse]'
GO
-- =============================================
-- Author:		Морозов М.А.
-- Create date: 20180629
-- Description:	Разбор поля Content
-- =============================================
ALTER PROCEDURE [dbo].[Parser_ContentParse]
	@FileLogId int, -- Id файла
	@Separator varchar(1) = NULL
AS
BEGIN
	--********************************************************
	-- Получаем данные для загрузки
	--********************************************************
	IF 1 = 1
	BEGIN 
		DECLARE @Content varchar(MAX)
		DECLARE @VisitCode varchar(MAX)
		
		SELECT 
			@Content = Content,
			@VisitCode = FileTypeCode
		FROM vFileLog 
		WHERE 
			FileLogId = @FileLogId
		
		IF ISNULL(@Content, '') = '' 
		BEGIN
			RAISERROR('Данные загружаемого файла с ID = %d пустые либо указанный файл не найден!', 16, 1, @FileLogId)
			RETURN
		END	

		IF @Separator IS NULL
			SET @Separator = ';'
	END
	
	------------------------------------------------------------------------
	-- Нужна таблица из внешней ХП
	------------------------------------------------------------------------
	--IF OBJECT_ID('tempdb..#ParserData') IS NOT NULL DROP TABLE #ParserData
	--CREATE TABLE #tariff(
	--	Line int, 
	--	[1] varchar(1000), 
	--	[2] varchar(1000), 
	--	[3] varchar(1000), 
	--	[4] varchar(1000), 
	--	[5] varchar(1000), 
	--	[6] varchar(1000), 
	--	[7] varchar(1000), 
	--	[8] varchar(1000), 
	--	[9] varchar(1000), 
	--	[10] varchar(1000), 
	--	[11] varchar(1000), 
	--	[12] varchar(1000), 
	--	[13] varchar(1000), 
	--	[14] varchar(1000), 
	--	[15] varchar(1000) 
	--	)
	
	-- Строки
	IF OBJECT_ID('tempdb..#Data') IS NOT NULL DROP TABLE #Data;
	CREATE TABLE #Data
	(
		Line int IDENTITY(1,1) PRIMARY KEY,
		Value varchar(MAX)
	)

	INSERT INTO #Data(Value)
	SELECT 
		Value 
	FROM dbo.SplitStringXml(@Content, CHAR(13))

	DECLARE @RowLen int = (SELECT count(*) FROM #Data)
	IF (@RowLen = 1)
	BEGIN
		TRUNCATE TABLE #Data

		INSERT INTO #Data(Value)
		SELECT 
			Value 
		FROM dbo.SplitStringXml(@Content, CHAR(10))
	END

	-- столбцы
	IF OBJECT_ID('tempdb..#Row') IS NOT NULL DROP TABLE #Row;
	CREATE TABLE #Row
	(
		Line int,
		Col int,
		Value varchar(MAX)
	)

	INSERT INTO #Row(Line, Col, Value)
	SELECT 
		d.Line,
		ROW_NUMBER() OVER (PARTITION BY d.Line ORDER BY d.Line) AS Col,
		s.Value
	FROM #Data d
		CROSS APPLY
		(
			SELECT Value
			FROM dbo.SplitStringXml(d.Value, @Separator)
		) s
	WHERE d.Line > 1

	-- Разбор данных
	INSERT INTO #ParserData(Line, [1], [2], [3], [4], [5], [6], [7], [8], [9], [10], [11], [12], [13], [14], [15])
	SELECT 
		Line, 
		REPLACE(REPLACE([1], CHAR(10), ''), CHAR(9), '') AS [1], -- в начале строки удаляем символ новой строки
		[2], [3], [4], [5], [6], [7], [8], [9], [10], [11], [12], [13], [14], [15]
	FROM 
	(
		SELECT
			Line, Col, Value 
		FROM #Row
	) p	
	PIVOT(MIN(Value) FOR Col IN ([1], [2], [3], [4], [5], [6], [7], [8], [9], [10], [11], [12], [13], [14], [15])) pvt

	IF NOT EXISTS(SELECT * FROM #ParserData) AND @VisitCode <> 'Visit'
	BEGIN
		RAISERROR('[Parser_ContentParse]. Не удалось разобрать исходные данные файла - результат пустой (Id файла = %d)!', 16, 1, @FileLogId)
		RETURN
	END	
END
GO
