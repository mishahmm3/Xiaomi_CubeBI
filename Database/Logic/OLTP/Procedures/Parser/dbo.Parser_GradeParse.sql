﻿IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Parser_GradeParse]') AND type IN (N'P', N'PC'))
BEGIN
	EXEC ('CREATE PROCEDURE [dbo].[Parser_GradeParse]  AS RAISERROR(''NOT implemented yet.'', 16, 3);')
END
GO
PRINT ' Altering procedure [dbo].[Parser_GradeParse]'
GO
-- =============================================
-- Author:		Морозов М.А.
-- Create date: 20180702
-- Description:	Парсинг грейдов
-- =============================================
ALTER PROCEDURE [dbo].[Parser_GradeParse]
	@FileLogId int, -- Id файла
	@IsPassErrors bit = NULL -- пропускать обработку ошибок
AS
BEGIN
	SET NOCOUNT ON

	SET @IsPassErrors = ISNULL(@IsPassErrors, 0)

	--********************************************************
	-- Первичная загрузка даных Content
	--********************************************************
	IF 1 = 1
	BEGIN 
		EXEC Parser_ContentParse @FileLogId, ';'

		DECLARE @FileName varchar(255)
		SELECT @FileName = File_name FROM file_logs where Id = @FileLogId

		DECLARE @YEAR int = (SELECT SUBSTRING(@FileName, CHARINDEX('20', @FileName), 4))
	END

	--********************************************************
	-- Обработка ошибок
	--********************************************************
	IF 1 = 1
	BEGIN 
		--DECLARE @ColName int = 1
		--DECLARE @ColMonth1 int = 2 и т.д.
		
		SELECT 
			[1] AS ShopName,
			sh.Id AS ShopId
		INTO #Shop
		FROM #ParserData d
			LEFT JOIN shops sh
				ON sh.name = d.[1]

		DECLARE @ErrorList varchar(8000) = '';

		SET @ErrorList= ''
		SELECT
			@ErrorList =
				CASE 
					WHEN LEN(@ErrorList) = 0 THEN CAST(error AS varchar(100))  
					WHEN LEN(@ErrorList) > 0 THEN (@ErrorList + ',' + CAST(error AS varchar(100)))  
				END
		FROM 
		(
			SELECT DISTINCT
				ShopName AS error					
			FROM #Shop
			WHERE ShopId IS NULL
		) err	

		IF @ErrorList <> '' 
		BEGIN
			RAISERROR('Загрузка грейдов (Parser_GradeParse). В базе не найдено соответствие имен следующим магазинам из файла грейдов: %s.', 16, 1, @ErrorList)
			RETURN
		END	
	END

	--********************************************************
	-- Загрузка данных
	--********************************************************
	IF 1 = 1
	BEGIN 
		DECLARE @Now datetime = GETDATE()	
		DECLARE @PeriodTypeId int = (SELECT PeriodTypeId FROM PeriodType WHERE Code = 'M')	

		SELECT 
			ShopId,
			Month - 1 AS Month,
			g.GradeId
		INTO #data
		FROM 
			(
				SELECT
					sh.id AS ShopId, 
					ISNULL([1], 0) AS [1],
					ISNULL([2], 0) AS [2],
					ISNULL([3], 0) AS [3],
					ISNULL([4], 0) AS [4],
					ISNULL([5], 0) AS [5],
					ISNULL([6], 0) AS [6],
					ISNULL([7], 0) AS [7],
					ISNULL([8], 0) AS [8],
					ISNULL([9], 0) AS [9],
					ISNULL([10], 0) AS[10],
					ISNULL([11], 0) AS[11],
					ISNULL([12], 0) AS[12],
					ISNULL([13], 0) AS[13]
				FROM #ParserData d
					LEFT JOIN shops sh
						ON sh.name = d.[1]
			) src
			UNPIVOT
			(
				GradeValue FOR Month IN ([2], [3], [4], [5], [6], [7], [8], [9], [10], [11], [12], [13])
			) upv
			LEFT JOIN Grade g
				ON g.Code = upv.GradeValue

		MERGE GradeShop AS tgt
		USING
		(
			SELECT 
				@PeriodTypeId AS PeriodTypeId,
				CONVERT(varchar(10), DATEFROMPARTS(@YEAR, Month, 1), 120) AS DateFrom,
				ShopId, 
				GradeId
			FROM #data
		) AS src
		ON src.PeriodTypeId = tgt.PeriodTypeId
			AND src.DateFrom = tgt.DateFrom
			AND src.ShopId = tgt.ShopId
		WHEN MATCHED AND tgt.GradeId <> src.GradeId THEN  
			UPDATE SET tgt.GradeId = src.GradeId
		WHEN NOT MATCHED THEN
			INSERT (PeriodTypeId, DateFrom, ShopId, GradeId)
			VALUES (src.PeriodTypeId, src.DateFrom, src.ShopId, GradeId);
	END
END
GO

--BEGIN TRAN

--exec [Parser_FileParse] 'Grade', 2 
--SELECT * FROM GradeShop
------EXEC [dbo].[Parser_FileParse] 'Visit'
------SELECT * FROM visit_logs
----SELECT * FROM vFileLog
--ROLLBACK TRAN



