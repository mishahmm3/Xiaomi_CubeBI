﻿IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Parser_GradeWidthParse]') AND type IN (N'P', N'PC'))
BEGIN
	EXEC ('CREATE PROCEDURE [dbo].[Parser_GradeWidthParse]  AS RAISERROR(''NOT implemented yet.'', 16, 3);')
END
GO
PRINT ' Altering procedure [dbo].[Parser_GradeWidthParse]'
GO
-- =============================================
-- Author:		Морозов М.А.
-- Create date: 20180702
-- Description:	Парсинг данных по ширине
-- =============================================
ALTER PROCEDURE [dbo].[Parser_GradeWidthParse]
	@FileLogId int, -- Id файла
	@IsPassErrors bit = NULL -- пропускать обработку ошибок
AS
BEGIN
	SET NOCOUNT ON

	SET @IsPassErrors = ISNULL(@IsPassErrors, 0)

	--********************************************************
	-- Первичная загрузка даных Content
	--********************************************************
	IF 1 = 1
	BEGIN 
		EXEC Parser_ContentParse @FileLogId, ';'

		DECLARE @FileName varchar(255)
		SELECT @FileName = File_name FROM file_logs where Id = @FileLogId

		DECLARE @YEAR int = (SELECT SUBSTRING(@FileName, CHARINDEX('20', @FileName), 4))
	END

	--********************************************************
	-- Обработка ошибок
	--********************************************************
	IF 1 = 1
	BEGIN 
		--DECLARE @Grade int = 1
		--DECLARE @GroupId int = 2
		--DECLARE @ColMonth1 int = 3 и т.д.
		SELECT 
			[2] AS Name,
			gr.Id AS Id
		INTO #Err
		FROM #ParserData d
			LEFT JOIN product_groups gr
				ON gr.name = d.[2]

		DECLARE @ErrorList varchar(8000) = '';

		SET @ErrorList= ''
		SELECT
			@ErrorList =
				CASE 
					WHEN LEN(@ErrorList) = 0 THEN CAST(error AS varchar(100))  
					WHEN LEN(@ErrorList) > 0 THEN (@ErrorList + ';' + CAST(error AS varchar(100)))  
				END
		FROM 
		(
			SELECT DISTINCT
				Name AS error					
			FROM #Err
			WHERE Id IS NULL
		) err	

		IF @ErrorList <> ''  AND @IsPassErrors = 0
		BEGIN
			RAISERROR('Загрузка данных по ширине (Parser_GradeWidthParse). В базе не найдено соответствие имен для следующих групп товаров: %s.', 16, 1, @ErrorList)
			RETURN
		END	
	END

	--********************************************************
	-- Загрузка данных
	--********************************************************
	IF 1 = 1
	BEGIN 
		DECLARE @Now datetime = GETDATE()	
		DECLARE @PeriodTypeId int = (SELECT PeriodTypeId FROM PeriodType WHERE Code = 'M')	

		SELECT 
			g.GradeId,
			ProductGroupId,
			Month - 2 AS Month,
			Value
		INTO #data
		FROM 
			(
				SELECT
					gr.Id AS ProductGroupId, 
					[1], 
					ISNULL([2], 0) AS [2],
					ISNULL([3], 0) AS [3],
					ISNULL([4], 0) AS [4],
					ISNULL([5], 0) AS [5],
					ISNULL([6], 0) AS [6],
					ISNULL([7], 0) AS [7],
					ISNULL([8], 0) AS [8],
					ISNULL([9], 0) AS [9],
					ISNULL([10], 0) AS[10],
					ISNULL([11], 0) AS[11],
					ISNULL([12], 0) AS[12],
					ISNULL([13], 0) AS[13],
					ISNULL([14], 0) AS[14]
				FROM #ParserData d
					LEFT JOIN product_groups gr
						ON gr.name = d.[2]
			) src
			UNPIVOT
			(
				Value FOR Month IN ([3], [4], [5], [6], [7], [8], [9], [10], [11], [12], [13], [14])
			) upv
			LEFT JOIN Grade g
				ON g.Code = upv.[1]

		MERGE GradeWidthPlan AS tgt
		USING
		(
			SELECT 
				@PeriodTypeId AS PeriodTypeId,
				CONVERT(varchar(10), DATEFROMPARTS(@YEAR, Month, 1), 120) AS DateFrom,
				GradeId,
				ProductGroupId, 
				Value
			FROM #data
		) AS src
		ON src.PeriodTypeId = tgt.PeriodTypeId
			AND src.DateFrom = tgt.DateFrom
			AND src.ProductGroupId = tgt.ProductGroupId
			AND src.GradeId = tgt.GradeId
		WHEN MATCHED AND tgt.Value <> src.Value THEN  
			UPDATE SET tgt.Value = src.Value
		WHEN NOT MATCHED THEN
			INSERT (PeriodTypeId, DateFrom, ProductGroupId, GradeId, Value)
			VALUES (src.PeriodTypeId, src.DateFrom, src.ProductGroupId, src.GradeId, src.Value);
	END
END
GO

--exec [Parser_FileParse] 'GradeWidth', 2
