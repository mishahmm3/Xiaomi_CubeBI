﻿IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Parser_ShiftParse]') AND type IN (N'P', N'PC'))
BEGIN
	EXEC ('CREATE PROCEDURE [dbo].[Parser_ShiftParse]  AS RAISERROR(''NOT implemented yet.'', 16, 3);')
END
GO
PRINT ' Altering procedure [dbo].[Parser_ShiftParse]'
GO
-- =============================================
-- Author:		Морозов М.А.
-- Create date: 20180629
-- Description:	Парсинг посещений
-- =============================================
ALTER PROCEDURE [dbo].[Parser_ShiftParse]
	@FileLogId int, -- Id файла
	@IsPassErrors bit = NULL -- пропускать обработку ошибок
AS
BEGIN
	SET NOCOUNT ON
	
	SET @IsPassErrors = ISNULL(@IsPassErrors, 0)
	
	--********************************************************
	-- Первичная загрузка даных Content
	--********************************************************
	IF 1 = 1
	BEGIN 
		EXEC Parser_ContentParse @FileLogId

		-- Конвертируем 
		DECLARE @Col_ShopName int = 1
		DECLARE @Col_RoleName int = 2
		DECLARE @Col_UserName int = 3
		DECLARE @Col_DateFrom int = 4
		DECLARE @Col_ShiftName int = 5

		SELECT 
			[1] AS ShopName,
			sh.Id AS ShopId,
			[2] AS RoleName,
			r.Id AS RoleId,
			[3] AS UserName,
			u.Id AS UserId,
			[5] AS ShiftName,
			s.ShiftId,
			CONVERT(datetime, [4], 104) as DateFrom
		INTO #Data
		FROM #ParserData d
			LEFT JOIN Shops sh
				ON sh.Name = d.[1]			
			LEFT JOIN Roles r
				ON r.Name = d.[2]			
			LEFT JOIN Users u
				ON u.Name = d.[3]			
			LEFT JOIN Shift s
				ON s.Code = d.[5]			
	END

	--********************************************************
	-- Обработка ошибок
	--********************************************************
	IF 1 = 1
	BEGIN 
		DECLARE @ErrorList varchar(8000) = '';

		-- Проверка магазинов
		SET @ErrorList= ''
		SELECT
			@ErrorList = CASE 
					WHEN LEN(@ErrorList) = 0 THEN '"' + CAST(error AS varchar(100)) + '"'
					WHEN LEN(@ErrorList) > 0 THEN (@ErrorList + ';' + '"' + CAST(error AS varchar(100))) + '"' 
				END
		FROM 
		(	SELECT DISTINCT 
				ShopName AS error
			FROM #Data d
			WHERE 
				ShopId IS NULL
		) err	

		IF @ErrorList <> '' AND @IsPassErrors = 0
		BEGIN
			RAISERROR('Загрузка смен (Parser_ShiftParse). В базе не найдено соответствие имен для магазинов: %s.', 16, 1, @ErrorList)
			RETURN
		END	
		IF @IsPassErrors = 1
		BEGIN
			DELETE FROM #Data WHERE ShopId IS NULL
		END

		-- Проверка ролей
		SET @ErrorList= ''
		SELECT
			@ErrorList = CASE 
					WHEN LEN(@ErrorList) = 0 THEN '"' + CAST(error AS varchar(100)) + '"'
					WHEN LEN(@ErrorList) > 0 THEN (@ErrorList + ';' + '"' + CAST(error AS varchar(100))) + '"' 
				END
		FROM 
		(	SELECT DISTINCT 
				RoleName AS error
			FROM #Data d
			WHERE 
				RoleId IS NULL
		) err	

		IF @ErrorList <> '' AND @IsPassErrors = 0
		BEGIN
			RAISERROR('Загрузка смен (Parser_ShiftParse). В базе не найдено соответствие имен для ролей: %s.', 16, 1, @ErrorList)
			RETURN
		END	
		IF @IsPassErrors = 1
		BEGIN
			DELETE FROM #Data WHERE RoleId IS NULL
		END

		-- Проверка сотрудников
		SET @ErrorList= ''
		SELECT
			@ErrorList = CASE 
					WHEN LEN(@ErrorList) = 0 THEN '"' + CAST(error AS varchar(100)) + '"'
					WHEN LEN(@ErrorList) > 0 THEN (@ErrorList + ';' + '"' + CAST(error AS varchar(100))) + '"' 
				END
		FROM 
		(	SELECT DISTINCT 
				UserName AS error
			FROM #Data d
			WHERE 
				UserId IS NULL
		) err	

		IF @ErrorList <> '' AND @IsPassErrors = 0
		BEGIN
			RAISERROR('Загрузка смен (Parser_ShiftParse). В базе не найдено соответствие имен для сотрудников: %s.', 16, 1, @ErrorList)
			RETURN
		END	
		IF @IsPassErrors = 1
		BEGIN
			DELETE FROM #Data WHERE UserId IS NULL
		END

		-- Проверка смен
		SET @ErrorList= ''
		SELECT
			@ErrorList = CASE 
					WHEN LEN(@ErrorList) = 0 THEN '"' + CAST(error AS varchar(100)) + '"'
					WHEN LEN(@ErrorList) > 0 THEN (@ErrorList + ';' + '"' + CAST(error AS varchar(100))) + '"' 
				END
		FROM 
		(	SELECT DISTINCT 
				ShiftName AS error
			FROM #Data d
			WHERE 
				ShiftId IS NULL
		) err	

		IF @ErrorList <> '' AND @IsPassErrors = 0
		BEGIN
			RAISERROR('Загрузка смен (Parser_ShiftParse). В базе не найдено соответствие имен для смен: %s.', 16, 1, @ErrorList)
			RETURN
		END	
		IF @IsPassErrors = 1
		BEGIN
			DELETE FROM #Data WHERE ShiftId IS NULL
		END
	END

	--********************************************************
	-- Загрузка данных
	--********************************************************
	IF 1 = 1
	BEGIN 
		DECLARE @Now datetime = GETDATE()	

		MERGE StaffPlan AS tgt
		USING
		(
			SELECT 
				ShopId, UserId, RoleId, ShiftId, DateFrom
			FROM #Data
		) AS src
		ON src.ShopId = tgt.Shop_Id
			AND src.UserId = tgt.User_Id
			AND src.RoleId = tgt.Role_Id
			AND src.ShiftId = tgt.Shift_Id
			AND src.DateFrom = tgt.Date
		WHEN MATCHED THEN  
			UPDATE SET tgt.updated_at = @Now
		WHEN NOT MATCHED THEN
			INSERT (Shop_Id, User_Id, Role_Id, Shift_Id, Date, created_at)
			VALUES (src.ShopId, src.UserId, src.RoleId, src.ShiftId, src.DateFrom, @Now);
	END
END
GO

--EXEC Parser_FileParse 'Shift', 1, 1
