﻿IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Parser_FileParse]') AND type IN (N'P', N'PC'))
BEGIN
	EXEC ('CREATE PROCEDURE [dbo].[Parser_FileParse]  AS RAISERROR(''NOT implemented yet.'', 16, 3);')
END
GO
PRINT ' Altering procedure [dbo].[Parser_FileParse]'
GO
-- =============================================
-- Author:		Морозов М.А.
-- Create date: 20180629
-- Description:	Парсинг посещений
-- =============================================
ALTER PROCEDURE [dbo].[Parser_FileParse]
	@FileTypeCode varchar(100) = NULL, -- null - если обрабатывать все типы
	@ReadyFileCount int = NULL, -- сколько грузить готовых файлов, -1 - грузить все имеющиеся
	@IsPassErrors bit = NULL
AS
BEGIN
	SET NOCOUNT ON
	
	SET DATEFORMAT mdy

	-- Определяем список для загрузки
	IF 1 = 1
	BEGIN 
		SET @FileTypeCode = ISNULL(@FileTypeCode, '') 
		SET @IsPassErrors = ISNULL(@IsPassErrors, 0)

		SET @ReadyFileCount = ISNULL(@ReadyFileCount, 10)
		IF (@ReadyFileCount = -1)
			SET @ReadyFileCount = 1000 -- очень большое число

		DECLARE @NewFileStatusId int = (SELECT id FROM file_status WHERE code = 'Processed')
		IF @NewFileStatusId IS NULL AND @FileTypeCode <> ''
		BEGIN
			RAISERROR('Не найден Id статуса с кодом %s!', 16, 1, 'Processed')
			RETURN
		END	
		DECLARE @FileTypeDisplay varchar(100) = CASE WHEN @FileTypeCode = '' THEN 'Все типы файлов' ELSE @FileTypeCode END 	
		PRINT CONVERT(varchar(30), GETDATE(), 114) + ' Загрузка файлов с типом "' + @FileTypeDisplay + '" в количестве (плановое) - ' + CAST(@ReadyFileCount AS varchar(10)) + ' штук'
	END
	
	--*********************************************************
	-- В цикле грузим нужное кол-ва файлов указанного типа
	--*********************************************************
	IF 1 = 1
	BEGIN 
		--********************************************************
		-- Первичная загрузка даных Content
		--********************************************************
		IF OBJECT_ID('tempdb..#ParserData') IS NOT NULL DROP TABLE #ParserData
		CREATE TABLE #ParserData(
			Line int, 
			[1] varchar(1000), 
			[2] varchar(1000), 
			[3] varchar(1000), 
			[4] varchar(1000), 
			[5] varchar(1000), 
			[6] varchar(1000), 
			[7] varchar(1000), 
			[8] varchar(1000), 
			[9] varchar(1000), 
			[10] varchar(1000), 
			[11] varchar(1000), 
			[12] varchar(1000), 
			[13] varchar(1000), 
			[14] varchar(1000), 
			[15] varchar(1000) 
			)

		DECLARE @Counter int = 1
		DECLARE @FileLogId int
		DECLARE @FileName varchar(1000)
		DECLARE @CurFileTypeCode varchar(100)
		
		SELECT TOP (@ReadyFileCount) 
				FileLogId, FileName, FileTypeCode
		INTO #FileData
		FROM vFileLog
		WHERE
			FileStatusCode = 'Ready'
			AND (FileTypeCode = @FileTypeCode OR @FileTypeCode = '')

		DECLARE @FileCount int = (SELECT COUNT(*) FROM #FileData)
		SET @FileCount = ISNULL(@FileCount, 0)
		
		PRINT CONVERT(varchar(30), GETDATE(), 114) + ' Найдено необработанных файлов - ' + CAST(@FileCount AS varchar(10)) + ' штук'

		BEGIN TRY
		SET XACT_ABORT ON
		BEGIN TRAN 
		
			DECLARE main_cursor CURSOR FOR

			SELECT
				FileLogId, FileName, FileTypeCode
			FROM #FileData

			OPEN main_cursor;
			FETCH NEXT FROM main_cursor INTO @FileLogId, @FileName, @CurFileTypeCode
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
				PRINT CONVERT(varchar(30), GETDATE(), 114) + ' (' + CAST(@Counter AS varchar(10)) + ') Старт загрузки файла (тип - ' + @CurFileTypeCode + ') "' + @FileName + '" (Id = ' + CAST(@FileLogId AS varchar(10)) + ')'
			
				TRUNCATE TABLE #ParserData
				-- Ветвление по типам файлов
				IF @CurFileTypeCode = 'Visit' 
					EXEC Parser_VisitParse @FileLogId, @IsPassErrors
				ELSE IF @CurFileTypeCode = 'Grade' 
					EXEC Parser_GradeParse @FileLogId, @IsPassErrors
				ELSE IF @CurFileTypeCode = 'GradeWidth' 
					EXEC Parser_GradeWidthParse @FileLogId, @IsPassErrors
				ELSE IF @CurFileTypeCode = 'GradeDepth' 
					EXEC Parser_GradeDepthParse @FileLogId, @IsPassErrors
				ELSE IF @CurFileTypeCode = 'ProductGroup' 
					EXEC Parser_ProductGroupParse @FileLogId, @IsPassErrors
				ELSE IF @CurFileTypeCode = 'Shift' 
					EXEC Parser_ShiftParse @FileLogId, @IsPassErrors
				ELSE IF @CurFileTypeCode = 'ShopStaff' 
					EXEC Parser_ShopStaffParse @FileLogId, @IsPassErrors
				ELSE
				BEGIN
					CLOSE main_cursor; DEALLOCATE main_cursor;
				
					PRINT 'Ошибка!!! Файл с типом = ' + @FileTypeCode + ' не найден!'
					RETURN
				END
			
				-- Ставит новый статус файла
				UPDATE file_logs SET file_status_id = @NewFileStatusId, parse_date = GETDATE()
				WHERE Id = @FileLogId

				PRINT CONVERT(varchar(30), GETDATE(), 114) + ' Успешно завершено'

				SET @Counter = @Counter + 1
				FETCH NEXT FROM main_cursor INTO @FileLogId, @FileName, @CurFileTypeCode
			END
			CLOSE main_cursor; DEALLOCATE main_cursor;

			PRINT CONVERT(varchar(30), GETDATE(), 114) + ' Завершено!'
	
			SELECT 1 AS Result
		COMMIT TRAN
		END TRY
		BEGIN CATCH
			IF(XACT_STATE() <> 0)
				ROLLBACK TRAN

			DECLARE @LogMessage varchar(4000) = ISNULL(ERROR_MESSAGE(), '')
			PRINT CONVERT(varchar(30), GETDATE(), 114) + ' Завершено с ошибкой: ' + @LogMessage

			RAISERROR(@LogMessage, 16, 1)
			SELECT 0 -- Ошибка!!!
		END CATCH
	END
END
GO
--EXEC Parser_FileParse 'Visit'
