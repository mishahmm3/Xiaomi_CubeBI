﻿IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Parser_ProductGroupParse]') AND type IN (N'P', N'PC'))
BEGIN
	EXEC ('CREATE PROCEDURE [dbo].[Parser_ProductGroupParse]  AS RAISERROR(''NOT implemented yet.'', 16, 3);')
END
GO
PRINT ' Altering procedure [dbo].[Parser_ProductGroupParse]'
GO
-- =============================================
-- Author:		Морозов М.А.
-- Create date: 20180629
-- Description:	Парсинг посещений
-- =============================================
ALTER PROCEDURE [dbo].[Parser_ProductGroupParse]
	@FileLogId int, -- Id файла
	@IsPassErrors bit = NULL -- пропускать обработку ошибок
AS
BEGIN
	SET NOCOUNT ON
	
	SET @IsPassErrors = ISNULL(@IsPassErrors, 0)
	DECLARE @Now datetime = GETDATE()	
	
	--********************************************************
	-- Первичная загрузка даных Content
	--********************************************************
	IF 1 = 1
	BEGIN 
		EXEC Parser_ContentParse @FileLogId
	END
	
	--********************************************************
	-- Сначала грузим справочник групп
	--********************************************************
	IF 1 = 1
	BEGIN 
		INSERT INTO product_groups(name, created_at)
		SELECT 
			GroupName, @Now 
		FROM 
		(	SELECT DISTINCT 
				[1] AS GroupName
			FROM #ParserData d
		) d
			LEFT JOIN product_groups pg
				ON pg.name = d.GroupName
		WHERE
			pg.id IS NULL
	END
	
	--********************************************************
	-- Обработка ошибок
	--********************************************************
	IF 1 = 1
	BEGIN 
		DECLARE @ColGroup int = 1
		DECLARE @ColProduct int = 2

		SELECT
			[1] AS GroupName,
			pg.id AS ProductGroupId,
			[2] AS ProductName,
			p.Id  AS ProductId
		INTO #data
		FROM #ParserData d
			LEFT JOIN product_groups pg
				ON pg.name = d.[1]
			LEFT JOIN products p
				ON p.name = d.[2]

		DECLARE @ErrorList varchar(8000) = '';
		
		SET @ErrorList= ''
		SELECT
			@ErrorList =
				CASE 
					WHEN LEN(@ErrorList) = 0 THEN '"' + CAST(error AS varchar(100)) + '"'
					WHEN LEN(@ErrorList) > 0 THEN (@ErrorList + ';' + '"' + CAST(error AS varchar(100))) + '"' 
				END
		FROM 
		(
			SELECT DISTINCT 
				ProductName AS error
			FROM #data d
			WHERE 
				ProductId IS NULL
		) err	
		
		IF @ErrorList <> '' AND @IsPassErrors = 0
		BEGIN
			RAISERROR('Загрузка группы товаров (Parser_ProductGroupParse). В базе не найдено соответствие имен для товаров: %s.', 16, 1, @ErrorList)
			RETURN
		END	
	END

	--********************************************************
	-- Загрузка данных
	--********************************************************
	IF 1 = 1
	BEGIN 
		INSERT INTO product_group_products(product_group_id, product_id)
		SELECT  
			d.ProductGroupId, 
			d.ProductId
		FROM #data d
			LEFT JOIN product_group_products p
				ON p.product_group_id = d.ProductGroupId
					AND p.product_id = d.ProductId
		WHERE
			p.product_group_id IS NULL
	END
END
GO

--EXEC [dbo].[Parser_FileParse] 'ProductGroup', 1
