﻿IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Parser_ShopStaffParse]') AND type IN (N'P', N'PC'))
BEGIN
	EXEC ('CREATE PROCEDURE [dbo].[Parser_ShopStaffParse]  AS RAISERROR(''NOT implemented yet.'', 16, 3);')
END
GO
PRINT ' Altering procedure [dbo].[Parser_ShopStaffParse]'
GO
-- =============================================
-- Author:		Морозов М.А.
-- Create date: 20180709
-- Description:	Загрузка штатки магазинов
-- =============================================
ALTER PROCEDURE [dbo].[Parser_ShopStaffParse]
	@FileLogId int, -- Id файла
	@IsPassErrors bit = NULL -- пропускать обработку ошибок
AS
BEGIN
	SET NOCOUNT ON

	SET @IsPassErrors = ISNULL(@IsPassErrors, 0)
	
	--********************************************************
	-- Первичная загрузка даных Content
	--********************************************************
	IF 1 = 1
	BEGIN 
		EXEC Parser_ContentParse @FileLogId, ';'

		DECLARE @FileName varchar(255)
		SELECT @FileName = File_name FROM file_logs where Id = @FileLogId

		DECLARE @Year int =  TRY_CONVERT(int, (SELECT SUBSTRING(@FileName, CHARINDEX('20', @FileName), 4)))
		DECLARE @Month int = TRY_CONVERT(int, (SELECT SUBSTRING(@FileName, CHARINDEX('20', @FileName) + 4, 2)))

		IF @Year IS NULL OR @Month IS NULL
		BEGIN
			RAISERROR('Загрузка штатного расписания (Parser_ShopStaffParse). Не удалось получить месяц и год из имени %s. Ожидается формат имени - "yyyyMM Штатное расписание магазинов.csv"', 16, 1, @FileName)
			RETURN
		END	

		-- Начало периода для данных попериодовок
		DECLARE @DateFrom date = CAST((SELECT SUBSTRING(@FileName, CHARINDEX('20', @FileName), 6) + '01') AS date)
	END
	--********************************************************
	-- Обработка ошибок
	--********************************************************
	IF 1 = 1
	BEGIN 
		--ShopName- 1
		--StaffCount - 2
		
		SELECT 
			[1] AS ShopName,
			sh.Id AS ShopId,
			[2] AS StaffCount
		INTO #Data
		FROM #ParserData d
			LEFT JOIN shops sh
				ON sh.name = d.[1]

		DECLARE @ErrorList varchar(8000) = '';

		SET @ErrorList= ''
		SELECT
			@ErrorList =
				CASE 
					WHEN LEN(@ErrorList) = 0 THEN CAST(error AS varchar(100))  
					WHEN LEN(@ErrorList) > 0 THEN (@ErrorList + ',' + CAST(error AS varchar(100)))  
				END
		FROM 
		(
			SELECT DISTINCT
				ShopName AS error					
			FROM #Data
			WHERE ShopId IS NULL
		) err	

		IF @ErrorList <> '' AND @IsPassErrors = 0
		BEGIN
			RAISERROR('Загрузка штатного расписания (Parser_ShopStaffParse). В базе не найдено соответствие имен следующим магазинам из файла грейдов: %s.', 16, 1, @ErrorList)
			RETURN
		END	

		IF @IsPassErrors = 1
		BEGIN
			DELETE FROM #Data WHERE ShopId IS NULL
		END

	END

	--********************************************************
	-- Загрузка данных
	--********************************************************
	IF 1 = 1
	BEGIN 
		DECLARE @Now datetime = GETDATE()	
		DECLARE @PeriodTypeId int = (SELECT PeriodTypeId FROM PeriodType WHERE Code = 'M')	
		DECLARE @ParameterId int = (SELECT ParameterId FROM Parameter WHERE Code = 'ShopStaff')	
		
		MERGE ParameterPeriodValue AS tgt
		USING
		(
			SELECT  
				@DateFrom AS DateFrom,
				@ParameterId AS ParameterId,
				@PeriodTypeId AS PeriodTypeId,
				ShopId AS EntityId,
				StaffCount AS Value
			FROM #Data d
		) AS src
		ON src.DateFrom = tgt.DateFrom
			AND src.ParameterId = tgt.ParameterId
			AND src.PeriodTypeId = tgt.PeriodTypeId
			AND src.EntityId = tgt.EntityId
		WHEN MATCHED AND tgt.Value <> src.Value THEN  
			UPDATE SET tgt.Value = src.Value
		WHEN NOT MATCHED THEN
			INSERT (DateFrom, ParameterId, PeriodTypeId, EntityId, Value)
			VALUES (src.DateFrom, src.ParameterId, src.PeriodTypeId, src.EntityId, src.Value);
	END
END
GO

--exec Parser_FileParse 'ShopStaff'--, 1, 1
