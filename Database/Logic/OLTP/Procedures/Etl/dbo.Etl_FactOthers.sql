﻿IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Etl_FactOthers]') AND type IN (N'P', N'PC'))
BEGIN
	EXEC ('CREATE PROCEDURE [dbo].[Etl_FactOthers]  AS RAISERROR(''NOT implemented yet.'', 16, 3);')
END
GO
PRINT ' Altering procedure [dbo].[Etl_FactOthers]'
GO
-- =============================================
-- Author:		Морозов М.А.
-- Create date: 20180619
-- Description:	Разные данные
-- =============================================
ALTER PROCEDURE [dbo].Etl_FactOthers
AS
BEGIN
	SET NOCOUNT ON
	
	-- Переменные и вспомогательные таблицы
	IF 1 = 1
	BEGIN 
		DECLARE @Now datetime = GETDATE()
	END

	--******************************************************
	PRINT 'Сумма инкассации'
	--******************************************************
	IF 1 = 1
	BEGIN 
		--Таблица: Документ.РасходныйКассовыйОрдер, Имя таблицы хранения: Document248, Назначение: Основная
		--- поля: 
		--   Ссылка (ID)
		--   ПометкаУдаления (Marked)
		--   Дата (Date_Time)
		--   Номер (Number)
		--   Проведен (Posted)
		--   Организация (Fld5133)
		--   КассаККМ (Fld5134)
		--   Касса (Fld5136)
		--   КассаПолучатель (Fld5137)
		--   ХозяйственнаяОперация (Fld5138)
		--   ОбластьДанныхОсновныеДанные (Fld516)
		DECLARE @OperId [binary](16) = 0xBDFE834DACB618F748A98AA3F604F718 -- Операция "Сдача ДС в банк"

		SELECT 
			DATEADD(YEAR, -2000, CAST(_Date_Time as date)) AS DateFrom, -- Дата - 2000 лет
			sh.id AS ShopId, 
			SUM(_Fld5148) AS EncashmentValue
						--_Number,
						--_Fld5136RRef AS Касса,
						--d._IdRRef AS [Summa_row_id]
		INTO #DataSumma
		FROM X.rt_smart.[dbo]._Document248 d
				--Таблица: Справочник.Кассы, Имя таблицы хранения: Reference65, Назначение: Основная
				LEFT JOIN X.rt_smart.[dbo]._Reference65 cass
					ON cass._IdRRef = d._Fld5136RRef 
				LEFT JOIN shops sh
					ON sh.row_id = cass._Fld1343RRef
		WHERE 
			d._Fld5138rref = @OperId
			--??????? AND _Posted = 1
		GROUP BY 
			DATEADD(YEAR, -2000, CAST(_Date_Time as date)), sh.id

							--		--AND 
							--		--_Date_time >= '4018-07-10 22:00:00' AND _Date_time < '4018-07-10 23:00:00'
							--		--SELECT * FROM X.rt_smart.[dbo]._Document248 d
							--		--SELECT _Fld5134RRef, _Fld5136RRef, * FROM X.rt_smart.[dbo]._Document248
							--		--		WHERE 
							--		--			_Fld5138rref = 0xBDFE834DACB618F748A98AA3F604F718
							--		--SELECT * FROM X.rt_smart.[dbo]._Reference65  WHERE _IdRRef =  0x80F500505601926011E79DDE4AC12928
	END
	
	--******************************************************
	-- Остатки ДС в кассе
	--******************************************************
	IF 1 = 1
	BEGIN 
		--!!!!!!!ДС в магазине надо считать как сумму операций по двум регистрам:
		--1)      РегистрНакопления.ДенежныеСредстваНаличные – Ведение учёта по кассе (сейф)
		--Таблица: РегистрНакопления.ДенежныеСредстваНаличные, Имя таблицы хранения: AccumRg8749, Назначение: Основная
		--   Период (Period)
		--   Регистратор (Recorder)
		--   НомерСтроки (LineNo)
		--   Активность (Active)
		--   ВидДвижения (RecordKind)
		--   Организация (Fld8750)
		--   Магазин (Fld8751)
		--   Касса (Fld8752)
		--   ДоговорПлатежногоАгента (Fld8753)
		--   Сумма (Fld8754)
		--   СтатьяДвиженияДенежныхСредств (Fld8755)
		--   ОбластьДанныхОсновныеДанные (Fld516)
		-- Получить вид операции по регистру РегистрНакопления.ДенежныеСредстваНаличные можете из справочника СтатьяДвиженияДенежныхСредств, в базе: _Fld8755RRef
		--Таблица: Справочник.СтатьиДвиженияДенежныхСредств, Имя таблицы хранения: Reference149, Назначение: Основная
		SELECT 
			DATEADD(YEAR, -2000, CAST(_Period as date)) AS DateFrom, -- Дата - 2000 лет
			sh.Id AS  ShopId,
			SUM(_Fld8754) AS RemainsValue
			--vid._Code,
			--vid._Description,
			--sh.Name AS ShopName,
			--data._RecorderRRef AS Remains_row_id
		INTO #Remains1
		FROM X.rt_smart.[dbo].[_AccumRg8749] data
			LEFT JOIN shops sh
				ON sh.row_id = data._Fld8751RRef
			LEFT JOIN X.rt_smart.[dbo].[_Reference149] vid
				ON vid._IDRRef = data._Fld8755RRef
		GROUP BY 
			DATEADD(YEAR, -2000, CAST(_Period as date)), sh.Id
			
						--SELECT * FROM X.rt_smart.[dbo].[_Reference149] 
						--SELECT * FROM X.rt_smart.[dbo]._Reference149
						--SELECT * FROM X.rt_smart.[dbo]._Fld8755RRef
						--SELECT * FROM X.rt_smart.[dbo]._AccumRg8749
		--2)      РегистрНакопления.ДенежныеСредстваККМ – Ведение учета по ККТ(денежный ящик)
		--Таблица: РегистрНакопления.ДенежныеСредстваККМ, Имя таблицы хранения: AccumRg8735, Назначение: Основная
		--- поля: 
		--   Период (Period)
		--   Регистратор (Recorder)
		--   НомерСтроки (LineNo)
		--   Активность (Active)
		--   ВидДвижения (RecordKind)
		--   КассаККМ (Fld8736)
		--   ДоговорПлатежногоАгента (Fld8737)
		--   Сумма (Fld8738)
		SELECT 
			DATEADD(YEAR, -2000, CAST(_Period as date)) AS DateFrom, -- Дата - 2000 лет
			cd.ShopId,
			SUM(_Fld8738) AS RemainsValue
		INTO #Remains2
		FROM X.rt_smart.[dbo]._AccumRg8735 d
			LEFT JOIN vDimCashDesk cd
				ON cd.row_id = 	_Fld8736RRef
		WHERE 
			_RecordKind = 1
		GROUP BY 
			DATEADD(YEAR, -2000, CAST(_Period as date)), cd.ShopId
	END
	
	--******************************************************
	PRINT  'Сводим вместе остатки и сумму инкассации'
	--******************************************************
	IF 1 = 1
	BEGIN 
		SELECT 
			ISNULL(d1.DateFrom, d2.DateFrom) AS DateFrom, 
			ISNULL(d1.ShopId, d2.ShopId) AS ShopId, 
			ISNULL(d1.RemainsValue, 0) + ISNULL(d2.RemainsValue, 0) AS RemainsValue
		INTO #RemainsAll
		FROM #Remains1 d1
			FULL JOIN #Remains2 d2
				ON d2.DateFrom = d1.DateFrom
					AND d2.ShopId = d1.ShopId

		SELECT 
			ISNULL(d1.DateFrom, d2.DateFrom) AS DateFrom, 
			ISNULL(d1.ShopId, d2.ShopId) AS ShopId, 
			ISNULL(d1.EncashmentValue, 0) AS EncashmentValue, 
			ISNULL(d2.RemainsValue, 0) AS RemainsValue
		INTO #Res
		FROM #DataSumma d1
			FULL JOIN #RemainsAll d2
				ON d2.DateFrom = d1.DateFrom
					AND d2.ShopId = d1.ShopId

		MERGE ShopEncashment AS tgt
		USING
		(
			SELECT 
				DateFrom, ShopId, EncashmentValue, RemainsValue
			FROM #Res
			WHERE ShopId IS NOT NULL	
		) AS src
		ON src.DateFrom = tgt.DateFrom
			AND src.ShopId = tgt.ShopId
		WHEN MATCHED AND NOT (src.EncashmentValue = tgt.EncashmentValue AND src.RemainsValue = tgt.RemainsValue) THEN  
			UPDATE SET 
				tgt.EncashmentValue = tgt.EncashmentValue, tgt.RemainsValue = tgt.RemainsValue
		WHEN NOT MATCHED THEN
			INSERT (DateFrom, ShopId, EncashmentValue, RemainsValue)
			VALUES (src.DateFrom, src.ShopId, src.EncashmentValue, src.RemainsValue);
	END
	
	--******************************************************
	PRINT  'Норматив по инкассации'
	--******************************************************
	IF 1 = 1
	BEGIN 
		--Таблица: РегистрСведений._ЛимитыНаличныхДенег, Имя таблицы хранения: InfoRg10861, Назначение: Основная
		--   Период (Period)
		--   Регистратор (Recorder)
		--   НомерСтроки (LineNo)
		--   Активность (Active)
		--   Магазин (Fld10862)
		--   Сумма (Fld10863)
		--   ОбластьДанныхОсновныеДанные (Fld516)
		SELECT 
			DATEADD(YEAR, -2000, CAST(_Period as date)) AS DateFrom, -- Дата - 2000 лет
			sh.Id AS  ShopId,
			_Fld10863 AS Norma
		INTO #Data
		FROM X.rt_smart.[dbo]._InfoRg10861 data
			LEFT JOIN shops sh
				ON sh.row_id = data._Fld10862RRef

		DECLARE @PeriodTypeId int = (SELECT PeriodTypeId FROM PeriodType WHERE Code = 'Any')	
		DECLARE @ParameterId int = (SELECT ParameterId FROM Parameter WHERE Code = 'EncashmentNorma')	
		
		MERGE ParameterPeriodValue AS tgt
		USING
		(
			SELECT 
				DateFrom, 
				@ParameterId AS ParameterId,
				@PeriodTypeId AS PeriodTypeId,
				ShopId AS EntityId,
				Norma AS Value
			FROM #Data m
		) AS src
		ON src.DateFrom = tgt.DateFrom
			AND src.ParameterId = tgt.ParameterId
			AND src.PeriodTypeId = tgt.PeriodTypeId
			AND src.EntityId = tgt.EntityId
		WHEN MATCHED AND tgt.Value <> src.Value THEN  
			UPDATE SET tgt.Value = src.Value
		WHEN NOT MATCHED THEN
			INSERT (DateFrom, ParameterId, PeriodTypeId, EntityId, Value)
			VALUES (src.DateFrom, src.ParameterId, src.PeriodTypeId, src.EntityId, src.Value);
	END

	DROP TABLE #DataSumma
	DROP TABLE #RemainsAll
	DROP TABLE #Remains1
	DROP TABLE #Remains2
	DROP TABLE #Res
END
GO

--DELETE FROM ShopEncashment
--exec [dbo].Etl_FactOthers

