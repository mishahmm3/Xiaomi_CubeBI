﻿IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Etl_FactAll]') AND type IN (N'P', N'PC'))
BEGIN
	EXEC ('CREATE PROCEDURE [dbo].[Etl_FactAll]  AS RAISERROR(''NOT implemented yet.'', 16, 3);')
END
GO
PRINT ' Altering procedure [dbo].[Etl_FactAll]'
GO
-- =============================================
-- Author:		Морозов М.А.
-- Create date: 2018-06-06
-- Description:	Общая ХП загрузки фактов в куб
-- =============================================
ALTER PROCEDURE [dbo].[Etl_FactAll]
AS
BEGIN
	PRINT 'Старт загрузки фактов куба - Etl_FactAll'
	PRINT GETDATE()

	EXEC [dbo].Etl_FactCheck
	
	EXEC [dbo].Etl_FactProductMove

	EXEC [dbo].Etl_FactBonusPoints
	
	EXEC dbo.Etl_FactProductUnload

	EXEC dbo.Etl_FactOthers
	
	EXEC Etl_FactMovement
	
	PRINT 'Завершение загрузки фактов куба - Etl_FactAll'
	PRINT GETDATE()
END
GO
