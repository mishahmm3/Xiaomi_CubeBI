﻿IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Etl_All]') AND type IN (N'P', N'PC'))
BEGIN
	EXEC ('CREATE PROCEDURE [dbo].[Etl_All]  AS RAISERROR(''NOT implemented yet.'', 16, 3);')
END
GO
PRINT ' Altering procedure [dbo].[Etl_All]'
GO
-- =============================================
-- Author:		Морозов М.А.
-- Create date: 2018-06-06
-- Description:	Общая ХП загрузки всех данных в куб
-- =============================================
ALTER PROCEDURE [dbo].[Etl_All]
AS
BEGIN
	PRINT 'Старт загрузки всех данных куба - Etl_All'
	PRINT GETDATE()

	EXEC [dbo].[Etl_DimAll]
	
	EXEC [dbo].[Etl_FactAll]

	PRINT 'Завершение загрузки всех данных куба - Etl_All'
	PRINT GETDATE()
END
GO
