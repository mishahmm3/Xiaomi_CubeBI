#include "dbo.Etl_DimAll.sql"

#include "dbo.Etl_FactProductMove.sql"
#include "dbo.Etl_FactCheck.sql"
#include "dbo.Etl_FactBonusPoints.sql"
#include "dbo.Etl_FactProductUnload.sql"
#include "dbo.Etl_FactOthers.sql"
#include "dbo.Etl_FactMovement.sql"

#include "dbo.Etl_FactAll.sql"
#include "dbo.Etl_All.sql"
