﻿IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Etl_FactProductMove]') AND type IN (N'P', N'PC'))
BEGIN
	EXEC ('CREATE PROCEDURE [dbo].[Etl_FactProductMove]  AS RAISERROR(''NOT implemented yet.'', 16, 3);')
END
GO
PRINT ' Altering procedure [dbo].[Etl_FactProductMove]'
GO
-- =============================================
-- Author:		Ternovskiy A.V.
-- Create date: 2018-06-02
-- Description:	Движение товаров
-- =============================================
ALTER PROCEDURE [dbo].[Etl_FactProductMove]
AS
BEGIN
	PRINT 'Старт загрузки измерений куба - Etl_FactProductMove'
	PRINT GETDATE()

	-- Переменные и вспомогательные таблицы
	IF 1 = 1
	BEGIN 
		DECLARE @Now datetime = GETDATE()
	END

	-- Заливка движения по складу
	IF 1 = 1
	BEGIN 
		PRINT 'Заливка движения по складу'

		--Таблица: РегистрНакопления.ТоварыНаСкладах, Имя таблицы хранения: AccumRg9072, Назначение: Основная
		--- поля: 
		--   Период (Period)
		--   Регистратор (Recorder)
		--   НомерСтроки (LineNo)
		--   Активность (Active)
		--   ВидДвижения (RecordKind)
		--   Склад (Fld9073)
		--   Номенклатура (Fld9074)
		--   Характеристика (Fld9075)
		--   Количество (Fld9076)
		--   Резерв (Fld9077)
		--   ор_Себестоимость (Fld9078)
		--   АналитикаХозяйственнойОперации (Fld9079)
		--   КодСтроки (Fld9080)
		--   ОбластьДанныхОсновныеДанные (Fld516)
		SELECT	
			_Fld9076,
			_Period,
			_Fld9074RRef,
			_Fld9073RRef,
			_RecordKind
		INTO #product_remains 	
		FROM X.rt_smart.[dbo].[_AccumRg9072]
		
		TRUNCATE TABLE product_remains
		
		INSERT INTO product_remains(product_id, warehouse_id, operation_id, [date], [count], created_at)
		SELECT	
			p.id,
			w.id,
			_RecordKind,
			dateadd(year, -2000, CAST(_Period as datetime)),
			_Fld9076,
			@Now
		FROM #product_remains r
			INNER JOIN products p 
				ON r._Fld9074RRef = p.row_id
			INNER JOIN warehouses w 
				ON r._Fld9073RRef = w.row_id
		DROP TABLE #product_remains
	END
	
	PRINT 'Завершение загрузки измерений куба - Etl_FactProductMove'
	PRINT GETDATE()
END
GO

------begin tran
----exec [dbo].Etl_FactProductMove
----select * from product_remains
------rollback tran



--exec [dbo].[Etl_FactProductMove]

--SELECT TOP 11 * FROM X.rt_smart.[dbo].[_AccumRg9072]
--order by _Fld9076 desc
