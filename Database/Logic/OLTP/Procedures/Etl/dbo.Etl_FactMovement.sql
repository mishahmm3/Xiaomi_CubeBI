﻿IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Etl_FactMovement]') AND type IN (N'P', N'PC'))
BEGIN
	EXEC ('CREATE PROCEDURE [dbo].[Etl_FactMovement]  AS RAISERROR(''NOT implemented yet.'', 16, 3);')
END
GO
PRINT ' Altering procedure [dbo].[Etl_FactMovement]'
GO
-- =============================================
-- Author:		Морозов М.А.
-- Create date: 20180711
-- Description:	Перемещение товаров между складами  
-- =============================================
ALTER PROCEDURE [dbo].[Etl_FactMovement]
AS
BEGIN
	SET NOCOUNT ON
	PRINT 'Старт загрузки данных - Etl_FactMovement'
	PRINT GETDATE()

	-- Переменные и вспомогательные таблицы
	IF 1 = 1
	BEGIN 
		DECLARE @Now datetime = GETDATE()
	END

	--**************************************************************
	--1.Документ.ПеремещениеТоваров, Document238 -- мастер
	--2.Документ.ПеремещениеТоваров.Товары, Document238.VT4860 - детали мастера
	--3.РегистрНакопления.ТоварыКОтгрузке, AccumRg9039 - данные по отгрузке
	--4.РегистрНакопления.ТоварыКПоступлению, Имя таблицы хранения: AccumRg9060 - данные по поступлению
	--**************************************************************
	IF 1 = 1
	BEGIN 
		--Таблица: Документ.ПеремещениеТоваров, Имя таблицы хранения: Document238, Назначение: Основная
		--   Дата (Date_Time)
		--    (NumberPrefix)
		--   Номер (Number)
		--   Проведен (Posted)
		--   ДанныеТранспортногоРаздела (Fld4844)
		--   ДокументОснование (Fld4845)
		--				Комментарий (Fld4846)
		--   МагазинОтправитель (Fld4847)
		--   МагазинПолучатель (Fld4848)
		--   Организация (Fld4849)
		--   ОрганизацияПолучатель (Fld4850)
		--				Ответственный (Fld4851)
		--				 ПредставлениеДокументаОснования (Fld4852)
		--   СкладОтправитель (Fld4853)
		--   СкладПолучатель (Fld4854)
		--   УдалитьСтатусОбработки_ЕГАИС (Fld4855)
		--   СуммаДокумента (Fld4856)
		--   ТТНИсходящаяЕГАИС (Fld4857)
		--   ТТНВходящаяЕГАИС (Fld4858)
		--   УдалитьТоварноТранспортнаяНакладнаяЕГАИСИсходящая (Fld4859)
		--   ОбластьДанныхОсновныеДанные (Fld516)
		SELECT 
			DATEADD(HOUR, DATEPART(HOUR, _Date_Time), CAST(DATEADD(YEAR, -2000, CAST(_Date_Time as date)) AS Datetime)) AS DateFrom,
			_Number AS DocNumber,
			ISNULL(_Posted, 0) AS IsPosted,
			wFrom.WarehouseId AS WarehouseFromId,
			wTo.WarehouseId AS WarehouseToId,
			d._IDRRef AS row_id
		INTO #DataMaster
		FROM X.rt_smart.[dbo].[_Document238] d
			LEFT JOIN vDimWarehouse wFrom 
				ON wFrom.row_id = d._Fld4853RRef 
			LEFT JOIN vDimWarehouse wto
				ON wto.row_id = d._Fld4854RRef 

		--!!! Предполагаем, что документы не меняются!!!
		INSERT INTO MovementDoc(DateFrom, DocNumber, IsPosted, WarehouseFromId, WarehouseToId, row_id)
		SELECT 
			d.DateFrom, d.DocNumber, d.IsPosted, d.WarehouseFromId, d.WarehouseToId, d.row_id	 
		FROM #DataMaster d
			LEFT JOIN MovementDoc md
				ON md.row_id = d.row_id
		WHERE 
			md.row_id IS NULL
												--			and d.DocNumber = 'ЦОUT-000149'
												--SELECT * FROM  X.rt_smart.[dbo].[_Document238] d
												--where _Posted = 0
												--_IDRRef in (0x80FD00505601926011E7F887066DD8D1, 0x80FD00505601926011E7F91505F49AFE)

		-- Детальная таблица
		--Таблица: Документ.ПеремещениеТоваров.Товары, Имя таблицы хранения: Document238.VT4860, Назначение: ТабличнаяЧасть
		--   НомерСтроки (LineNo4861)
		--   Номенклатура (Fld4862)
		--   Характеристика (Fld4863)
		--   Количество (Fld4864)
		--   Цена (Fld4865)
		--   Сумма (Fld4866)
		--   КлючСвязиСерийныхНомеров (Fld4867)
		--   Упаковка (Fld4868)
		--   КоличествоУпаковок (Fld4869)
		--   ор_ЗаказНаПеремещение (Fld4870)
		--   СтатусУказанияСерий (Fld4871)
		--   СтатусУказанияСерийОтправитель (Fld4872)
		--   СтатусУказанияСерийПолучатель (Fld4873)
		SELECT 
			_Document238_IDRRef AS MovementDocId,
			p.id AS ProductId,
			SUM(_Fld4864) AS ProductCount
		INTO #DataDetaild
		FROM X.rt_smart.[dbo].[_Document238_VT4860] d
			LEFT JOIN products p 
				ON p.row_id = d._Fld4862RRef 
		GROUP BY 
			_Document238_IDRRef, p.id			
		
		--!!! Предполагаем, что документы не меняются!!!
		INSERT INTO MovementDocProduct(MovementDocId, ProductId, ProductCount)
		SELECT 
			m.MovementDocId, dd.ProductId, dd.ProductCount	 
		FROM #DataDetaild dd
			INNER JOIN MovementDoc m
				ON m.row_id = dd.MovementDocId
			LEFT JOIN MovementDocProduct md
				ON md.MovementDocId = m.MovementDocId
					AND md.ProductId = dd.ProductId
		WHERE 
			md.MovementDocId IS NULL
	END

	--**************************************************************
	--Таблица: РегистрНакопления.ТоварыКПоступлению, Имя таблицы хранения: AccumRg9060, Назначение: Основная
	--**************************************************************
	IF 1 = 1
	BEGIN 
		--  Период (Period)
		--  Регистратор (Recorder)
		--  НомерСтроки (LineNo)
		--  Активность (Active)
		--  ВидДвижения (RecordKind)
		--  ДокументОснование (Fld9061)
		--  Склад (Fld9062)
		--  Номенклатура (Fld9063)
		--  Характеристика (Fld9064)
		--  Количество (Fld9065)
		--  ОбластьДанныхОсновныеДанные (Fld516)
		SELECT 
			m.MovementDocId,
			MIN(DATEADD(HOUR, DATEPART(HOUR, _Period), CAST(DATEADD(YEAR, -2000, CAST(_Period as date)) AS Datetime))) AS DateFrom,
			p.id AS ProductId,
			_RecordKind AS Kind,
			SUM(_Fld9065) AS ProductCount
		INTO #DataTo
		FROM X.rt_smart.[dbo]._AccumRg9060 d
			INNER JOIN MovementDoc m
				ON m.row_id = d._Fld9061_RRRef
			LEFT JOIN products p 
				ON p.row_id = d._Fld9063RRef 
			LEFT JOIN warehouses w
				ON w.row_id = d._Fld9062RRef
		GROUP BY 
			m.MovementDocId, p.id, _RecordKind		

		-- Делим на "По заданию получить" и "Получили"
		SELECT 
			ISNULL(d1.MovementDocId, d2.MovementDocId) AS MovementDocId,
			ISNULL(d1.DateFrom, d2.DateFrom) AS DateFrom,
			ISNULL(d1.ProductId, d2.ProductId) AS ProductId,
			ISNULL(d1.ProductCount, 0) AS PlanCount,
			ISNULL(d1.ProductCount, 0) AS FactCount
		INTO #DataToAll
		FROM #DataTo d1
			FULL JOIN #DataTo d2 
				ON d2.MovementDocId = d1.MovementDocId
					AND d2.ProductId = d1.ProductId 
					AND d2.Kind = 1
		WHERE d1.Kind = 0
	END

	--**************************************************************
	--Таблица: РегистрНакопления.ТоварыКОтгрузке, Имя таблицы хранения: AccumRg9039, Назначение: Основная
	--**************************************************************
	IF 1 = 1
	BEGIN 
	   --Период (Period)
	   --Регистратор (Recorder)
	   --НомерСтроки (LineNo)
	   --Активность (Active)
	   --ВидДвижения (RecordKind)
	   --ДокументОснование (Fld9040)
	   --Склад (Fld9041)
	   --Номенклатура (Fld9042)
	   --Характеристика (Fld9043)
	   --Количество (Fld9044)
	   --ОбластьДанныхОсновныеДанные (Fld516)
		SELECT 
			m.MovementDocId,
			MIN(DATEADD(HOUR, DATEPART(HOUR, _Period), CAST(DATEADD(YEAR, -2000, CAST(_Period as date)) AS Datetime))) AS DateFrom,
			p.id AS ProductId,
			_RecordKind AS Kind,
			SUM(_Fld9044) AS ProductCount
		INTO #DataFrom
		FROM X.rt_smart.[dbo]._AccumRg9039 d
			INNER JOIN MovementDoc m
				ON m.row_id = d._Fld9040_RRRef
			LEFT JOIN products p 
				ON p.row_id = d._Fld9042RRef 
			LEFT JOIN warehouses w
				ON w.row_id = d._Fld9041RRef
		GROUP BY 
			m.MovementDocId, p.id, _RecordKind		

		-- Делим на "По заданию получить" и "Получили"
		SELECT 
			ISNULL(d1.MovementDocId, d2.MovementDocId) AS MovementDocId,
			ISNULL(d1.DateFrom, d2.DateFrom) AS DateFrom,
			ISNULL(d1.ProductId, d2.ProductId) AS ProductId,
			ISNULL(d1.ProductCount, 0) AS PlanCount,
			ISNULL(d1.ProductCount, 0) AS FactCount
		INTO #DataFromAll
		FROM #DataFrom d1
			FULL JOIN #DataFrom d2 
				ON d2.MovementDocId = d1.MovementDocId
					AND d2.ProductId = d1.ProductId 
					AND d2.Kind = 1
		WHERE d1.Kind = 0
	END

	--**************************************************************
	-- Объединяем отгрузку и поставку
	--**************************************************************
	IF 1 = 1
	BEGIN 
		-- Делим на "По заданию получить" и "Получили"
		SELECT 
			ISNULL(d1.MovementDocId, d2.MovementDocId) AS MovementDocId,
			ISNULL(d1.ProductId, d2.ProductId) AS ProductId,
			d1.DateFrom AS FromDateFrom,
			d2.DateFrom AS ToDateFrom,
			ISNULL(d1.PlanCount, 0) AS FromPlanCount,
			ISNULL(d1.FactCount, 0) AS FromFactCount,
			ISNULL(d2.PlanCount, 0) AS ToPlanCount,
			ISNULL(d2.FactCount, 0) AS ToFactCount
		INTO #DataFromTo
		FROM #DataFromAll d1
			FULL JOIN #DataToAll d2 
				ON d2.MovementDocId = d1.MovementDocId
					AND d2.ProductId = d1.ProductId 

		INSERT INTO MovementDocFromTo(MovementDocId, ProductId, FromDateFrom, ToDateFrom, FromPlanCount, FromFactCount, ToPlanCount, ToFactCount)
		SELECT 
			dd.MovementDocId, dd.ProductId, dd.FromDateFrom, dd.ToDateFrom, dd.FromPlanCount, dd.FromFactCount, dd.ToPlanCount, dd.ToFactCount 
		FROM #DataFromTo dd
			LEFT JOIN MovementDocFromTo m
				ON m.MovementDocId = dd.MovementDocId
					AND m.ProductId = dd.ProductId
		WHERE 
			m.MovementDocId IS NULL
	END
	--SELECT * FROM MovementDocFromTo
--	-- отладка 
--	--SELECT count(*) FROM #DataFromAll
--SELECT * FROM #DataFromAll where productId = 2683
------where PlanCount = 0 OR  FactCount = 0
------where MovementDocId = 7194 --AND ProductId = 424

--SELECT * FROM #DataToAll where productId = 2683
------MovementDocId = 6771 AND ProductId = 424
------SELECT * FROM MovementDoc where  MovementDocId = 5118-- = 'ЦОUT-002306'
------SELECT * FROM products where id = 382
	
----SELECT * FROM X.rt_smart.[dbo]._AccumRg9039 d	where _Fld9042RRef = 0x80FD00505601926011E7E8B8F08E922B
----SELECT * FROM X.rt_smart.[dbo]._AccumRg9060 d	where _Fld9061_RRRef = 0x80FD00505601926011E7E8B8F08E922B
	
	--SELECT * FROM #DataFromAll dd where 
	--		dd.MovementDocId = 5118
	--		and dd.ProductId =  382


	--SELECT * FROM #DataToAll dd where 
	--		dd.MovementDocId = 5118
	--		and dd.ProductId =  382


	PRINT 'Завершение загрузки данных - Etl_FactMovement'
	PRINT GETDATE()
END
GO
--exec [dbo].[Etl_FactMovement]


--SELECT 
--			DATEADD(HOUR, DATEPART(HOUR, _Date_Time), CAST(DATEADD(YEAR, -2000, CAST(_Date_Time as date)) AS Datetime)) AS DateFrom,
--			_Number AS DocNumber,
--			ISNULL(_Posted, 0) AS IsPosted,
--			_Fld4853RRef AS 'Склад-отправитель',
--			wFrom.WarehouseId AS WarehouseFromId,
--			wTo.WarehouseId AS WarehouseToId,
--			d._IDRRef AS row_id,
--			d.*
--		FROM X.rt_smart.[dbo].[_Document238] d
--			LEFT JOIN vDimWarehouse wFrom 
--				ON wFrom.row_id = d._Fld4853RRef 
--			LEFT JOIN vDimWarehouse wto
--				ON wto.row_id = d._Fld4854RRef 
--WHERE 
--DATEADD(HOUR, DATEPART(HOUR, _Date_Time), CAST(DATEADD(YEAR, -2000, CAST(_Date_Time as date)) AS Datetime)) >= '20190201' 
--AND DATEADD(HOUR, DATEPART(HOUR, _Date_Time), CAST(DATEADD(YEAR, -2000, CAST(_Date_Time as date)) AS Datetime)) < '20190301'
----and wFrom.WarehouseId = 709
--and wTo.WarehouseId = 709

----SELECT * FROM X.rt_smart.[dbo].[_Document238]