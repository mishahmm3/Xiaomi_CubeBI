﻿IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Etl_FactBonusPoints]') AND type IN (N'P', N'PC'))
BEGIN
	EXEC ('CREATE PROCEDURE [dbo].[Etl_FactBonusPoints]  AS RAISERROR(''NOT implemented yet.'', 16, 3);')
END
GO
PRINT ' Altering procedure [dbo].[Etl_FactBonusPoints]'
GO
-- =============================================
-- Author:		Морозов М.А.
-- Create date: 20180612
-- Description:	Загрузка бонусных баллов
-- =============================================
ALTER PROCEDURE [dbo].Etl_FactBonusPoints
AS
BEGIN
	-- Переменные и вспомогательные таблицы
	IF 1 = 1
	BEGIN 
		DECLARE @Now datetime = GETDATE()
	END

		PRINT 'Заливка БонусныеБаллы'

	--        Остатки баллов на конкретной карте (Регистр накоплений: «БонусныеБаллы»)
	--Таблица: РегистрНакопления.БонусныеБаллы, Имя таблицы хранения: AccumRg8696, Назначение: Основная
	--   Период (Period)
	--   Регистратор (Recorder)
	--   НомерСтроки (LineNo)
	--   Активность (Active)
	--   ВидДвижения (RecordKind)
	--   ДисконтнаяКарта (Fld8697)
	--   БонуснаяПрограммаЛояльности (Fld8698)
	--   Начислено (Fld8699)
	--   КСписанию (Fld8700)
	--   ДатаПервоначальногоНачисления (Fld8701)
	--   ОбластьДанныхОсновныеДанные (Fld516)
	
	INSERT INTO BonusPoint(DateFrom, CardId, CountCharged, CountOff)
	SELECT 
		DateFrom,
		CardId,
		SUM(CountCharged),	--Начислено
		SUM(CountOff)		--КСписанию
	FROM 
		(SELECT 
			DATEADD(YEAR, -2000, CAST(_period as date)) AS DateFrom, -- Дата - 2000 лет
			cards.id AS CardId,
			_Fld8699 AS CountCharged,
			_Fld8700 AS CountOff
		FROM X.rt_smart.[dbo]._AccumRg8696 bp
				--mma !!!!!!! Видимо не все карты попадают в cards
				--Есть различия между LEFT и INNER
				-- Проблема может быть в загрузке cards
			INNER JOIN cards
				ON cards.row_id = bp._fld8697rref
			LEFT JOIN BonusPoint res
				ON res.DateFrom = DATEADD(YEAR, -2000, CAST(_period as date))
					AND res.CardId = cards.id
		WHERE
			res.CardId IS NULL	
				AND DATEADD(YEAR, -2000, CAST(_period as date)) < @Now
		) data
	GROUP BY
		DateFrom, CardId	
END
GO
