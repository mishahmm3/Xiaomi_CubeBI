﻿IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Etl_DimAll]') AND type IN (N'P', N'PC'))
BEGIN
	EXEC ('CREATE PROCEDURE [dbo].[Etl_DimAll]  AS RAISERROR(''NOT implemented yet.'', 16, 3);')
END
GO
PRINT ' Altering procedure [dbo].[Etl_DimAll]'
GO
-- =============================================
-- Author:		Морозов М.А.
-- Create date: 20180610
-- Description:	Загрузка измерений
-- =============================================
ALTER PROCEDURE [dbo].[Etl_DimAll]
AS
BEGIN
	-- Переменные и вспомогательные таблицы
	IF 1 = 1
	BEGIN 
		DECLARE @Now datetime = GETDATE()
	END

	PRINT 'Старт загрузки измерений куба - Etl_DimAll'
	PRINT GETDATE()
	
	-- Заливка компаний
	IF 1 = 1
	BEGIN 
		PRINT 'Заливка компаний'
		--Таблица: Справочник.Организации, Имя таблицы хранения: Reference111, Назначение: Основная
		--- поля: 
		--   Ссылка (ID)
		--   ВерсияДанных (Version)
		--   ПометкаУдаления (Marked)
		--   ИмяПредопределенныхДанных (PredefinedID)
		--   Код (Code)
		--   Наименование (Description)
		--   Префикс (Fld2064)
		--   КодПоОКАТО (Fld2065)
		--   КодПоОКПО (Fld2066)
		--   ИНН (Fld2067)
		--   НаименованиеПолное (Fld2068)
		--   НаименованиеСокращенное (Fld2069)
		--   КПП (Fld2070)
		--   ОГРН (Fld2071)
		--   СвидетельствоДатаВыдачи (Fld2072)
		--   СвидетельствоСерияНомер (Fld2073)
		--   ЮрФизЛицо (Fld2074)
		--   СпособОценкиТоваровВРознице (Fld2075)
		--   УдалитьGLN (Fld2076)
		--   УдалитьGCP (Fld2077)
		--   ГоловнаяОрганизация (Fld2078)
		--   ОбособленноеПодразделение (Fld2079)
		--   ОбластьДанныхОсновныеДанные (Fld516)
		INSERT INTO companies(name, prefix, description, created_at, row_id)
		SELECT	
			_Description, _Fld2064, _Fld2069, @Now, _IDRRef
		FROM X.rt_smart.[dbo].[_Reference111] d
			LEFT JOIN companies	comp
				ON comp.row_id = d._IDRRef
		WHERE 
			comp.row_id IS NULL
	END

	-- Заливка физических лиц
	IF 1 = 1
	BEGIN 
		PRINT 'Заливка физических лиц'
		--Таблица: Справочник.ФизическиеЛица, Имя таблицы хранения: Reference168, Назначение: Основная
		--- поля: 
		--   Ссылка (ID)
		--   ВерсияДанных (Version)
		--   ПометкаУдаления (Marked)
		--   ИмяПредопределенныхДанных (PredefinedID)
		--   Родитель (ParentID)
		--   ЭтоГруппа (Folder)
		--   Наименование (Description)
		--   ДатаРождения (Fld3028)
		--   Комментарий (Fld3029)
		--   Сотрудник (Fld3030)
		--   Магазин (Fld3031)
		--   Пол (Fld3032)
		--   ИНН (Fld9860)
		--   _ФИОРП (Fld10752)
		--   _НаОсновании (Fld10753)
		--   Телефон (Fld10897)
		--   ЭлектроннаяПочта (Fld10898)
		--   ОбластьДанныхОсновныеДанные (Fld516)
		INSERT INTO clients(name, birth_day, created_at, row_id)
		SELECT	
			_Description, 
			CASE 
				WHEN YEAR(CAST(_Fld3028 AS datetime)) < 3900 
				THEN @Now
				ELSE DATEADD(YEAR, -2000, CAST(_Fld3028 AS datetime)) 
			END AS birth_day,
			@Now,
			_IDRRef
		FROM X.rt_smart.[dbo].[_Reference168] r
		WHERE 
			_Fld3030 = 0 AND  r._IDRRef NOT IN (SELECT ISNULL(row_id, 0) FROM [dbo].[clients])
	END
	
	-- Заливка карт
	IF 1 = 1
	BEGIN 
		PRINT 'Заливка карт'
		--Таблица: Справочник.ИнформационныеКарты, Имя таблицы хранения: Reference64, Назначение: Основная
		--- поля: 
		--   Ссылка (ID)
		--   ВерсияДанных (Version)
		--   ПометкаУдаления (Marked)
		--   ИмяПредопределенныхДанных (PredefinedID)
		--   Родитель (ParentID)
		--   ЭтоГруппа (Folder)
		--   Код (Code)
		--   Наименование (Description)
		--   КодКарты (Fld1327)
		--   ВладелецКарты (Fld1328)
		--   ВидКарты (Fld1329)
		--   ВидДисконтнойКарты (Fld1330)
		--   ТипКарты (Fld1331)
		--   ДатаОткрытия (Fld1332)
		--   ДатаПоследнегоОпроса (Fld1333)
		--   ДатаСледующегоОпроса (Fld1334)
		--   БонуснаяПрограммаЛояльности (Fld1335)
		--   КартаДляНакоплений (Fld1336)
		--   ор_УдалитьМаскаФлагов (Fld1337)
		--   ор_БиометрическийКод (Fld1338)
		--   ор_ФлагБиометрическийКод (Fld1339)
		--   ГруппаВладельцаКартыПоШаблону (Fld9828)
		--   ОбластьДанныхОсновныеДанные (Fld516)
		INSERT INTO cards(client_id, number, created_at, row_id)
		SELECT 
			c.id,
			r._Description,
			@Now,
			r._IDRRef
		FROM X.rt_smart.[dbo].[_Reference64] r
			LEFT JOIN cards	cards
				ON cards.row_id = r._IDRRef
			LEFT JOIN clients c 
				ON r._Fld1328_RRRef = c.row_id
		WHERE 
			cards.row_id IS NULL
	END

	-- Заливка магазинов
	IF 1 = 1
	BEGIN 
		PRINT 'Заливка магазинов'
		--Таблица: Справочник.Магазины, Имя таблицы хранения: Reference75, Назначение: Основная
		--- поля: 
		--   Ссылка (ID)
		--   ВерсияДанных (Version)
		--   ПометкаУдаления (Marked)
		--   ИмяПредопределенныхДанных (PredefinedID)
		--   Код (Code)
		--   Наименование (Description)
		--   ВидМинимальныхЦенПродажи (Fld1485)
		--   ДатаНачалаЗапретаРеализацииТоваровБезЗаказа (Fld10830)
		--   ДатаНачалаОрдернойСхемыПриОтгрузке (Fld10824)
		--   ДатаНачалаОрдернойСхемыПриПеремещении (Fld10826)
		--   ДатаНачалаОрдернойСхемыПриПоступлении (Fld10825)
		--   ЗапретРеализацииТоваровБезЗаказа (Fld10831)
		--   ИспользоватьОрдернуюСхемуПриОтгрузке (Fld1486)
		--   ИспользоватьОрдернуюСхемуПриОтраженииИзлишковНедостач (Fld1487)
		--   ИспользоватьОрдернуюСхемуПриПеремещении (Fld1488)
		--   ИспользоватьОрдернуюСхемуПриПоступлении (Fld1489)
		--   ИспользоватьПрименениеЦен (Fld1490)
		--   ИспользоватьПрямуюИнкассацию (Fld1498)
		--   КонтролироватьАссортимент (Fld1499)
		--   КонтролироватьАссортиментВЗаказеПокупателя (Fld1500)
		--   МетодРасчетаПотребности (Fld1501)
		--   НаименованиеДляПечати (Fld10836)
		--   Описание (Fld1503)
		--   ор_ИдентификаторБС (Fld9967)
		--   ПлощадьТорговогоЗала (Fld1505)
		--   ПорядокОкругленияСуммыЧекаВПользуПокупателя (Fld1491)
		--   ПравилоЦенообразования (Fld1492)
		--   СегментИсключаемойНоменклатуры (Fld1493)
		--   СкладПоступления (Fld1494)
		--   СкладПродажи (Fld1495)
		--   СкладУправляющейСистемы (Fld1497)
		--   ТипОкругленияЧекаВПользуПокупателя (Fld1504)
		--   ФайлКартинки (Fld1502)
		--   ФорматМагазина (Fld1496)
		--   Организация (Fld10896)
		--   ОбластьДанныхОсновныеДанные (Fld516)
		--   УИД (Fld10823)
		SELECT 
			c.id AS company_id,
			_Code AS Code,
			_Description AS Name,
			@Now AS created_at,
			r._IDRRef AS row_id,
			s.row_id AS Cur_row_id
		INTO #shops
		FROM X.rt_smart.[dbo].[_Reference75] r
			INNER JOIN companies c
				ON r._Fld10896RRef = c.row_id
			LEFT JOIN shops	s
				ON s.row_id = r._IDRRef
		
		INSERT INTO shops(company_id, Code, Name, created_at, row_id)
		SELECT
			company_id, Code, Name, created_at, row_id
		FROM #shops
		WHERE 
			Cur_row_id IS NULL

		--!!!! Обновить имена!!!!!
		UPDATE res
		SET res.Name = s.Name
		FROM shops res
			INNER JOIN #shops s
				ON res.row_id = s.row_id
					AND res.Name <> s.Name
		
		IF NOT EXISTS(SELECT * FROM shops WHERE Name = 'Магазин не привязан')
		BEGIN
			INSERT INTO shops(code, name)
			SELECT 'Магазин не привязан', 'Магазин не привязан'
		END
	END

	-- Заливка Маркетинговый акций (скидок)
	-- Справочник Акций
	-- Справочник Скидок
	-- Соответствие скидок акциям и и магазинам
	IF 1 = 1
	BEGIN 
		PRINT 'Маркетинговая акция'
		--Таблица: Документ.МаркетинговаяАкция, Имя таблицы хранения: Document204, Назначение: Основная
		--- поля: 
		--   Ссылка (ID)
		--   ВерсияДанных (Version)
		--   ПометкаУдаления (Marked)
		--   Дата (Date_Time) (NumberPrefix)
		--   Номер (Number)
		--   Проведен (Posted)
		--   НаименованиеАкции (Fld3949)
		--   Описание (Fld3950)
		--   ДатаНачалаДействия (Fld3951)
		--   ДатаОкончанияДействия (Fld3952)
		--   Комментарий (Fld3953)
		--   Ответственный (Fld3954)
		--   ДляВсехМагазинов (Fld3955)
		--   ДляВсехМагазиновОдноРасписаниеСкидок (Fld3956)
		--   ОбластьДанныхОсновныеДанные (Fld516)
		INSERT INTO MarketAction(name, DateFrom, DateTo, created_at, row_id)
		SELECT	
			ma._Fld3949 AS Name,
			CASE 
				WHEN YEAR(CAST(ma._Fld3951 AS datetime)) < 3900 
				THEN @Now
				ELSE DATEADD(YEAR, -2000, CAST(ma._Fld3951 AS datetime)) 
			END AS DateFrom,
			CASE 
				WHEN YEAR(CAST(ma._Fld3952 AS datetime)) < 3900 
				THEN @Now
				ELSE DATEADD(YEAR, -2000, CAST(ma._Fld3952 AS datetime)) 
			END AS DateTo,
			@Now AS created_at,
			ma._IDRRef
			------------------
		FROM X.rt_smart.[dbo].[_Document204] ma
		WHERE 
			ma._IDRRef NOT IN (SELECT ISNULL(row_id, 0) FROM MarketAction)

		--Таблица: Справочник.СкидкиНаценки, Имя таблицы хранения: Reference141, Назначение: Основная
		--- поля: 
		--   Ссылка (ID)
		--   ВерсияДанных (Version)
		--   ПометкаУдаления (Marked)
		--   ИмяПредопределенныхДанных (PredefinedID)
		--   Родитель (ParentID)
		--   ЭтоГруппа (Folder)
		--   Наименование (Description)
		--   ВалютаПредоставления (Fld2628)
		--   ВариантСовместногоПрименения (Fld2629)
		--   ВидЦены (Fld2630)
		--   ЗначениеСкидкиНаценки (Fld2631)
		--   ОбластьПредоставления (Fld2632)
		--   РеквизитДопУпорядочивания (Fld2633)
		--   СегментНоменклатурыПредоставления (Fld2634)
		--   СпособПредоставления (Fld2635)
		--   СтатусДействия (Fld2636)
		--   Управляемая (Fld2637)
		--   ПодарокИзСписка (Fld2638)
		--   ПодарокИзКорзиныПокупателя (Fld2639)
		--   КоличествоПодарковИзКорзиныПокупателя (Fld2640)
		--   УчитыватьПодарокКакПродажу (Fld2641)
		--   ТекстСообщения (Fld2642)
		--   МоментВыдачиСообщения (Fld2643)
		--   БонуснаяПрограммаЛояльности (Fld2644)
		--   ПериодДействия (Fld2645)
		--   КоличествоПериодовДействия (Fld2646)
		--   ПериодОтсрочкиНачалаДействия (Fld2647)
		--   КоличествоПериодовОтсрочкиНачалаДействия (Fld2648)
		--   КратноКоличествуУсловий (Fld2649)
		--   ОграничениеРазмераПодчиненныхСкидок (Fld2650)
		--   ВнешняяОбработка (Fld2651)
		--   ПараметрыВнешнейОбработки (Fld2652)
		--   ОбластьДанныхОсновныеДанные (Fld516)
		INSERT INTO discounts(name, value, created_at, row_id)
		SELECT	
			_Description,
			CAST(_Fld2631 AS float),
			@Now,
			_IDRRef
		FROM X.rt_smart.[dbo].[_Reference141] r
		WHERE 
			_Folder = 1
			AND r._IDRRef NOT IN (SELECT ISNULL(row_id, 0) FROM discounts)

		--Таблица: Документ.МаркетинговаяАкция.СкидкиНаценки, Имя таблицы хранения: Document204.VT3960, Назначение: ТабличнаяЧасть
		--   НомерСтроки (LineNo3961)
		--   ДатаНачала (Fld3962)
		--   ДатаОкончания (Fld3963)
		--   Магазин (Fld3964)
		--   СкидкаНаценка (Fld3965)
		DECLARE @ShopEmptyId int = (SELECT id FROM shops WHERE Name = 'Магазин не привязан')

		INSERT INTO MarketActionDiscount(MarketActionId, discount_id, shop_id, created_at)
		SELECT 
			a.MarketActionId,
			d.id AS discount_id,
			ISNULL(sh.Id, @ShopEmptyId) AS shop_id,
			@Now 
		FROM X.rt_smart.[dbo]._Document204_VT3960 ad
			INNER JOIN MarketAction a
				ON a.row_id = ad._Document204_IDRRef
			LEFT JOIN discounts d
				ON d.row_id = ad._fld3965Rref
			LEFT JOIN shops sh 
				ON sh.row_id = ad._Fld3964Rref
			LEFT JOIN MarketActionDiscount res
				ON res.MarketActionId = a.MarketActionId
					AND res.discount_id = d.id
					AND res.shop_id = ISNULL(sh.Id, @ShopEmptyId)
		WHERE
			res.MarketActionId IS NULL				
	END

	-- Заливка типов склада
	IF 1 = 1
	BEGIN 
		PRINT 'Заливка типов склада'

		--update [dbo].[warehouse_types]
		--set row_id = e._IDRRef
		--FROM [dbo].[warehouse_types] wt INNER JOIN 
		--X.rt_smart.[dbo].[_Enum478] e ON wt.id = _EnumOrder + 1

		INSERT INTO warehouse_types(Name, row_id)
		SELECT	
			CASE 
				WHEN _EnumOrder = 0 THEN 'Торговый зал'
				WHEN _EnumOrder = 1 THEN 'Складское помещение'
				ELSE 'Тип склада ' + CAST(_EnumOrder AS varchar(10))
			END AS Name,
			d._IDRRef
		FROM X.rt_smart.[dbo]._Enum478 d
			LEFT JOIN warehouse_types res	
				ON res.row_id = d._IDRRef
		WHERE 
			res.row_id IS NULL

		-- Заливка склада
		--Таблица: Справочник.Склады, Имя таблицы хранения: Reference142, Назначение: Основная
		--- поля: 
		--   Ссылка (ID)
		--   ВерсияДанных (Version)
		--   ПометкаУдаления (Marked)
		--   ИмяПредопределенныхДанных (PredefinedID)
		--   Родитель (ParentID)
		--   ЭтоГруппа (Folder)
		--   Код (Code)
		--   Наименование (Description)
		--   ТипСклада (Fld2670)
		--   Организация (Fld2671)
		--   Магазин (Fld2672)
		--   ОбластьДанныхОсновныеДанные (Fld516)
		--   УИД (Fld10823)
		INSERT INTO warehouses(warehouse_type_id, shop_id, name, created_at, row_id)
		SELECT 
			t.id,
			s.id,
			_Description,
			@Now,
			_IDRRef
		FROM X.rt_smart.[dbo].[_Reference142] r
			LEFT JOIN warehouse_types t 
				ON r._Fld2670RRef = t.row_id
			LEFT JOIN shops s 
				ON r._Fld2672RRef = s.row_id
		WHERE 
			_IDRRef NOT IN (SELECT ISNULL(row_id, 0) FROM warehouses)
	END
	
	-- Заливка касс
	IF 1 = 1
	BEGIN 
		PRINT 'Заливка касс'
		--Таблица: Справочник.КассыККМ, Имя таблицы хранения: Reference66, Назначение: Основная
		--- поля: 
		--   Ссылка (ID)
		--   ВерсияДанных (Version)
		--   ПометкаУдаления (Marked)
		--   ИмяПредопределенныхДанных (PredefinedID)
		--   Владелец (OwnerID)
		--   Код (Code)
		--   Наименование (Description)
		--   ИспользоватьБезПодключенияОборудования (Fld1357)
		--   Магазин (Fld1354)
		--   НастройкаРаспределенияВыручкиПоСекциям (Fld1365)
		--   ПодключаемоеОборудование (Fld1363)
		--   РабочееМесто (Fld1362)
		--   РегистрационныйНомер (Fld1355)
		--   СерийныйНомер (Fld1356)
		--   Модель (Fld10835)
		--   СоответствиеВидовОплаты (Fld1367)
		--   ТипКассы (Fld1364)
		--   ТипОборудования (Fld9846)
		--   УдалитьРучнойРежимФормированияЧека (Fld1361)
		--   УдалитьФормироватьНефискальныеЧеки (Fld1359)
		--   ШаблонЧекаККМ (Fld1358)
		--   ШаблонЧекаККМВозврат (Fld1366)
		--   ШиринаЛенты (Fld1360)
		--   ЭлектронныйЧекEmailПередаютсяПрограммой1С (Fld9847)
		--   ЭлектронныйЧекSMSПередаютсяПрограммой1С (Fld9848)
		--   ОбластьДанныхОсновныеДанные (Fld516)
		--   УИД (Fld10823)
		INSERT INTO cash_desks(shop_id, name, created_at, row_id)
		SELECT 
			s.id,
			_Description,
			@Now,
			_IDRRef
		FROM X.rt_smart.[dbo].[_Reference66] r
			INNER JOIN shops s 
				ON r._Fld1354RRef = s.row_id
		WHERE 
			r._IDRRef NOT IN (SELECT ISNULL(row_id, 0) FROM cash_desks)
	END

	-- Заливка сотрудников
	IF 1 = 1
	BEGIN 
		PRINT 'Заливка сотрудников'
		--Таблица: Справочник.ФизическиеЛица, Имя таблицы хранения: Reference168, Назначение: Основная
		--- поля: 
		--   Ссылка (ID)
		--   ВерсияДанных (Version)
		--   ПометкаУдаления (Marked)
		--   ИмяПредопределенныхДанных (PredefinedID)
		--   Родитель (ParentID)
		--   ЭтоГруппа (Folder)
		--   Наименование (Description)
		--   ДатаРождения (Fld3028)
		--   Комментарий (Fld3029)
		--   Сотрудник (Fld3030)
		--   Магазин (Fld3031)
		--   Пол (Fld3032)
		--   ИНН (Fld9860)
		--   _ФИОРП (Fld10752)
		--   _НаОсновании (Fld10753)
		--   Телефон (Fld10897)
		--   ЭлектроннаяПочта (Fld10898)
		--   ОбластьДанныхОсновныеДанные (Fld516)
		INSERT INTO [users](shop_id, name, created_at, row_id)
		SELECT 
			s.id,
			_Description,
			@Now,
			_IDRRef
		FROM X.rt_smart.[dbo].[_Reference168] r
			INNER JOIN shops s 
				ON r._Fld3031RRef = s.row_id
		WHERE 
			_Fld3030 = 1 AND r._IDRRef NOT IN (SELECT ISNULL(row_id, 0) FROM [users])

		UPDATE u
		SET 
			u.shop_id = s.id,
			u.name = r._Description
		FROM users u
			INNER JOIN X.rt_smart.[dbo].[_Reference168] r
				ON r._IDRRef = u.row_id
			INNER JOIN shops s 
				ON r._Fld3031RRef = s.row_id
	END
	
	-- Заливка единицы измерения
	IF 1 = 1
	BEGIN 
		PRINT 'Заливка единицы измерения'

		--Таблица: Справочник.БазовыеЕдиницыИзмерения, Имя таблицы хранения: Reference31, Назначение: Основная
		--- поля: 
		--   Ссылка (ID)
		--   ВерсияДанных (Version)
		--   ПометкаУдаления (Marked)
		--   ИмяПредопределенныхДанных (PredefinedID)
		--   Код (Code)
		--   Наименование (Description)
		--   НаименованиеПолное (Fld808)
		--   МеждународноеСокращение (Fld809)
		--   ОбластьДанныхОсновныеДанные (Fld516)
		INSERT INTO [dbo].[measure_types]
		(name, [description], created_at, row_id)
		SELECT	_Fld808,
				_Description,
				GetDate(),
				_IDRRef
		FROM X.rt_smart.[dbo].[_Reference31]
		WHERE _IDRRef NOT IN (SELECT ISNULL(row_id, 0) FROM [dbo].[measure_types])
	END

	-- Заливка брендов
	IF 1 = 1
	BEGIN 
		PRINT 'Заливка брендов'
		--Таблица: Справочник.Марки, Имя таблицы хранения: Reference77, Назначение: Основная
		--- поля: 
		--   Ссылка (ID)
		--   ВерсияДанных (Version)
		--   ПометкаУдаления (Marked)
		--   ИмяПредопределенныхДанных (PredefinedID)
		--   Родитель (ParentID)
		--   ЭтоГруппа (Folder)
		--   Код (Code)
		--   Наименование (Description)
		--   ОбластьДанныхОсновныеДанные (Fld516)
		INSERT INTO product_brands(name, created_at, row_id)
		SELECT 
			_Description,
			@Now,
			_IDRRef
		FROM X.rt_smart.[dbo].[_Reference77]
		WHERE 
			_IDRRef NOT IN (SELECT ISNULL(row_id, 0) FROM product_brands)
	END

	-- Заливка видов номенклатуры
	IF 1 = 1
	BEGIN 
		PRINT 'Заливка видов номенклатуры'
		--Таблица: Справочник.ВидыНоменклатуры, Имя таблицы хранения: Reference43, Назначение: Основная
		--- поля: 
		--   Ссылка (ID)
		--   ВерсияДанных (Version)
		--   ПометкаУдаления (Marked)
		--   ИмяПредопределенныхДанных (PredefinedID)
		--   Родитель (ParentID)
		--   ЭтоГруппа (Folder)
		--   Наименование (Description)
		--   АгентскиеУслуги (Fld989)
		--   АлкогольнаяПродукция (Fld990)
		--   ВидАлкогольнойПродукцииЕГАИС (Fld991)
		--   ЕдиницаИзмерения (Fld992)
		--   ИмпортнаяАлкогольнаяПродукция (Fld993)
		--   ИспользованиеХарактеристик (Fld994)
		--   ИспользоватьКоличествоСерии (Fld995)
		--   ИспользоватьНомерСерии (Fld996)
		--   ИспользоватьСерии (Fld997)
		--   ИспользоватьСрокГодностиСерии (Fld998)
		--   ИспользоватьХарактеристики (Fld999)
		--   НаборСвойств (Fld1000)
		--   НаборСвойствХарактеристик (Fld1001)
		--   НаборУпаковок (Fld1002)
		--   НастройкаИспользованияСерий (Fld1003)
		--   СрокГарантии (Fld1004)
		--   СтавкаНДС (Fld1005)
		--   ТипНоменклатуры (Fld1006)
		--   ор_ИнвентарныйУчет (Fld1007)
		--   ор_УслугаТерминалаУслуг (Fld1008)
		--   ТоварнаяГруппа (Fld1009)
		--   ТочностьУказанияСрокаГодностиСерии (Fld1010)
		--   УдалитьВидАлкогольнойПродукции (Fld1011)
		--   ЦеноваяГруппа (Fld1012)
		--   ШаблонЦенника (Fld1013)
		--   ШаблонЭтикетки (Fld1014)
		--   ПродаетсяВРозлив (Fld1015)
		--   ОсобенностьУчета (Fld1016)
		--   ПродукцияМаркируемаяДляГИСМ (Fld1017)
		--   КиЗГИСМ (Fld1018)
		--   ИспользоватьRFIDМеткиСерии (Fld1019)
		--   ИспользоватьНомерКИЗГИСМСерии (Fld1020)
		--   ИспользоватьВидКИЗГИСМСерии (Fld1021)
		--   ПризнакПредметаРасчета (Fld9818)
		--   ОбластьДанныхОсновныеДанные (Fld516)
		INSERT INTO product_types(name, created_at, row_id)
		SELECT 
			_Description,
			@Now,
			_IDRRef
		FROM X.rt_smart.[dbo].[_Reference43] r
		WHERE 
			r._IDRRef NOT IN (SELECT ISNULL(row_id, 0) FROM product_types)
	END

	-- Заливка номенклатуры
	IF 1 = 1
	BEGIN 
		PRINT 'Заливка номенклатуры'
		--Таблица: Справочник.Номенклатура, Имя таблицы хранения: Reference85, Назначение: Основная
		--- поля: 
		--   Ссылка (ID)
		--   ВерсияДанных (Version)
		--   ПометкаУдаления (Marked)
		--   ИмяПредопределенныхДанных (PredefinedID)
		--   Родитель (ParentID)
		--   ЭтоГруппа (Folder)
		--   Код (Code)
		--   Наименование (Description)
		--   АлкогольнаяПродукция (Fld1774)
		--   Артикул (Fld1775)
		--   Вес (Fld1776)
		--   Весовой (Fld1777)
		--   ВидАлкогольнойПродукцииЕГАИС (Fld1778)
		--   ВидНоменклатуры (Fld1779)
		--   ДатаОкончанияДействия (Fld1780)
		--   ДоговорПлатежногоАгента (Fld1781)
		--   ЕдиницаИзмерения (Fld1782)
		--   ИмпортнаяАлкогольнаяПродукция (Fld1783)
		--   ИспользоватьСерийныеНомера (Fld1784)
		--   КоличествоПериодовДействия (Fld1785)
		--   Крепость (Fld1786)
		--   Марка (Fld1787)
		--   НаборУпаковок (Fld1788)
		--   НаименованиеПолное (Fld1789)
		--   Номинал (Fld1790)
		--   ОбъемДАЛ (Fld1791)
		--   Описание (Fld1792)
		--   Периодичность (Fld1793)
		--   Производитель (Fld1794)
		--   ПроизводительИмпортерАлкогольнойПродукции (Fld1795)
		--   СтавкаНДС (Fld1796)
		--   СтранаПроисхождения (Fld1797)
		--   ТипНоменклатуры (Fld1798)
		--   ТипСерийногоНомера (Fld1799)
		--   ТипСрокаДействия (Fld1800)
		--   ТоварнаяГруппа (Fld1801)
		--   ТоварнаяКатегория (Fld1802)
		--   УдалитьВидАлкогольнойПродукции (Fld1803)
		--   ор_ВыписыватьГарантийныйТалон (Fld1804)
		--   ор_СрокГарантии (Fld1805)
		--   ор_ТерминалУслуг (Fld1806)
		--   ор_ПроцентТорговойУступки (Fld1807)
		--   ор_ИмяФайлаОбработкиОбслуживанияТерминалаУслуг (Fld1808)
		--   ор_ОбработкаВводаПараметров (Fld1809)
		--   ор_КодОперации (Fld1810)
		--   ор_ТипКомиссии (Fld1811)
		--   ор_Комиссия (Fld1812)
		--   ор_УслугаТерминалаУслуг (Fld1813)
		--   ор_ОписаниеСопутствующегоТовара (Fld1814)
		--   ор_КонтролироватьБалансАгента (Fld1815)
		--   ФайлКартинки (Fld1816)
		--   ЦеноваяГруппа (Fld1817)
		--   ор_ШаблонЦенника (Fld1818)
		--   ор_ШаблонЭтикетки (Fld1819)
		--   ОсобенностьУчета (Fld1820)
		--   ПродукцияМаркируемаяДляГИСМ (Fld1821)
		--   КиЗГИСМ (Fld1822)
		--   КиЗГИСМВид (Fld1823)
		--   КиЗГИСМСпособВыпускаВОборот (Fld1824)
		--   КиЗГИСМGTIN (Fld1825)
		--   КиЗГИСМРазмер (Fld1826)
		--   ВидМехаГИСМ (Fld1827)
		--   КодТНВЭД (Fld1828)
		--   ОбластьДанныхОсновныеДанные (Fld516)
		SELECT 
			pt.id AS product_type_id,
			pb.id AS product_brand_id,
			mt.id AS measure_type_id,
			_Fld1775 AS sku,
			_Description AS name,
			_Fld1789 AS full_name,
			@Now AS created_at,
			_IDRRef AS row_id,
			pr.row_id AS Cur_row_id
		INTO #product
		FROM X.rt_smart.[dbo].[_Reference85] r
			LEFT JOIN product_types pt 
				ON r._Fld1779RRef = pt.row_id
			LEFT JOIN product_brands pb 
				ON r._Fld1787RRef = pb.row_id
			LEFT JOIN measure_types mt 
				ON r._Fld1782RRef = mt.row_id
			LEFT JOIN products pr
				ON pr.row_id = r._IDRRef

		INSERT INTO products(product_type_id, product_brand_id, measure_type_id, sku, name, full_name, created_at, row_id)
		SELECT
			product_type_id, product_brand_id, measure_type_id, sku, name, full_name, created_at, row_id
		FROM #product
		WHERE 
			Cur_row_id IS NULL

		--!!!! Обновить имена!!!!!
		UPDATE res
		SET 
			res.Name = s.Name,
			res.product_brand_id = s.product_brand_id,
			res.product_type_id = s.product_type_id,
			res.measure_type_id = s.measure_type_id,
			res.sku = s.sku
		FROM products res
			INNER JOIN #product s
				ON res.row_id = s.row_id
	END

	PRINT 'Виды типы платежей'
	IF 1 = 1
	BEGIN 
		INSERT INTO PaymentType(Code, Name, row_id)
		SELECT	
			_EnumOrder AS Code,
			'Тип платежа' + CAST(_EnumOrder AS varchar(10)) AS Code,
			d._IDRRef
		FROM X.rt_smart.[dbo]._Enum469 d
			LEFT JOIN PaymentType res	
				ON res.row_id = d._IDRRef
		WHERE 
			res.row_id IS NULL

		INSERT INTO PaymentVid(Code, Name, PaymentTypeId, row_id)
		SELECT	
			_Code AS Code,
			_Description AS Name,
			pt.PaymentTypeId,
			d._IDRRef
		FROM X.rt_smart.[dbo]._Reference44 d
			INNER JOIN PaymentType pt	
				ON pt.row_id = d._Fld1027RRef
			LEFT JOIN PaymentVid res	
				ON res.row_id = d._IDRRef
		WHERE 
			res.row_id IS NULL
	END
	
	PRINT 'Виды Статусов чеков '
	IF 1 = 1
	BEGIN 
		INSERT INTO CheckStatus(Code, row_id)
		SELECT	
			_EnumOrder AS Code,
			d._IDRRef
		FROM X.rt_smart.[dbo]._Enum454 d
			LEFT JOIN CheckStatus res	
				ON res.row_id = d._IDRRef
		WHERE 
			res.row_id IS NULL
	END

	PRINT 'Видоперации по чеку'
	IF 1 = 1
	BEGIN 
		INSERT INTO CheckOperationVid(Code, row_id)
		SELECT	
			_EnumOrder AS Code,
			d._IDRRef
		FROM X.rt_smart.[dbo]._Enum310 d
			LEFT JOIN CheckOperationVid res	
				ON res.row_id = d._IDRRef
		WHERE 
			res.row_id IS NULL
	END
	PRINT 'Завершение загрузки измерений куба - Etl_DimAll'
	PRINT GETDATE()
END
GO

