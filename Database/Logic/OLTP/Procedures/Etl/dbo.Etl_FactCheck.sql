﻿IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Etl_FactCheck]') AND type IN (N'P', N'PC'))
BEGIN
	EXEC ('CREATE PROCEDURE [dbo].[Etl_FactCheck]  AS RAISERROR(''NOT implemented yet.'', 16, 3);')
END
GO
PRINT ' Altering procedure [dbo].[Etl_FactCheck]'
GO
-- =============================================
-- Author:		Ternovskiy A.V.
-- Create date: 2018-05-25
-- Description:	Загрузка чеков
-- =============================================
ALTER PROCEDURE [dbo].[Etl_FactCheck]
AS
BEGIN
	-- Переменные и вспомогательные таблицы
	IF 1 = 1
	BEGIN 
		DECLARE @Now datetime = GETDATE()
		
		-- Тип операции по чеку
		DECLARE @Operation_id_in int = (SELECT id FROM operations WHERE Name = 'Приход')
		DECLARE @Operation_id_out int = (SELECT id FROM operations WHERE Name = 'Расход')

		SELECT
			DISTINCT row_id
		INTO #detail
		FROM check_details
	END

	-- Заливка заголовков чеков
	IF 1 = 1
	BEGIN 
		PRINT 'Заливка заголовков чеков'
		--Таблица: Документ.ЧекККМ, Имя таблицы хранения: Document273, Назначение: Основная
		--- поля: 
		--   Ссылка (ID)
		--   ВерсияДанных (Version)
		--   ПометкаУдаления (Marked)
		--   Дата (Date_Time)
		--    (NumberPrefix)
		--   Номер (Number)
		--   Проведен (Posted)
		--   АдресЧекаЕГАИС (Fld5806)
		--   АналитикаХозяйственнойОперации (Fld5807)
		--   БонусыНачислены (Fld5808)
		--   ВидОперации (Fld5809)
		--   ВладелецДисконтнойКарты (Fld5810)
		--   ДисконтнаяКарта (Fld5811)
		--   ЗаказПокупателя (Fld5812)
		--   КассаККМ (Fld5813)
		--   Комментарий (Fld5814)
		--   Магазин (Fld5815)
		--   НомерСменыККМ (Fld5816)
		--   НомерЧекаККМ (Fld5817)
		--   Организация (Fld5818)
		--   Ответственный (Fld5819)
		--   ОтработанПереход (Fld5820)
		--   ОтчетОРозничныхПродажах (Fld5821)
		--   ПодписьЧекаЕГАИС (Fld5822)
		--   Продавец (Fld5823)
		--   СкидкиРассчитаны (Fld5824)
		--   СтатусЧекаККМ (Fld5825)
		--   СуммаДокумента (Fld5826)
		--   ор_ВозвратИзРемонта (Fld5827)
		--   ор_ПоступлениеТоваров (Fld5828)
		--   ор_НомерТранзакции (Fld5829)
		--   ор_НомерПродажи (Fld5830)
		--   ор_НомерВнешнейКарты (Fld5831)
		--   ор_НачисленоБаллов (Fld5832)
		--   ор_СписаноБаллов (Fld5833)
		--   ор_НомерКартыДляПользователя (Fld5834)
		--   ор_ВидВнешнейПрограммыЛояльности (Fld5835)
		--   УдалитьДоговорЭквайринга (Fld5836)
		--   ЦенаВключаетНДС (Fld5837)
		--   ЧекККМПродажа (Fld5838)
		--   Телефон (Fld9880)
		--   АдресЭП (Fld9881)
		--   ДоговорПлатежногоАгента (Fld9882)
		--   ВидНалога (Fld9883)
		--   Контрагент (Fld10642)
		--   ОперацияСДенежнымиСредствами (Fld10643)
		--   ДокументРасчета (Fld10644)
		--   КассоваяСмена (Fld10899)
		--   ОбластьДанныхОсновныеДанные (Fld516)
		
		INSERT INTO dbo.check_headers(cash_desk_id, card_id, seller_id, CheckOperationVidId, 
			Number, [date], CheckStatusId, change_number, check_number, created_at, row_id)
		SELECT	
			cd.id AS cash_desk_id,
			c.id AS card_id,
			u.id AS seller_id,
			ov.CheckOperationVidId,
			_Number AS Number,
			DATEADD(YEAR, -2000, CAST(_Date_Time as datetime)) AS [Date], -- Дата - 2000 лет
			st.CheckStatusId,
			_Fld5816,
			_Fld5817,
			@Now AS created_at,
			_IDRRef AS row_id
		FROM X.rt_smart.[dbo].[_Document273] d
			INNER JOIN cash_desks cd 
				ON d._Fld5813RRef = cd.row_id
			LEFT JOIN cards 
				c ON d._Fld5811RRef = c.row_id
			-- mma здесь вроде distinct не нужен
			LEFT JOIN check_headers head 
				ON head.row_id = d._IDRRef
			LEFT JOIN CheckStatus st 
				ON st.row_id = d._Fld5825Rref
			LEFT JOIN CheckOperationVid ov 
				ON ov.row_id = d._Fld5809Rref
			LEFT JOIN users u 
				ON u.row_id = d._Fld5823Rref
		WHERE
			d._Posted <> 0
			AND head.row_id IS NULL
	END

	-- Товары. Товары чека
	IF 1 = 1
	BEGIN 
		PRINT 'Заливка товаров чека'
		--Таблица: Документ.ЧекККМ.Товары, Имя таблицы хранения: Document273.VT5839, Назначение: ТабличнаяЧасть
		--- поля: 
		--   НомерСтроки (LineNo5840)
		--   ЗаказПокупателя (Fld5841)
		--   КлючСвязи (Fld5842)
		--   КлючСвязиСерийныхНомеров (Fld5843)
		--   КодСтроки (Fld5844)
		--   Количество (Fld5845)
		--   КоличествоУпаковок (Fld5846)
		--   НеобходимостьВводаАкцизнойМарки (Fld5847)
		--   Номенклатура (Fld5848)
		--   Продавец (Fld5849)
		--   ПродажаПодарка (Fld5850)
		--   ПроцентАвтоматическойСкидки (Fld5851)
		--   ПроцентРучнойСкидки (Fld5852)
		--   РегистрацияПродажи (Fld5853)
		--   Резервировать (Fld5854)
		--   Склад (Fld5855)
		--   СтавкаНДС (Fld5856)
		--   СтатусУказанияСерий (Fld5857)
		--   Сумма (Fld5858)
		--   СуммаАвтоматическойСкидки (Fld5859)
		--   СуммаНДС (Fld5860)
		--   СуммаРучнойСкидки (Fld5861)
		--   СуммаСкидкиОплатыБонусом (Fld5862)
		--   Упаковка (Fld5863)
		--   Характеристика (Fld5864)
		--   Цена (Fld5865)
		--   Удалитьор_ЗаказПокупателя (Fld5866)
		--   Штрихкод (Fld5867)
		--   ор_СуммаСкидкиБС (Fld5868)
		--   _НомерСтраховки (Fld10708)
		
		SELECT
			_Document273_IDRRef, _LineNo5840, _Fld5845, _Fld5865, _Fld5858, _Fld5859, _Fld5861, _Fld5862, _Fld5860, _Fld5848RRef, _Fld5849RRef
		INTO #data_details
		FROM X.rt_smart.[dbo].[_Document273_VT5839] data
			LEFT JOIN #detail det
				ON det.row_id = data._Document273_IDRRef
		WHERE
			det.row_id IS NULL
			
		INSERT INTO check_details(
			check_header_id, operation_id, check_type_id, product_id, [user_id], [line_number], [count], price, amount, discount_auto, discount_manual, bonus, nds, created_at, row_id)
		SELECT 
				ch.id AS check_header_id, 
				@Operation_id_in,
				NULL AS check_type_id,
				p.id AS product_id,
				u.id AS [user_id],
				data._LineNo5840 AS [line_number],
				data._Fld5845 AS [count],
				data._Fld5865 AS price,
				data._Fld5858 AS amount,
				data._Fld5859 AS discount_auto,
				data._Fld5861 AS discount_manual,
				data._Fld5862 AS bonus,
				data._Fld5860 AS nds,
				@Now AS created_at,
				data._Document273_IDRRef
		FROM #data_details data
			INNER JOIN check_headers ch 
				ON ch.row_id = data._Document273_IDRRef
			LEFT JOIN products p 
				ON p.row_id = data._Fld5848RRef
			LEFT JOIN users u 
				ON u.row_id = data._Fld5849RRef
		END

	--	Товары. Заливка возвратов
	IF 1 = 1
	BEGIN 
		PRINT 'Товары. Заливка возвратов товаров'
		--	Таблица: Документ.ВозвратТоваровОтПокупателя.Товары, Имя таблицы хранения: Document187.VT3496, Назначение: ТабличнаяЧасть
		--- поля: 
		--   НомерСтроки (LineNo3497)
		--   Номенклатура (Fld3498)
		--   Характеристика (Fld3499)
		--   Количество (Fld3500)
		--   Упаковка (Fld3501)
		--   КоличествоУпаковок (Fld3502)
		--   Цена (Fld3503)
		--   Сумма (Fld3504)
		--   СтавкаНДС (Fld3505)
		--   СуммаНДС (Fld3506)
		--   ДокументПродажи (Fld3507)
		--   Продавец (Fld3508)
		--   ЧекККМ (Fld3509)
		--   СтатусУказанияСерий (Fld3510)
		--   СуммаСкидкиОплатыБонусом (Fld3511)
		--   СуммаАвтоматическойСкидки (Fld3512)
		--   СуммаРучнойСкидки (Fld3513)
		--   НеобходимостьВводаАкцизнойМарки (Fld3514)
		--   ор_СуммаСкидкиБС (Fld3517)
		INSERT INTO check_details(
			check_header_id, operation_id, check_type_id, product_id, [user_id], [line_number], [count], price, amount, discount_auto, discount_manual, bonus, nds, created_at, row_id)
		SELECT 
			ch.id,
			@Operation_id_out,
			NULL AS check_type_id,
			p.id,
			u.id,
			_LineNo3497,
			_Fld3500,
			_Fld3503,
			0 -_Fld3504,
			_Fld3512,
			_Fld3513,
			_Fld3511,
			_Fld3506,
			@Now AS created_at,
			_Document187_IDRRef
		FROM X.rt_smart.[dbo].[_Document187_VT3496] v
			INNER JOIN check_headers ch 
				ON v._Fld3509RRef = ch.row_id
			INNER JOIN products	p 
				ON v._Fld3498RRef = p.row_id
			INNER JOIN users u 
				ON v._Fld3508RRef = u.row_id
			LEFT JOIN #detail det
				ON det.row_id = v._Document187_IDRRef
		WHERE 
			det.row_id IS NULL
	END
			
	-- Заливка скидок для чеков
	IF 1 = 1
	BEGIN 
		PRINT 'Заливка скидок для чеков'
		--Таблица: Документ.ЧекККМ.СкидкиНаценки, Имя таблицы хранения: Document273.VT5909, Назначение: ТабличнаяЧасть
		--- поля: 
		--   НомерСтроки (LineNo5910)
		--   КлючСвязи (Fld5911)
		--   Сумма (Fld5912)
		--   СкидкаНаценка (Fld5913)
		--   ОграниченаМинимальнойЦеной (Fld5914)
			DECLARE @DiscId int = (SELECT id FROM Discounts WHERE Name = 'Округление в пользу клиента')
			
			INSERT INTO check_discounts(check_header_id, discount_id, product_id, amount, created_at, row_id)
			SELECT 
				check_header_id, 
				discount_id,
				product_id,
				SUM(amount) AS amount,
				GETDATE() AS [date],
				row_id
			FROM (
				SELECT 
					ch.id AS check_header_id, 
					ISNULL(d.id, @DiscId) AS discount_id,
					cd.product_id AS product_id,
					c._Fld5912 AS amount,
					GETDATE() AS [date],
					_Document273_IDRRef AS row_id
				FROM X.rt_smart.[dbo].[_Document273_VT5909] c
					INNER JOIN check_headers ch 
						ON c._Document273_IDRRef = ch.row_id
					INNER JOIN check_details cd 
						ON ch.id = cd.check_header_id and c._Fld5911 = cd.line_number
					LEFT JOIN discounts d 
						ON c._Fld5913RRef = d.row_id
				WHERE 
					c._Document273_IDRRef NOT IN (SELECT DISTINCT ISNULL(row_id, 0) FROM check_discounts)
			) d
			GROUP BY 
				check_header_id, discount_id, product_id, row_id

			-- Не всегда есть сопоставления товаров между деталями и скидками
			--SELECT * FROM vFactCheck where CheckHeaderRowId = 0x8B671C1B0DB4219511E86AF92FD0AB42
			--SELECT * FROM X.rt_smart.[dbo].[_Document273_VT5909] where _Document273_IDRRef = 0x8B671C1B0DB4219511E86AF92FD0AB42
			--SELECT * FROM X.rt_smart.[dbo].[_Document273_VT5839] data WHERE _Document273_IDRRef = 0x8B671C1B0DB4219511E86AF92FD0AB42
	END

	-- Заливка бонусов по покупке
	IF 1 = 1
	BEGIN 
		PRINT 'Заливка БонусныеБаллы'

		--Таблица: Документ.ЧекККМ.БонусныеБаллыКНачислению, Имя таблицы хранения: Document273.VT5929, Назначение: ТабличнаяЧасть
		--- поля: 
		--   НомерСтроки (LineNo5930)
		--   КлючСвязи (Fld5931)
		--   БонуснаяПрограммаЛояльности (Fld5932)
		--   СкидкаНаценка (Fld5933)
		--   ДатаНачисления (Fld5934)
		--   ДатаСписания (Fld5935)
		--   КоличествоБонусныхБаллов (Fld5936)

		INSERT INTO check_bonuses(
			check_header_id, product_id, [add_date], [remove_date], bonus_count, created_at, row_id)
		SELECT	
			ch.id AS check_header, 	
			cd.product_id AS product_id,   
			CASE 
				WHEN YEAR(CAST(_Fld5934 AS datetime)) < 3900 
				THEN NULL
				ELSE dateadd(YEAR, -2000, CAST(_Fld5934 as datetime)) 
			END AS add_date,
			CASE 
				WHEN YEAR(CAST(_Fld5935 AS datetime)) < 3900 
				THEN NULL
				ELSE dateadd(YEAR, -2000, CAST(_Fld5935 AS datetime)) 
			END AS remove_date,
			_Fld5936 AS bonus_count,
			@Now AS created_at,
			_Document273_IDRRef AS row_id
		FROM  X.rt_smart.[dbo].[_Document273_VT5929] c
			INNER JOIN check_headers ch 
				ON c._Document273_IDRRef = ch.row_id
			INNER JOIN check_details cd 
				ON ch.id = cd.check_header_id and c._LineNo5930 = cd.line_number
		WHERE 
			c._Document273_IDRRef NOT IN (SELECT DISTINCT ISNULL(row_id, 0) FROM check_bonuses)
	END

	-- Платежи по чеку
	IF 1 = 1
	BEGIN 
		PRINT 'Платежи по чеку'
		--Таблица: Документ.ЧекККМ.Оплата, Имя таблицы хранения: Document273.VT5869, Назначение: ТабличнаяЧасть
		--   НомерСтроки (LineNo5870)
		--   ВидОплаты (Fld5871)
		--   ЭквайринговыйТерминал (Fld5872)
		--   Сумма (Fld5873)
		--   ПроцентКомиссии (Fld5874)
		--   СуммаКомиссии (Fld5875)
		--   СсылочныйНомер (Fld5876)
		--   НомерЧекаЭТ (Fld5877)
		--   НомерПлатежнойКарты (Fld5878)
		--   ДанныеПереданыВБанк (Fld5879)
		--   СуммаБонусовВСкидках (Fld5880)
		--   КоличествоБонусов (Fld5881)
		--   КоличествоБонусовВСкидках (Fld5882)
		--   БонуснаяПрограммаЛояльности (Fld5883)
		--   ДоговорПлатежногоАгента (Fld5884)
		--   КлючСвязиОплаты (Fld5885)
		--   _КредитныйДоговор (Fld10813)
		SELECT
			DISTINCT row_id
		INTO #CheckPaymentCur
		FROM CheckPayment
		
		INSERT INTO CheckPayment(check_header_id, product_id, PaymentVidId, Summa, CommisionPercent, CommisionSumma, BonusSumma, row_id)
		SELECT
			ch.id AS check_header_id, 
			cd.product_id,
			pv.PaymentVidId,
			_Fld5873 AS Summa,
			_Fld5874 AS CommisionPercent,
			_Fld5875 AS CommisionSumma,
			_Fld5880 AS BonusSumma, -- СуммаБонусовВСкидках (Fld5880)
			d._Document273_IDRRef AS row_id
		FROM X.rt_smart.[dbo].[_Document273_VT5869] d
			INNER JOIN check_headers ch 
				ON ch.row_id = d._Document273_IDRRef
			INNER JOIN PaymentVid pv	
				ON pv.row_id = d._Fld5871Rref
			LEFT JOIN #CheckPaymentCur det
				ON det.row_id = d._Document273_IDRRef
			LEFT JOIN check_details cd 
				ON cd.check_header_id = ch.id 
					AND d._LineNo5870 = cd.line_number
					AND cd.operation_id = @Operation_id_in
		WHERE
			det.row_id IS NULL	
	END
END
GO

--exec [dbo].[Etl_FactCheck]

