﻿IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Etl_FactProductUnload]') AND type IN (N'P', N'PC'))
BEGIN
	EXEC ('CREATE PROCEDURE [dbo].[Etl_FactProductUnload]  AS RAISERROR(''NOT implemented yet.'', 16, 3);')
END
GO
PRINT ' Altering procedure [dbo].[Etl_FactProductUnload]'
GO
-- =============================================
-- Author:		Морозов М.А.
-- Create date: 20180612
-- Description:	Данные по товарам к отгрузке
-- =============================================
ALTER PROCEDURE [dbo].Etl_FactProductUnload
AS
BEGIN
	-- Переменные и вспомогательные таблицы
	IF 1 = 1
	BEGIN 
		DECLARE @Now datetime = GETDATE()
	END

		PRINT 'Данные по товарам к отгрузке'

	--Таблица: РегистрНакопления.ТоварыКОтгрузке, Имя таблицы хранения: AccumRg9039, Назначение: Основная
	--   Период (Period)
	--   Регистратор (Recorder)
	--   НомерСтроки (LineNo)
	--   Активность (Active)
	--   ВидДвижения (RecordKind)
	--   ДокументОснование (Fld9040)
	--   Склад (Fld9041)
	--   Номенклатура (Fld9042)
	--   Характеристика (Fld9043)
	--   Количество (Fld9044)
	--   ОбластьДанныхОсновныеДанные (Fld516)

	INSERT INTO ProductUnload(DateFrom, WarehouseId, ProductId, CountUnload)
	SELECT 
		DateFrom,
		WarehouseId,
		ProductId,
		SUM(Count) AS CountUnload
	FROM 
		(SELECT 
				DATEADD(YEAR, -2000, CAST(_period as date)) AS DateFrom, -- Дата - 2000 лет
				w.Id AS WarehouseId,
				p.Id AS ProductId,
				_Fld9044 AS Count
			FROM X.rt_smart.[dbo]._AccumRg9039 data
				INNER JOIN warehouses w
					ON w.row_id = data._Fld9041RRef
				INNER JOIN products p 
					ON p.row_id = data._Fld9042RRef
				LEFT JOIN ProductUnload res
					ON res.DateFrom = DATEADD(YEAR, -2000, CAST(_period as date))
						AND res.WarehouseId = w.id
						AND res.ProductId = p.id
			WHERE
				res.DateFrom IS NULL	
		) data
	GROUP BY
		DateFrom, WarehouseId, ProductId	
END
GO

--exec Etl_FactProductUnload

--delete FROM ProductUnload