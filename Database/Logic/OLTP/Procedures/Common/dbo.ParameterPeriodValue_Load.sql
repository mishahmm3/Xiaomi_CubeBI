﻿IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ParameterPeriodValue_Load]') AND type IN (N'P', N'PC'))
BEGIN
	EXEC ('CREATE PROCEDURE [dbo].[ParameterPeriodValue_Load]  AS RAISERROR(''NOT implemented yet.'', 16, 3);')
END
GO
PRINT ' Altering procedure [dbo].[ParameterPeriodValue_Load]'
GO
-- =============================================
-- Author:		Морозов М.А.
-- Create date: 20180620
-- Description:	Загрузка данных по параметрам
-- =============================================
--'<Parameters>
--    <Parameter Code="KSeason" PeriodCode="D" DateFrom="20170101" Value="1.5" />
--    <Parameter Code="KSeason" PeriodCode="M" DateFrom="20170101" Value="2" />
--</Parameters>'
-- =============================================
ALTER PROCEDURE [dbo].[ParameterPeriodValue_Load] 
	@xml xml
AS
BEGIN
	SET NOCOUNT ON;

	--**************************************************
	-- ОПТИМИЗАЦИЯ РАБОТЫ С XML ЧЕРЕЗ ТАБЛИЦУ
	--**************************************************
	IF OBJECT_ID('tempdb..#xmlTemp') IS NOT NULL DROP TABLE #xmlTemp
	CREATE TABLE #xmlTemp(id int PRIMARY KEY, z xml)

	INSERT INTO #xmlTemp(id, z) VALUES (1, @xml)
	CREATE PRIMARY XML INDEX IX_#xmlTemp ON dbo.#xmlTemp(z)

	--*************************************************************************
	-- Получаем данные
	--*************************************************************************
	IF 1 = 1
	BEGIN 
		SELECT 
			R.c.value('@Code', 'varchar(100)') AS ParameterCode,
			p.ParameterId AS ParameterId,
			R.c.value('@PeriodCode', 'varchar(100)') AS PeriodTypeCode,
			pt.PeriodTypeId AS PeriodTypeId,
			R.c.value('@DateFrom', 'datetime') AS DateFrom,
			R.c.value('@Value', 'decimal(13,5)') AS Value
		INTO #Data
		FROM #xmlTemp CROSS APPLY z.nodes('/Parameters/Parameter') AS R(c)
			LEFT JOIN Parameter p
				ON p.Code = R.c.value('@Code', 'varchar(100)')
			LEFT JOIN PeriodType pt
				ON pt.Code = R.c.value('@PeriodCode', 'varchar(100)')
	END
	
	--*************************************************************************
	-- Проверка ошибок
	--*************************************************************************
	IF 1 = 1
	BEGIN 
		DECLARE @ErrorList varchar(8000) = '';

		SET @ErrorList= ''
		SELECT
			@ErrorList =
				CASE 
					WHEN LEN(@ErrorList) = 0 THEN CAST(error AS varchar(100))  
					WHEN LEN(@ErrorList) > 0 THEN (@ErrorList + ',' + CAST(error AS varchar(100)))  
				END
		FROM 
		(
			SELECT DISTINCT
				ParameterCode AS error					
			FROM #Data
			WHERE ParameterId IS NULL
		) err	

		IF @ErrorList <> '' 
		BEGIN
			RAISERROR('Загрузка  данных (ParameterPeriodValue_Load). Не найдены параметры с кодами: %s.', 16, 1, @ErrorList)
			RETURN
		END	

		SET @ErrorList= ''
		SELECT
			@ErrorList =
				CASE 
					WHEN LEN(@ErrorList) = 0 THEN CAST(error AS varchar(100))  
					WHEN LEN(@ErrorList) > 0 THEN (@ErrorList + ',' + CAST(error AS varchar(100)))  
				END
		FROM 
		(
			SELECT DISTINCT
				PeriodTypeCode AS error					
			FROM #Data
			WHERE PeriodTypeId IS NULL
		) err	

		IF @ErrorList <> '' 
		BEGIN
			RAISERROR('Загрузка  данных (ParameterPeriodValue_Load). Не найдены периоды с кодами: %s.', 16, 1, @ErrorList)
			RETURN
		END	

		SET @ErrorList= ''
		SELECT
			@ErrorList =
				CASE 
					WHEN LEN(@ErrorList) = 0 THEN 'Code=' + ParameterCode + ', PeriodCode=' + PeriodTypeCode + ', DateFrom=' + CONVERT(VARCHAR(10), DateFrom, 104)
					WHEN LEN(@ErrorList) > 0 THEN (@ErrorList + ',' + 'Code=' + ParameterCode + ', PeriodCode=' + PeriodTypeCode + ', DateFrom=' + CONVERT(VARCHAR(10), DateFrom, 104))  
				END
		FROM 
		(
			SELECT 
				ParameterCode, PeriodTypeCode, DateFrom
			FROM #Data
			GROUP BY
				ParameterCode, PeriodTypeCode, DateFrom
			HAVING COUNT(*) > 1
		) err	

		IF @ErrorList <> '' 
		BEGIN
			RAISERROR('Загрузка  данных (ParameterPeriodValue_Load). Найдены задвоения данных для следующих ключей: %s.', 16, 1, @ErrorList)
			RETURN
		END	
	END

	--*************************************************************************
	-- Загрузка данных
	--*************************************************************************
	IF 1 = 1
	BEGIN 
		MERGE ParameterPeriodValue AS tgt
		USING
		(
			SELECT 
				ParameterId, PeriodTypeId, DateFrom, Value
			FROM #Data
		) AS src
		ON src.ParameterId = tgt.ParameterId
			AND src.PeriodTypeId = tgt.PeriodTypeId
			AND src.DateFrom = tgt.DateFrom
		WHEN MATCHED THEN  
			UPDATE SET tgt.Value = src.Value
		WHEN NOT MATCHED THEN
			INSERT (ParameterId, PeriodTypeId, DateFrom, Value)
			VALUES (src.ParameterId, src.PeriodTypeId, src.DateFrom, src.Value);
	END
END
GO

--BEGIN TRAN

--SELECT * FROM PeriodType
--SELECT * FROM ParameterPeriodValue
--EXEC [dbo].[ParameterPeriodValue_Load] 
--	@xml = '<Parameters>
--		<Parameter Code="KSeason" PeriodCode="Y" DateFrom="20170101" Value="33333" />
--		<Parameter Code="KSeason" PeriodCode="M" DateFrom="20170101" Value="20000" />
--		<Parameter Code="KSeason" PeriodCode="M" DateFrom="20170110" Value="12345" />
--	</Parameters>'

--SELECT * FROM ParameterPeriodValue
--ROLLBACK TRAN