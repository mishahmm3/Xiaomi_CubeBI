﻿IF EXISTS (SELECT *	FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SplitString]') AND type IN (N'FN', N'IF', N'TF', N'FS', N'FT')) 
DROP FUNCTION [dbo].[SplitString]
GO
-- =============================================
-- Author:		Морозов М.А.
-- Create date: 
-- Description:	Преобразует строку значений с разделителями в таблицу, содержащую эти значения.
-- =============================================
CREATE FUNCTION [dbo].[SplitString] (
	@string varchar(max), -- Строка значений, разделенных разделителем.
	@delimiter nvarchar(10) = ';' -- Разделитель
	)
RETURNS @value_table TABLE (value varchar(MAX), position int)
BEGIN
	DECLARE @next_string varchar(max)
	DECLARE @pos int
	DECLARE @next_pos int
	DECLARE @comma_check varchar(1)

	IF (@string = '')
		RETURN

	--Initialize
	SET @next_string = ''
	SET @comma_check = right(@string, 1)
	
	--Check for trailing Comma, IF not exists, INSERT
	--IF (@comma_check <> @delimiter )
	SET @string = @string + @delimiter
	--Get position of first Comma
	SET @pos = charindex(@delimiter, @string)
	SET @next_pos = 1
	
	DECLARE @position int = 1 -- порядковый номер записи.

	--Loop while there is still a comma in the String of levels
	WHILE (@pos <> 0)
	BEGIN
		SET @next_string = substring(@string, 1, @Pos - 1)

		INSERT INTO @value_table (value, position)
		VALUES (@next_string, @position)

		SET @string = rtrim(ltrim(substring(@string, @pos + 1, len(@string))))
		SET @next_pos = @pos
		SET @pos = charindex(@delimiter, @string)
		SET @position = @position + 1;
	END
	RETURN
END
GO
