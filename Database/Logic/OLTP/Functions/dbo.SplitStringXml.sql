IF EXISTS (SELECT *	FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SplitStringXml]') AND type IN (N'FN', N'IF', N'TF', N'FS', N'FT')) 
DROP FUNCTION [dbo].SplitStringXml
GO
-- =============================================
-- Author:		������� �.�.
-- Create date: 20180703
-- Description:	����������� ������ �������� � ������������� � �������, ���������� ��� ��������.
-- =============================================
CREATE FUNCTION [dbo].[SplitStringXml] (
	@String varchar(max), -- ������ ��������, ����������� ������������.
	@Delimiter varchar(1) = ';' -- �����������
	)
RETURNS TABLE
WITH SCHEMABINDING
AS
   RETURN 
   (  
      SELECT 
		value = y.i.value('(./text())[1]', 'varchar(MAX)')
      FROM 
      ( 
        SELECT x = CONVERT(XML, '<i>' 
          + REPLACE(@String, @Delimiter, '</i><i>') 
          + '</i>').query('.')
      ) AS a CROSS APPLY x.nodes('i') AS y(i)
   );
GO

