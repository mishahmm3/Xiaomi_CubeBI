IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'v_ProductRemainsCalc'))
BEGIN
	EXEC ('CREATE VIEW v_ProductRemainsCalc AS SELECT 1 [Default];')
END
GO
PRINT ' Altering VIEW v_ProductRemainsCalc'
GO
-- =======================================================
-- Author:		������ �������
-- Create date: 20180607
-- =======================================================
-- Description:	������� �� �������
-- 
-- =======================================================
ALTER VIEW v_ProductRemainsCalc
AS
	SELECT
		CAST([date] AS Date)	AS OperationalDate,
		p.name					AS ProductName,
		wh.name					AS WarehouseName,
		pMove.count				AS ProductCount,
		pMove.product_id		AS ProductId,
		pMove.warehouse_id		AS WarehouseId,
		[date]					AS DateFrom	
	FROM product_calc_remains pMove
		INNER JOIN products p
			ON p.id = pMove.product_id
		INNER JOIN warehouses wh
			ON wh.id = pMove.warehouse_id
GO