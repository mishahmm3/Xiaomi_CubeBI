IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'v_ParameterDay'))
BEGIN
	EXEC ('CREATE VIEW v_ParameterDay AS SELECT 1 [Default];')
END
GO
PRINT ' Altering VIEW v_ParameterDay'
GO
-- =======================================================
-- Author:		������ �������
-- Create date: 20180609
-- =======================================================
-- Description:	��������� �� ����
-- 
-- =======================================================
ALTER VIEW v_ParameterDay
AS
	SELECT
		DateFrom,
		p.Code AS ParameterCode,
		p.EntityCode AS EntityTableCode,
		EntityId,
		Value
	FROM ParameterDay pDay
		INNER JOIN Parameter p
			ON p.ParameterId = pDay.ParameterId
GO
--SELECT * FROM v_ParameterDay


