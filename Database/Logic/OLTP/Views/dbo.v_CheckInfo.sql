IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'v_CheckInfo'))
BEGIN
	EXEC ('CREATE VIEW v_CheckInfo AS SELECT 1 [Default];')
END
GO
PRINT ' Altering VIEW v_CheckInfo'
GO
-- =======================================================
-- Author:		������ �������
-- Create date: 20180607
-- =======================================================
-- Description:	
-- =======================================================
ALTER VIEW v_CheckInfo
AS
	SELECT 
		CAST(ch.date AS Date) AS DateOnly,
		ch.Number AS CheckNumber,
		desk.ShopName,
		desk.CashDeskName,
		p.name AS ProductName,
		Amount * CASE WHEN v.Code = '0' THEN 1 ELSE -1 END AS Amount,
		cs.Name AS CheckStatus,
		v.Name AS CheckOperationVid,
		ch.date AS DateFrom,
		p.Id AS ProductId,
		desk.ShopId,
		desk.CashDeskId
	FROM check_headers ch 
		INNER JOIN check_details cd
			ON cd.check_header_id = ch.id
		LEFT JOIN vDimCashDesk desk
			ON desk.CashDeskId = ch.cash_desk_id	
		LEFT JOIN products p
			ON p.id = cd.product_id	
		LEFT JOIN CheckOperationVid v
			ON v.CheckOperationVidId = ch.CheckOperationVidId
		LEFT JOIN CheckStatus cs
			ON cs.CheckStatusId = ch.CheckStatusId
GO

--SELECT * 
--FROM v_CheckInfo

--WHERE 
--	DateOnly =   '20180602'
--	AND ShopName LIKE '%����%'
--	AND ProductName LIKE '%redmi 5 32%'
