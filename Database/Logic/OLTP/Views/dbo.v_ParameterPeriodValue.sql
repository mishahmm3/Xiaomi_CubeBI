IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'v_ParameterPeriodValue'))
BEGIN
	EXEC ('CREATE VIEW v_ParameterPeriodValue AS SELECT 1 [Default];')
END
GO
PRINT ' Altering VIEW v_ParameterPeriodValue'
GO
-- =======================================================
-- Author:		Михаил Морозов
-- Create date: 20180712
-- =======================================================
-- Description:	Таблица попериодовок
-- 
-- =======================================================
ALTER VIEW v_ParameterPeriodValue
AS
	SELECT
		d.DateFrom,
		d.EntityId,
		par.EntityCode AS EntityTableCode,
		par.Name AS ParameterName,
		d.Value,
		pt.Code AS PeriodTypeCode,
		pt.Name AS PeriodTypeName,
		par.ParameterId,
		par.Code AS ParameterCode
	FROM ParameterPeriodValue d
		INNER JOIN Parameter par
			ON par.ParameterId = d.ParameterId
		INNER JOIN PeriodType pt
			ON pt.PeriodTypeId = d.PeriodTypeId
GO


--SELECT * FROM v_ParameterPeriodValue