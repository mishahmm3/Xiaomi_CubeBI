IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'v_ProductRemains'))
BEGIN
	EXEC ('CREATE VIEW v_ProductRemains AS SELECT 1 [Default];')
END
GO
PRINT ' Altering VIEW v_ProductRemains'
GO
-- =======================================================
-- Author:		������ �������
-- Create date: 20180607
-- =======================================================
-- Description:	�������� ������
-- 
-- =======================================================
ALTER VIEW v_ProductRemains
AS
	SELECT
		CAST([date] AS Date)	AS OperationalDate,
		op.name					AS OperationName,
		p.name					AS ProductName,
		wh.name					AS WarehouseName,
		pMove.count				AS ProductCount,
		pMove.product_id		AS ProductId,
		pMove.warehouse_id		AS WarehouseId,
		pMove.operation_id		AS OperationId,
		[date]					AS DateFrom	
	FROM product_remains pMove
		INNER JOIN products p
			ON p.id = pMove.product_id
		INNER JOIN warehouses wh
			ON wh.id = pMove.warehouse_id
		INNER JOIN operations op
			ON op.Id = pMove.operation_id
GO
