﻿IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'vFileLog'))
BEGIN
	EXEC ('CREATE VIEW vFileLog AS SELECT 1 [Default];')
END
GO
PRINT ' Altering VIEW vFileLog'
GO
-- =======================================================
-- Author:		Михаил Морозов
-- Create date: 20180629
-- =======================================================
-- Description:	История загрузки файлов
-- =======================================================
ALTER VIEW vFileLog
AS
	SELECT
		fl.Id AS FileLogId,
		t.code AS FileTypeCode,
		t.name AS FileTypeName,
		s.code AS FileStatusCode,
		s.name AS FileStatusName,
		fl.file_name AS FileName,
		upload_date AS UploadDate,
		parse_date AS ParseDate, 
		DATALENGTH(Content) AS ContentSize,
		Content		
	FROM file_logs fl
		LEFT JOIN file_status s
			ON s.id = fl.file_status_id
		LEFT JOIN file_types t
			ON t.Id = fl.file_type_id
GO

--SELECT * FROM vFileLog

--WHERE 
--	DateOnly =   '20180602'
--	AND ShopName LIKE '%жемч%'
--	AND ProductName LIKE '%redmi 5 32%'



