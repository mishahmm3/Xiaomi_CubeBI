--*************************************
-- Job �������� ������ ������ CDC
--*************************************
USE [msdb]
GO

--*****************************************************
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--*****************************************************
DECLARE @JOBNAME varchar(100) = N'cdc_clear_tables'

-- �������, ���� ���������� � ����� ������  
DECLARE @job_id uniqueidentifier
SELECT @job_id = job_id FROM msdb.dbo.sysjobs_view WHERE name = @JOBNAME
IF  (@job_id IS NOT NULL)
BEGIN
	EXEC msdb.dbo.sp_delete_job @job_id = @job_id, @delete_unused_schedule=1
END
GO

	--*****************************************************
	--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	--*****************************************************
	DECLARE @JOBNAME varchar(100) = N'cdc_clear_tables'
	DECLARE @DBNAME varchar(100) = N'!!!!!����!!!!!!!!!'
	DECLARE @COMMAND varchar(100) = N'EXEC Etl.sp_cdc_clear_tables 3'
	DECLARE @OWNER_LOGIN_NAME varchar(100) = N'!!!!!������������!!!!!!'	

BEGIN TRANSACTION
	DECLARE @ReturnCode INT
	SELECT @ReturnCode = 0
	/****** Object:  JobCategory [[Uncategorized (Local)]]]    Script Date: 06/10/2013 17:05:08 ******/
	IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
	BEGIN
		EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
		IF (@@ERROR <> 0 OR @ReturnCode <> 0) 
		GOTO QuitWithRollback
	END

	DECLARE @jobId BINARY(16)
	EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=@JOBNAME, 
			@enabled=1, 
			@notify_level_eventlog=2, 
			@notify_level_email=0, 
			@notify_level_netsend=0, 
			@notify_level_page=0, 
			@delete_level=0, 
			@description=N'CDC Cleanup Job', 
			@category_name=N'[Uncategorized (Local)]', 
			@owner_login_name=@OWNER_LOGIN_NAME, @job_id = @jobId OUTPUT
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

	EXEC @ReturnCode = msdb.dbo.sp_add_jobstep 
		@job_id=@jobId,
		@step_name=N'CDC Cleanup Job', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=10, 
		@retry_interval=1, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=@COMMAND, 
		--@server=N'RZ-DEV', 
		@database_name=@DBNAME, 
		@flags=4
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

	--��������� ���������� 
	EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule 
		@job_id=@jobId,					-- id 
		@name=@JOBNAME,					-- ������������ ���
		@enabled=1,						-- �������
		@freq_type=4,					-- ������ �������: 4 - ���������
		@freq_interval=1,				-- ��������:	1 - �� �����������, 4 - ���������,  8 - ����������� 
		@freq_subday_type=1,			-- 1 - � ������������� �����, 4 - ����������, 8 - ��������
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20130123, 
		@active_end_date=99991231, 
		@active_start_time=11200, 
		@active_end_time=235959

	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO


