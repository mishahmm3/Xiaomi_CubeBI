IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[product_calc_remain_fact]'))
BEGIN
   DROP VIEW dbo.product_calc_remain_fact
END
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[cash_desk_dim]'))
BEGIN
   DROP VIEW dbo.cash_desk_dim
END
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[product_dim]'))
BEGIN
   DROP VIEW dbo.product_dim
END
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[conversion_fact]'))
BEGIN
   DROP VIEW dbo.conversion_fact
END
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vFactCashDeskEncashment]'))
BEGIN
   DROP VIEW dbo.vFactCashDeskEncashment
END
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[shop_company_dim]'))
BEGIN
   DROP VIEW dbo.shop_company_dim
END
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[cross_check_fact]'))
BEGIN
   DROP VIEW dbo.cross_check_fact
END
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vDimMovementDoc]'))
BEGIN
   DROP VIEW dbo.vDimMovementDoc
END
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[shift_dim]'))
BEGIN
   DROP VIEW dbo.shift_dim
END

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vDimCheckType]'))
BEGIN
   DROP VIEW dbo.vDimCheckType
END
