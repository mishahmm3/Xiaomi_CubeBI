﻿IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vDimDateDetail]'))
BEGIN
	EXEC ('CREATE VIEW [dbo].[vDimDateDetail] AS SELECT 1 [Default];')
END
GO
-- =======================================================
-- Author:		Михаил Морозов
-- Create date: 20180618
-- =======================================================
ALTER VIEW [dbo].[vDimDateDetail]
AS
	SELECT   
		--DateDetailId, 
		[date] AS DateFromId, 
		CAST([date] AS Date) AS DateOnly,
		DATEADD(MONTH,DATEDIFF(MONTH, 0, [date]) , 0) AS MonthStart, 
		CONVERT(VARCHAR(10), [date], 104) AS  DateName,
		DATEPART(hh, [date]) AS [Hour], 
		CAST(DATEPART(hh, [date]) AS varchar(2)) + '-' + CAST(DATEPART(hh, [date]) + 1 AS varchar(2)) AS [HourName], 

		DATEPART(dd, [date]) AS [Day], 
		
		-- Неделя
		CASE WHEN DATEPART(dw, [date]) - CASE WHEN @@DATEFIRST = 1 THEN 0 ELSE 1 END = 0 
			THEN 7 
			ELSE DATEPART(dw, [date]) - CASE WHEN @@DATEFIRST = 1 THEN 0 ELSE 1 END
		END	AS [DayWeekNb], 
		
		CASE	
			CASE WHEN DATEPART(dw, [date]) - CASE WHEN @@DATEFIRST = 1 THEN 0 ELSE 1 END = 0 
					THEN 7 
					ELSE DATEPART(dw, [date]) - CASE WHEN @@DATEFIRST = 1 THEN 0 ELSE 1 END
			END	
			WHEN 1 THEN 'Понедельник' WHEN 2 THEN 'Вторник' WHEN 3 THEN 'Среда' WHEN 4 THEN 'Четверг' 
			WHEN 5 THEN 'Пятница' WHEN 6 THEN 'Суббота' WHEN 7 THEN 'Воскресенье' 
		END AS [WeekDay], 
		
		DATEPART(wk, [date]) 
			- CASE WHEN @@DATEFIRST = 1 THEN 1 ELSE 0 END - CASE WHEN @@DATEFIRST = 7 AND DATEPART(dw, [date]) = 1 THEN 1 ELSE 0 END  AS [Week], 
		
		-- Интервал недели
		CONVERT(VARCHAR(10), DATEADD(dd, - ((DATEPART(dw, [date]) + CASE WHEN @@DATEFIRST = 1 THEN 6 ELSE 5 END) % 7), [date]), 4) 
			+ ' - ' 
			+ CONVERT(VARCHAR(10), DATEADD(dd, - ((DATEPART(dw, [date]) + CASE WHEN @@DATEFIRST = 1 THEN 6 ELSE 5 END) % 7) + 6, [date]), 4) AS WeekName, 
		
		CAST(DATEADD(hh, - DATEPART(hh, [date]), DATEADD(dd, - ((DATEPART(dw, [date]) + CASE WHEN @@DATEFIRST = 1 THEN 6 ELSE 5 END) % 7), [date])) 
					AS DATE) AS [WeekId],

		-- Месяц и далее
		DATEPART(mm, [date]) AS [Month], 
		dfMonth.MonthName	 AS [MonthName],
		DATEPART(qq, [date]) AS [Quarter], 
		CAST(DATEPART(qq, [date]) AS varchar(1)) + ' квартал' AS [QuarterName], 
		CASE WHEN DATEPART(qq, [date]) > 2 THEN 2 ELSE 1 END AS [HalfYear], 
		CAST(CASE WHEN DATEPART(qq, [date]) > 2 THEN 2 ELSE 1 END AS varchar(1)) + ' полугодие' AS [HalfYearName], 
		DATEPART(yy, [date]) AS [Year] 
	FROM calendar dd
		INNER JOIN vMonthName dfMonth
			ON ISNULL(DATEPART(mm, [date]), 0) = dfMonth.MonthId
GO

--SELECT * FROM [vDimDateDetail]
--SELECT * FROM calendar

--SELECT * FROM vDimCalendar

--SELECT * FROM [vDimDateDetail]
