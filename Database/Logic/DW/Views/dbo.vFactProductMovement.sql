﻿IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'vFactProductMovement'))
BEGIN
	EXEC ('CREATE VIEW vFactProductMovement AS SELECT 1 [Default];')
END
GO
PRINT ' Altering VIEW vFactProductMovement'
GO
-- =======================================================
-- Author:		Михаил Морозов
-- Create date: 20180907
-- =======================================================
-- Description:	РГ Движение товаров 
-- =======================================================
ALTER VIEW vFactProductMovement
AS
	SELECT      
		pr.id, 
		w.shop_id,
		pr.product_id, 
		pr.warehouse_id, 
		pr.operation_id, 
		DATEADD(hour, DATEDIFF(hour, '', pr.date), '') AS [date], 
		pr.[count]
	FROM product_remains pr
		INNER JOIN warehouses w 
			ON w.id = pr.warehouse_id
GO
