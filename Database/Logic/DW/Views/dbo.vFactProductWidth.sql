﻿IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'vFactProductWidth'))
BEGIN
	EXEC ('CREATE VIEW vFactProductWidth AS SELECT 1 [Default];')
END
GO
PRINT ' Altering VIEW vFactProductWidth'
GO
-- =======================================================
-- Author:		Михаил Морозов
-- Create date: 20180703
-- =======================================================
-- Description:	Данные по ширине товара
-- =======================================================
ALTER VIEW vFactProductWidth
AS
	SELECT 
		d.Date,
		d.ShopId,
		d.ProductGroupId,
		d.RemainValue,
		wid.WidthValue
	FROM
	( 
		SELECT 
			[Date],
			w.shop_id AS ShopId,
			gr.product_group_id AS ProductGroupId,
			COUNT(*) AS RemainValue
		FROM product_calc_remains pr
			LEFT JOIN warehouses w 
				ON pr.warehouse_id = w.id
			LEFT JOIN product_group_products gr
				On gr.product_id = pr.product_id
		WHERE 
			[Count] > 0
		GROUP BY 
			[Date],	w.shop_id, gr.product_group_id
	) d
	LEFT JOIN
	(
		SELECT 
			dd.DateOnly AS DateFrom, 
			ShopId,
			ProductGroupId,
			dp.Value AS WidthValue 
		FROM GradeWidthPlan dp
			LEFT JOIN GradeShop gs
				ON gs.DateFrom = dp.DateFrom
					AND gs.GradeId = dp.GradeId	
			INNER JOIN 
				(SELECT DISTINCT MonthStart, DateOnly FROM vDimDateDetail) dd
					ON dd.MonthStart = dp.DateFrom
	) wid
		ON wid.DateFrom = d.date
			AND wid.ShopId = d.ShopId
			AND wid.ProductGroupId = d.ProductGroupId
GO

--SELECT * FROM vFactProductWidth
--WHERE Date = '20180323'
--WHERE Date = '20171201'
--and shopid = 7148
----and product_id = 316
--SELECT Distinct Datefrom FROM GradeWidthPlan
--order by 1