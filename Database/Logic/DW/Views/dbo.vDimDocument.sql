﻿IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'vDimDocument'))
BEGIN
	EXEC ('CREATE VIEW vDimDocument AS SELECT 1 [Default];')
END
GO
PRINT ' Altering VIEW vDimDocument'
GO
-- =======================================================
-- Author:		Михаил Морозов
-- Create date: 20180907
-- =======================================================
-- Description:	Измерение Документ-основание
-- 
-- =======================================================
ALTER VIEW vDimDocument
AS
	SELECT DISTINCT
		MovementDocId AS DocumentId,
		DocNumber	AS DocumentNumber,
		CASE WHEN IsPosted = 1 THEN 'Да' ELSE 'Нет' END IsPosted,
		'Товар' AS DocumentType
	FROM MovementDoc m

	UNION ALL 

	SELECT DISTINCT
		Id		AS DocumentId,
		Number	AS DocumentNumber,
		'Да'	AS IsPosted,
		'Чек' AS DocumentType
	FROM check_headers 
GO

