﻿IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'vFactProductCalcRemain'))
BEGIN
	EXEC ('CREATE VIEW vFactProductCalcRemain AS SELECT 1 [Default];')
END
GO
PRINT ' Altering VIEW vFactProductCalcRemain'
GO
-- =======================================================
-- Author:		Михаил Морозов
-- Create date: 20180619
-- =======================================================
-- Description:	Остатки по товарам
-- =======================================================
ALTER VIEW vFactProductCalcRemain
AS
	SELECT 
		d.Date,
		d.product_id,
		d.shop_id,
		d.warehouse_id,
		d.RemainValue,
		dep.DepthValue
	FROM
	( 
		SELECT 
			[Date],
			pr.product_id,
			w.shop_id,
			warehouse_id,
			ROW_NUMBER() OVER (PARTITION BY [Date], pr.product_id, w.shop_id ORDER BY warehouse_id) AS WH_Number,
			[Count] AS RemainValue
		FROM product_calc_remains pr
			LEFT JOIN warehouses w 
				ON pr.warehouse_id = w.id
		WHERE 
			[Count] > 0
	) d
	LEFT JOIN
	(
		SELECT 
			dp.DateFrom, 
			ProductId,
			ShopId,
			dp.Value AS DepthValue 
		FROM GradeDepthPlan dp
			LEFT JOIN GradeShop gs
				ON gs.DateFrom = dp.DateFrom
					AND gs.GradeId = dp.GradeId	
	) dep
		ON dep.DateFrom = d.date
			AND dep.ProductId = d.product_id
			AND dep.ShopId = d.shop_id
			AND d.WH_Number = 1
GO

--SELECT * FROM vFactProductCalcRemain
--WHERE Date = '20171201'
--and product_id = 316
--and shop_id = 7148


--SELECT 
--	dp.DateFrom, 
--	ProductId,
--	ShopId,
--	dp.Value AS DepthValue 
--FROM GradeDepthPlan dp
--	LEFT JOIN GradeShop gs
--		ON gs.DateFrom = dp.DateFrom
--			AND gs.GradeId = dp.GradeId
--WHERE 
--	dp.DateFrom = '20171201'
--and ProductId = 316
--and ShopId = 7148
--SELECT * FROM products where id = 316
--SELECT * FROM Shops where id = 7148
----SELECT * FROM GradeDepthPlan where DateFrom = '20171201'

----update GradeDepthPlan set ProductId = 316
----where GradeDepthPlanId = 48

----SELECT * FROM GradeDepthPlan

----SELECT * FROM GradeShop where DateFrom = '20171201'
----SELECT * FROM GradeDepthPlan where DateFrom = '20171201'
----SELECT * FROM GradeShop


----SELECT * FROM vFactProductCalcRemain
----WHERE Date = '20171201'
----and product_id = 316
----and shop_id = 7148

----SELECT 
----	Date, product_id, shop_id, Count(*) 
----FROM vFactProductCalcRemain
----WHERE Date = '20171201'
----and product_id = 316
----GROUP BY Date, product_id, shop_id

----SELECT * FROM warehouses where id IN (467,653)


----product_calc_remain_fact