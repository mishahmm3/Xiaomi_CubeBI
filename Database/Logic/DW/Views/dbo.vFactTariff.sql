﻿IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'vFactTariff'))
BEGIN
	EXEC ('CREATE VIEW vFactTariff AS SELECT 1 [Default];')
END
GO
PRINT ' Altering VIEW vFactTariff'
GO
-- =======================================================
-- Author:		Михаил Морозов
-- Create date: 20180620
-- =======================================================
-- Description:	Данные тарифам/показателям 
-- =======================================================
ALTER VIEW vFactTariff
AS
	SELECT 
		DATEPART(YEAR, DateFrom) AS Year,
		DATEPART(MONTH, DateFrom) AS Month,
		DATEPART(DAY, DateFrom) AS Day,
		KSeason
	FROM TariffCalc ch 
GO
