﻿IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'vDimWarehouse'))
BEGIN
	EXEC ('CREATE VIEW vDimWarehouse AS SELECT 1 [Default];')
END
GO
PRINT ' Altering VIEW vDimWarehouse'
GO
-- =======================================================
-- Author:		Михаил Морозов
-- Create date: 20180710
-- =======================================================
-- Description:	Измерение Склад 
-- 
-- =======================================================
ALTER VIEW vDimWarehouse
AS
	SELECT
		 w.Id AS WarehouseId,    
		 w.Name AS  WarehouseName,
		 sh.Id AS ShopId,    
		 sh.Name AS  ShopName,
		 w.row_id
	FROM Warehouses w
		LEFT JOIN Shops sh
			ON sh.id = w.shop_id
GO

--SELECT * FROM vDimWarehouse


