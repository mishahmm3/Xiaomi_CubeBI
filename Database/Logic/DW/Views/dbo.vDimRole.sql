IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'vDimRole'))
BEGIN
	EXEC ('CREATE VIEW vDimRole AS SELECT 1 [Default];')
END
GO
PRINT ' Altering VIEW vDimRole'
GO
-- =======================================================
-- Author:		������ �������
-- Create date: 20180707
-- =======================================================
-- Description:	��������� ���� ����������
-- 
-- =======================================================
ALTER VIEW vDimRole
AS
	SELECT
		Id		AS RoleId,
		name	AS RoleName
	FROM Roles
GO
