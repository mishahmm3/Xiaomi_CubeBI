﻿IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'vDimCashDesk'))
BEGIN
	EXEC ('CREATE VIEW vDimCashDesk AS SELECT 1 [Default];')
END
GO
PRINT ' Altering VIEW vDimCashDesk'
GO
-- =======================================================
-- Author:		Михаил Морозов
-- Create date: 20180619
-- =======================================================
-- Description:	Измерение Касса
-- 
-- =======================================================
ALTER VIEW vDimCashDesk
AS
	SELECT        
		cd.Id AS CashDeskId, 
		cd.Name	AS CashDeskName,
		sh.ShopId,
		sh.ShopName,
		sh.DivisionId,
		sh.DivisionName,
		sh.CompanyId,
		sh.CompanyName,
		sh.CityId,
		sh.CityName,
		cd.row_id
	FROM cash_desks cd
		LEFT JOIN vDimShop sh
			ON sh.ShopId = cd.shop_id
GO

--SELECT * FROM vDimCashDesk
--SELECT * FROM vDimShop


