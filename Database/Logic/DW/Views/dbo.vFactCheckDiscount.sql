﻿IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'vFactCheckDiscount'))
BEGIN
	EXEC ('CREATE VIEW vFactCheckDiscount AS SELECT 1 [Default];')
END
GO
PRINT ' Altering VIEW vFactCheckDiscount'
GO
-- =======================================================
-- Author:		Михаил Морозов
-- Create date: 20180914
-- =======================================================
-- Description:	РГ Чеки_Скидки
-- =======================================================
ALTER VIEW vFactCheckDiscount
AS
	SELECT 
		-- Измерения
		CAST(head.date AS date) AS DateOnly,
		DATEADD(HOUR, DATEDIFF(HOUR, '', head.[date]), '') as [date],
		DATEPART(MINUTE, head.[date]) AS DateMinuteId,
		head.card_id,
		head.CheckOperationVidId,
		head.seller_id AS SellerId,
		head.Id AS DocumentId, 
		dis.product_id AS ProductId,
		head.cash_desk_id,
		cd.shop_id AS ShopId,
		-- Показатели
		dis.amount AS DiscountSumma,
		-- Служебные
		head.id AS CheckHeaderId,
		head.row_id AS CheckHeaderRowId
	FROM check_headers head WITH(NOLOCK) 
		INNER JOIN check_discounts dis WITH(NOLOCK) 
			ON dis.check_header_id = head.id
		LEFT JOIN cash_desks cd
			ON cd.id = head.cash_desk_id
GO

--SELECT * FROM vFactCheckDiscount
--SELECT * FROM X.rt_smart.[dbo].[_Document273_VT5909] where _Document273_IDRRef = 0xA0EBF40669E671EC11E7E48B12E69662
--SELECT * FROM vFactCheck where CheckHeaderRowId = 0xA0EBF40669E671EC11E7E48B12E69662


--SELECT * FROM X.rt_smart.[dbo].[_Document273_VT5909] where _Document273_IDRRef = 0xB7FF1C1B0DB421EC11E84AE4DA443661
--SELECT * FROM X.rt_smart.[dbo].[_Document273_VT5839] data WHERE _Document273_IDRRef = 0xB7FF1C1B0DB421EC11E84AE4DA443661

--SELECT * FROM check_headers where row_Id = 0x8B671C1B0DB4219511E86AF92FD0AB42
--SELECT * FROM check_headers where Id = 2760215
---- много позиций
--SELECT * FROM X.rt_smart.[dbo].[_Document273_VT5839] data WHERE _Document273_IDRRef = 0x8B671C1B0DB4219511E86AF92FD0AB42
--SELECT * FROM X.rt_smart.[dbo].[_Document273_VT5909] where _Document273_IDRRef = 0x8B671C1B0DB4219511E86AF92FD0AB42



--SELECT * FROM discounts
--SELECT * FROM X.rt_smart.[dbo].[_Reference141]

--SELECT * FROM check_discounts where check_header_id = 3035288
--SELECT * FROM vDimDiscount where DiscountId = 38
--SELECT * FROM vFactCheckDiscount
--SELECT * FROM check_discounts

--SELECT * FROM vDimDocument where  DocumentNumber = 'ЦО13-004997'


