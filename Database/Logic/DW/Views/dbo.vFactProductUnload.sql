﻿IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'vFactProductUnload'))
BEGIN
	EXEC ('CREATE VIEW vFactProductUnload AS SELECT 1 [Default];')
END
GO
PRINT ' Altering VIEW vFactProductUnload'
GO
-- =======================================================
-- Author:		Михаил Морозов
-- Create date: 20180617
-- =======================================================
-- Description:	Данные по товарам к отгрузке
-- =======================================================
ALTER VIEW vFactProductUnload
AS
	SELECT 
		DateFrom,
		WarehouseId,
		ProductId,
		CountUnload
	FROM ProductUnload ch 
GO
