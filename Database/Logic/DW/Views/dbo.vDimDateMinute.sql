IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'vDimDateMinute'))
BEGIN
	EXEC ('CREATE VIEW vDimDateMinute AS SELECT 1 [Default];')
END
GO
PRINT ' Altering VIEW vDimDateMinute'
GO
-- =======================================================
-- Author:		������ �������
-- Create date: 20180612
-- =======================================================
-- Description:	��������� ������ 
-- 
-- =======================================================
ALTER VIEW vDimDateMinute
AS
	SELECT 
		Digit1 * 10 + Digit2 AS DateMinuteId,
		'������ ' + CAST(Digit1 * 10 + Digit2 AS varchar(3)) AS DateMinuteName
	FROM 
		(	SELECT 0 AS Digit1 UNION ALL
			SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5
		) D1
		CROSS JOIN
		(	SELECT 0 AS Digit2 UNION ALL
			SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL 
			SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9
		) D2
GO

--SELECT * FROM vDimDateMinute
