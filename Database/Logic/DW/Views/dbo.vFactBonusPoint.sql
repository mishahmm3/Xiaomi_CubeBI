﻿IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'vFactBonusPoint'))
BEGIN
	EXEC ('CREATE VIEW vFactBonusPoint AS SELECT 1 [Default];')
END
GO
PRINT ' Altering VIEW vFactBonusPoint'
GO
-- =======================================================
-- Author:		Михаил Морозов
-- Create date: 20180612
-- =======================================================
-- Description:	РГ Бонусные баллы
-- 
-- =======================================================
ALTER VIEW vFactBonusPoint
AS
	SELECT      
		DateFrom,
		CardId,
		CountCharged,
		CountOff
	FROM BonusPoint
GO
