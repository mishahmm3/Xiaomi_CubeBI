IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'vDimPaymentVidType'))
BEGIN
	EXEC ('CREATE VIEW vDimPaymentVidType AS SELECT 1 [Default];')
END
GO
PRINT ' Altering VIEW vDimPaymentVidType'
GO
-- =======================================================
-- Author:		������ �������
-- Create date: 20180614
-- =======================================================
-- Description:	��������� ���� ���� �������
-- 
-- =======================================================
ALTER VIEW vDimPaymentVidType
AS
	SELECT 
		v.PaymentVidId,
		v.Code AS PaymentVidCode,
		v.Name AS PaymentVidName,
		t.PaymentTypeId,
		t.Code AS PaymentTypeCode,
		t.Name AS PaymentTypeName
	FROM PaymentVid v
		INNER JOIN PaymentType t
			ON t.PaymentTypeId = v.PaymentTypeId
GO
