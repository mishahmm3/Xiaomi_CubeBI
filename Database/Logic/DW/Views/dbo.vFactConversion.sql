﻿IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'vFactConversion'))
BEGIN
	EXEC ('CREATE VIEW vFactConversion AS SELECT 1 [Default];')
END
GO
PRINT ' Altering VIEW vFactConversion'
GO
-- =======================================================
-- Author:		Михаил Морозов
-- Create date: 20180629
-- =======================================================
-- Description:	Данные по посещениям
-- =======================================================
ALTER VIEW vFactConversion
AS
	SELECT 
		ISNULL(ch.DateFrom, v.date) AS DateFrom,
		ISNULL(ch.shop_id, v.shop_id) AS ShopId,
		ch.CheckCount,
		v.count AS VisitCount
	FROM 
	(
		SELECT		
			DATEADD(hour, DATEDIFF(HOUR, '', ch.[date]), '') AS DateFrom,
			cd.shop_id,
			COUNT(*) AS CheckCount
		FROM check_headers ch
			INNER JOIN cash_desks cd 
				ON ch.cash_desk_id = cd.id
		GROUP BY 
			DATEADD(hour, DATEDIFF(HOUR, '', ch.[date]), '') , cd.shop_id
	) ch	
	 FULL JOIN visit_logs v
		ON v.date = ch.DateFrom
			AND v.shop_id = ch.shop_id
GO

--SELECT * FROM vFactConversion
--where VisitCount is not null


--SELECT * FROM visit_logs