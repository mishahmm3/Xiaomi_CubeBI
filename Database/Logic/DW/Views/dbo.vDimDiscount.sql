﻿IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'vDimDiscount'))
BEGIN
	EXEC ('CREATE VIEW vDimDiscount AS SELECT 1 [Default];')
END
GO
PRINT ' Altering VIEW vDimDiscount'
GO
-- =======================================================
-- Author:		Михаил Морозов
-- Create date: 20180608
-- =======================================================
-- Description:	Измерение Скидки по акциям
-- 
-- =======================================================
ALTER VIEW vDimDiscount
AS
	SELECT 
		ad.MarketActionDiscountId	AS DimDiscountId,
		d.Id	AS DiscountId,
		d.Name	AS DiscountName,
		CAST(d.Name	AS varchar(40)) AS DiscountNameBrief,
		d.Value AS DiscountValue,
		sh.Id	AS ShopId,
		sh.Code AS ShopCode,
		sh.Name AS ShopName,
		a.MarketActionId,
		CASE WHEN ISNULL(a.Name, '') = ''
			THEN 'Название с Id= ' + CAST(a.MarketActionId AS varchar(10)) + ' не определено'
			ELSE a.Name
		END
		AS MarketActionName,
		a.DateFrom,
		COALESCE(CONVERT(VARCHAR(10), a.DateFrom, 104), 'не определено') AS DateFromName,
		a.DateTo,
		COALESCE(CONVERT(VARCHAR(10), a.DateTo, 104), 'не определено') AS DateToName
	FROM MarketActionDiscount ad
		INNER JOIN Discounts d
			ON d.id = ad.discount_id
		INNER JOIN MarketAction a
			ON a.MarketActionId = ad.MarketActionId
		INNER JOIN shops sh
			ON sh.id = ad.shop_Id
GO
