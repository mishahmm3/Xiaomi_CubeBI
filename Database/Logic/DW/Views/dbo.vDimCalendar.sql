IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'vDimCalendar'))
BEGIN
	EXEC ('CREATE VIEW vDimCalendar AS SELECT 1 [Default];')
END
GO
PRINT ' Altering VIEW vDimCalendar'
GO
-- =======================================================
-- Author:		������ �������
-- Create date: 20180613
-- =======================================================
-- Description:	��������� ���������
-- 
-- =======================================================
ALTER VIEW vDimCalendar
AS
	SELECT        
		[date], 
		CAST ([Date] AS DATE) AS DateOnly,
		[year], quater, [month], week, weekday, [day], [hour]
	FROM calendar
GO
