﻿IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'vDimShiftPlan'))
BEGIN
	EXEC ('CREATE VIEW vDimShiftPlan AS SELECT 1 [Default];')
END
GO
PRINT ' Altering VIEW vDimShiftPlan'
GO
-- =======================================================
-- Author:		Михаил Морозов
-- Create date: 20180910
-- =======================================================
-- Description:	Измерение Смены плановые 
-- =======================================================
ALTER VIEW vDimShiftPlan
AS
	SELECT 
		sp.Id AS DimShiftPlanId,
		date AS DateOnly, 
		sp.shop_id AS ShopId,
		sh.name AS ShopName,
		user_id AS UserId,
		u.Name AS UserName,
		role_id AS RoleId,
		r.name AS RoleName,
		shift_id AS ShiftId,
		s.Code AS ShiftCode, 
		s.Name AS ShiftName 
	FROM StaffPlan sp
		INNER JOIN Shift s
			ON s.ShiftId = sp.shift_id
		INNER JOIN roles r
 			ON r.id = sp.role_id
		INNER JOIN users u
 			ON u.id = sp.user_id
		INNER JOIN shops sh
 			ON sh.id = sp.shop_id
GO




