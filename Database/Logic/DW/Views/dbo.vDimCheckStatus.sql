IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'vDimCheckStatus'))
BEGIN
	EXEC ('CREATE VIEW vDimCheckStatus AS SELECT 1 [Default];')
END
GO
PRINT ' Altering VIEW vDimCheckStatus'
GO
-- =======================================================
-- Author:		������ �������
-- Create date: 201806019
-- =======================================================
-- Description:	��������� ������ ���� ���
-- 
-- =======================================================
ALTER VIEW vDimCheckStatus
AS
	SELECT
		 CheckStatusId,    
		 Code AS  CheckStatusCode,
		 Name AS  CheckStatusName
	FROM CheckStatus
GO
