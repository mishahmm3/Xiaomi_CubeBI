﻿IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'vDimShift'))
BEGIN
	EXEC ('CREATE VIEW vDimShift AS SELECT 1 [Default];')
END
GO
PRINT ' Altering VIEW vDimShift'
GO
-- =======================================================
-- Author:		Михаил Морозов
-- Create date: 20180910
-- =======================================================
-- Description:	Измерение Смены
-- =======================================================
ALTER VIEW vDimShift
AS
	SELECT 
		ShiftId,
		Code,
		Name
	FROM Shift
GO


