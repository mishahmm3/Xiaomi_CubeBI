﻿IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vMonthName]'))
BEGIN
	EXEC ('CREATE VIEW [dbo].[vMonthName] AS SELECT 1 [Default];')
END
GO
-- =======================================================
-- Author:		Михаил Морозов
-- Create date: 20180618
-- =======================================================
-- Вспомагательная view с названиями месяцев
-- =======================================================
ALTER VIEW [dbo].[vMonthName]
AS
	SELECT 0 AS MonthId, 'Не определено' AS MonthName
	UNION ALL
	SELECT 1 AS MonthId, 'Январь' 
	UNION ALL
	SELECT 2 AS MonthId, 'Февраль' 
	UNION ALL
	SELECT 3 AS MonthId, 'Март'
	UNION ALL
	SELECT 4 AS MonthId, 'Апрель' 
	UNION ALL
	SELECT 5 AS MonthId, 'Май' 
	UNION ALL
	SELECT 6 AS MonthId, 'Июнь' 
	UNION ALL
	SELECT 7 AS MonthId, 'Июль' 
	UNION ALL
	SELECT 8 AS MonthId, 'Август' 
	UNION ALL
	SELECT 9 AS MonthId, 'Сентябрь' 
	UNION ALL
	SELECT 10 AS MonthId, 'Октябрь' 
	UNION ALL
	SELECT 11 AS MonthId, 'Ноябрь' 
	UNION ALL
	SELECT 12 AS MonthId, 'Декабрь' 
GO
