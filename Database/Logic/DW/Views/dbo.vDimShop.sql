﻿IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'vDimShop'))
BEGIN
	EXEC ('CREATE VIEW vDimShop AS SELECT 1 [Default];')
END
GO
PRINT ' Altering VIEW vDimShop'
GO
-- =======================================================
-- Author:		Михаил Морозов
-- Create date: 20180613
-- =======================================================
-- Description:	Измерение Магазин
-- 
-- =======================================================
ALTER VIEW vDimShop
AS
	SELECT        
		sh.id	AS ShopId, 
		sh.Code AS ShopCode, 
		sh.name AS ShopName, 
		ISNULL(comp.Id, 0) AS CompanyId, 
		ISNULL(comp.name, 'Не присвоен') AS CompanyName, 
		ISNULL(c.Id, 0)	AS CityId, 
		ISNULL(c.name, 'Не присвоен')	AS CityName, 
		ISNULL(reg.Id, 0)	AS RegionId, 
		ISNULL(reg.name, 'Не присвоен') AS RegionName, 
		ISNULL(div.Id, 0)	AS DivisionId, 
		ISNULL(div.name, 'Не присвоен') AS DivisionName, 
		ISNULL(st.Id, 0)	AS ShopTypeId, 
		ISNULL(st.name, 'Не присвоен') AS ShopTypeName, 
		ISNULL(u.Id, 0)	AS RgmId,
		ISNULL(u.Name, 'Не присвоен')	AS RgmName
	FROM shops sh
		LEFT JOIN companies comp
			ON comp.Id = sh.company_id
		LEFT JOIN cities c
			ON c.id = sh.city_id
		LEFT JOIN regions reg
			ON reg.id = c.region_id
		LEFT JOIN divisions div
			ON div.id = sh.division_id
		LEFT JOIN users u
			ON div.user_id = u.id
		LEFT JOIN shop_types st
			ON st.id = sh.shop_type_id				 
GO
