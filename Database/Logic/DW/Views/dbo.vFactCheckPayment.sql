﻿IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'vFactCheckPayment'))
BEGIN
	EXEC ('CREATE VIEW vFactCheckPayment AS SELECT 1 [Default];')
END
GO
PRINT ' Altering VIEW vFactCheckPayment'
GO
-- =======================================================
-- Author:		Михаил Морозов
-- Create date: 20180614
-- =======================================================
-- Description:	РГ Платежи по чекам
-- 
-- =======================================================
ALTER VIEW vFactCheckPayment
AS
	SELECT 
		cp.CheckPaymentId, 
		CAST(ch.date AS date) AS DateOnly,
		DATEADD(HOUR, DATEDIFF(HOUR, '', ch.[date]), '') as [date],
		DATEPART(MINUTE, ch.[date]) AS DateMinuteId,
		ch.Id AS DocumentId, 
		cp.product_id AS ProductId,
		NULL AS operation_id,
		ch.cash_desk_id,
		cd.shop_id AS ShopId,
		ch.card_id,
		cp.PaymentVidId,
		ch.CheckOperationVidId,
		ch.seller_id AS SellerId,
		cp.Summa * CASE WHEN v.Code = '0' THEN 1 ELSE -1 END AS Summa,
		cp.CommisionSumma * CASE WHEN v.Code = '0' THEN 1 ELSE -1 END AS CommisionSumma,
		cp.CommisionPercent,
		cp.BonusSumma * CASE WHEN v.Code = '0' THEN 1 ELSE -1 END AS BonusSumma,
		ch.id AS CheckHeaderId,
		ch.row_id AS CheckHeaderRowId
	FROM check_headers ch WITH(NOLOCK) 
		INNER JOIN CheckPayment cp WITH(NOLOCK) 
			ON cp.check_header_id = ch.id
		LEFT JOIN cash_desks cd
			ON cd.id = ch.cash_desk_id
		LEFT JOIN CheckOperationVid v
			ON v.CheckOperationVidId = ch.CheckOperationVidId
GO
