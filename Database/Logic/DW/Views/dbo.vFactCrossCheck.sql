IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'vFactCrossCheck'))
BEGIN
	EXEC ('CREATE VIEW vFactCrossCheck AS SELECT 1 [Default];')
END
GO
PRINT ' Altering VIEW vFactCrossCheck'
GO
-- =======================================================
-- Author:		������ �������
-- Create date: 20180612
-- =======================================================
-- Description:	�� �����-����
-- 
-- =======================================================
ALTER VIEW vFactCrossCheck
AS
	SELECT        
		date AS DateFrom,
		ShopId,
		product_group1_id AS ProductGroup1Id,
		product_group2_id AS ProductGroup2Id,
		[count]
	FROM cross_checks
GO
