﻿IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'vFactMovement'))
BEGIN
	EXEC ('CREATE VIEW vFactMovement AS SELECT 1 [Default];')
END
GO
PRINT ' Altering VIEW vFactMovement'
GO
-- =======================================================
-- Author:		Михаил Морозов
-- Create date: 20180629
-- =======================================================
-- Description:	РГ движение товаров по складам 
-- =======================================================
ALTER VIEW vFactMovement
AS
	SELECT 
		m.MovementDocId AS DocumentId,
		DateFrom,
		FromDateFrom,
		ToDateFrom,
		WarehouseFromId,
		WarehouseToId,
		p.ProductId,
		ProductCount,
		FromPlanCount,
		FromFactCount,
		ToPlanCount,
		ToFactCount
	FROM MovementDoc m
		LEFT JOIN MovementDocProduct p
			ON p.MovementDocId = m.MovementDocId
		LEFT JOIN MovementDocFromTo ft
			ON ft.MovementDocId = p.MovementDocId
				AND ft.ProductId = p.ProductId
GO
