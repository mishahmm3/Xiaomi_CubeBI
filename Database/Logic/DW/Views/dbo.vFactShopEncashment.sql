﻿IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'vFactShopEncashment'))
BEGIN
	EXEC ('CREATE VIEW vFactShopEncashment AS SELECT 1 [Default];')
END
GO
PRINT ' Altering VIEW vFactShopEncashment'
GO
-- =======================================================
-- Author:		Михаил Морозов
-- Create date: 20180629
-- =======================================================
-- Description:	РГ движение товаров по складам 
-- =======================================================
ALTER VIEW vFactShopEncashment
AS
	SELECT 
		ISNULL(d1.DateFrom, d2.DateFrom) AS DateFrom, 
		DATEPART(YEAR, ISNULL(d1.DateFrom, d2.DateFrom)) AS Year,
		DATEPART(MONTH, ISNULL(d1.DateFrom, d2.DateFrom)) AS Month,
		DATEPART(DAY, ISNULL(d1.DateFrom, d2.DateFrom)) AS Day,
		ISNULL(d1.ShopId, d2.EntityId) AS ShopId, 
		d1.EncashmentValue,
		d1.RemainsValue,
		d2.Value AS NormaValue
	FROM ShopEncashment d1
		FULL JOIN v_ParameterDay d2
			ON d2.DateFrom = d1.DateFrom
				AND d2.EntityId = d1.ShopId
	WHERE 
		d2.ParameterCode = 'EncashmentNorma'
GO
