﻿IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'vDimProduct'))
BEGIN
	EXEC ('CREATE VIEW vDimProduct AS SELECT 1 [Default];')
END
GO
PRINT ' Altering VIEW vDimProduct'
GO
-- =======================================================
-- Author:		Михаил Морозов
-- Create date: 20180613
-- =======================================================
-- Description:	Измерение Товар
-- 
-- =======================================================
ALTER VIEW vDimProduct
AS
	SELECT        
		p.id AS ProductId, 
		p.sku AS ProductSku, 
		p.name AS ProductName, 
		ISNULL(pt.id, 0) AS ProductTypeId,
		ISNULL(pt.name, 'Прочий вид товара') AS ProductTypeName, 
		ISNULL(pb.id, 0) AS ProductBrandId,
		ISNULL(pb.name, 'Прочие брэнды и услуги') AS ProductBrandName,
		ISNULL(gp.id, 0) AS ProductGroupId,
		ISNULL(gp.name, 'Группа не определена') AS ProductGroupName
	FROM products p 
		LEFT JOIN product_types pt 
			ON p.product_type_id = pt.id 
		LEFT JOIN product_brands pb 
			ON pb.id = p.product_brand_id 
		LEFT JOIN product_group_products pgp 
			ON pgp.product_id = p.id
		LEFT JOIN product_groups gp 
			ON gp.id = pgp.product_group_id
GO




--SELECT product_id, count(*) FROM product_group_products 
--group by product_id
--where product_id = 223
