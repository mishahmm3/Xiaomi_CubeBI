﻿IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'vFactStaff'))
BEGIN
	EXEC ('CREATE VIEW vFactStaff AS SELECT 1 [Default];')
END
GO
PRINT ' Altering VIEW vFactStaff'
GO
-- =======================================================
-- Author:		Михаил Морозов
-- Create date: 20180707
-- =======================================================
-- Description:	РГ Данные по персоналу
-- =======================================================
ALTER VIEW vFactStaff
AS
	SELECT 
		ISNULL(d.DateFrom, st.DateFrom) AS DateFrom,
		DATEPART(YEAR, ISNULL(d.DateFrom, st.DateFrom)) AS Year,
		DATEPART(MONTH, ISNULL(d.DateFrom, st.DateFrom)) AS Month,
		DATEPART(DAY, ISNULL(d.DateFrom, st.DateFrom)) AS Day,
		ISNULL(d.ShopId, st.ShopId) AS ShopId,
		d.RoleId,
		d.ShiftId,
		d.StaffPlanCount,
		d.StaffFactCount,
		d.StaffNeedCount,
		st.StaffCount
	FROM 
	(
		SELECT 
			ISNULL(pl.DateFrom, f.DateFrom) AS DateFrom,
			ISNULL(pl.ShopId, f.ShopId) AS ShopId,
			ISNULL(pl.RoleId, f.RoleId) AS RoleId,
			ISNULL(pl.ShiftId, f.ShiftId) AS ShiftId,
			pl.StaffPlanCount,
			f.StaffFactCount,
			ROW_NUMBER() OVER (PARTITION BY ISNULL(pl.DateFrom, f.DateFrom), ISNULL(pl.ShopId, f.ShopId) ORDER BY pl.StaffPlanCount) AS Line_Number,
			ISNULL(pl.StaffPlanCount, 0) - ISNULL(f.StaffFactCount, 0) AS StaffNeedCount
		FROM 
		(
			SELECT
				shop_id AS ShopId,
				Role_id AS RoleId,
				Shift_id AS ShiftId,
				date AS DateFrom,
				DATEPART(YEAR, date) AS Year,
				DATEPART(MONTH, date) AS Month,
				DATEPART(DAY, date) AS Day,
				Count(*) AS StaffPlanCount
			FROM StaffPlan
			GROUP BY 
				shop_id, Role_id, Shift_id, date	
		) pl
		FULL JOIN 
		(
			SELECT
				ShopId,
				RoleId,
				ShiftId,
				DateFrom,
				Count(*) AS StaffFactCount
			FROM (
				SELECT DISTINCT
					CAST(ch.date AS Date) AS DateFrom,
					desk.ShopId,
					cd.User_Id AS UserId,
					Role_id AS RoleId,
					Shift_id AS ShiftId
				FROM check_headers ch 
					INNER JOIN check_details cd
						ON cd.check_header_id = ch.id
					LEFT JOIN vDimCashDesk desk
						ON desk.CashDeskId = ch.cash_desk_id	
					LEFT JOIN StaffPlan pl
						ON CAST(ch.date AS Date) = pl.Date
							AND pl.Shop_id = desk.ShopId
							AND pl.User_id = cd.User_Id
				WHERE 
					cd.User_Id IS NOT NULL
			) fact
			GROUP BY 
				ShopId, RoleId, ShiftId, DateFrom	
		) f
			ON f.DateFrom = pl.DateFrom
				AND f.ShopId = pl.ShopId
				AND ISNULL(f.RoleId, 0) = ISNULL(pl.RoleId, 0)
				AND ISNULL(f.ShiftId, 0) = ISNULL(pl.ShiftId, 0)
	) d
	FULL JOIN 
	(
		SELECT 
			DateFrom,
			EntityId AS ShopId,
			NULL AS RoleId,
			NULL AS ShiftId,
			Value AS StaffCount 
		FROM v_ParameterDay
		WHERE ParameterCode = 'ShopStaff'
	) st
		ON st.DateFrom = d.DateFrom
			AND st.ShopId = d.ShopId
			AND d.Line_Number = 1
GO

--SELECT * FROM vFactStaff WHERE datefrom = '20180403' and shopID IN (7146)
--			ROW_NUMBER() OVER (PARTITION BY [Date], pr.product_id, w.shop_id ORDER BY warehouse_id) AS WH_Number,
--SELECT * FROM vFactStaff
--WHERE DateFrom = '2018-04-01 00:00:00.000'
--AND shopId = 7219
--order by 1
--SELECT * FROM ParameterDay
--SELECT * FROM ParameterPeriodValue where datefrom = '2018-01-01 00:00:00.000'