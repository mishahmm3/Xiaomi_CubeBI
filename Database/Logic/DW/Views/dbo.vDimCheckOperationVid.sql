IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'vDimCheckOperationVid'))
BEGIN
	EXEC ('CREATE VIEW vDimCheckOperationVid AS SELECT 1 [Default];')
END
GO
PRINT ' Altering VIEW vDimCheckOperationVid'
GO
-- =======================================================
-- Author:		������ �������
-- Create date: 201806026
-- =======================================================
-- Description:	��������� ��� �������� �� ����
-- 
-- =======================================================
ALTER VIEW vDimCheckOperationVid
AS
	SELECT
		 CheckOperationVidId, 
		 Code AS CheckOperationVidCode,
		 Name AS CheckOperationVidName
	FROM CheckOperationVid
GO

