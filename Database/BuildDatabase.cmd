@echo off

Binaries\DatabaseCompiler\DatabaseCompiler.EXE "..\Database\Logic\OLTP\!OLTP.sql" "..\Database\_Output\2_OLTP_Logic.sql"
if %ERRORLEVEL% NEQ 0 goto errors

Binaries\DatabaseCompiler\DatabaseCompiler.EXE "..\Database\Logic\DW\!DW.sql" "..\Database\_Output\1_DW_Logic.sql"
if %ERRORLEVEL% NEQ 0 goto errors

@echo Ok
pause
exit /b 0

goto done

:errors

pause
exit /b 1
	
goto done

:done


