USE [master]
GO

/****** Object:  LinkedServer [X]    Script Date: 15.07.2019 11:58:55 ******/
EXEC master.dbo.sp_addlinkedserver @server = N'X', @srvproduct=N'', @provider=N'SQLNCLI', @datasrc=N'178.159.33.249'
 /* For security reasons the linked server remote logins password is changed with ######## */
EXEC master.dbo.sp_addlinkedsrvlogin @rmtsrvname=N'X',@useself=N'False',@locallogin=NULL,@rmtuser=N'devsmart1c',@rmtpassword='########'
GO

EXEC master.dbo.sp_serveroption @server=N'X', @optname=N'collation compatible', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'X', @optname=N'data access', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'X', @optname=N'dist', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'X', @optname=N'pub', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'X', @optname=N'rpc', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'X', @optname=N'rpc out', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'X', @optname=N'sub', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'X', @optname=N'connect timeout', @optvalue=N'0'
GO

EXEC master.dbo.sp_serveroption @server=N'X', @optname=N'collation name', @optvalue=null
GO

EXEC master.dbo.sp_serveroption @server=N'X', @optname=N'lazy schema validation', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'X', @optname=N'query timeout', @optvalue=N'0'
GO

EXEC master.dbo.sp_serveroption @server=N'X', @optname=N'use remote collation', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'X', @optname=N'remote proc transaction promotion', @optvalue=N'true'
GO


