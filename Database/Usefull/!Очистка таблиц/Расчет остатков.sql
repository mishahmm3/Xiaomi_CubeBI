EXEC [dbo].[Calc_ProductRemains] '20170101', '20170103'
EXEC [dbo].[Calc_ProductRemains] '20170103', '20170201'
EXEC [dbo].[Calc_ProductRemains] '20170201', '20170301'
EXEC [dbo].[Calc_ProductRemains] '20170301', '20170501'
EXEC [dbo].[Calc_ProductRemains] '20170501', '20170601'
EXEC [dbo].[Calc_ProductRemains] '20170601', '20170801'
EXEC [dbo].[Calc_ProductRemains] '20170801', '20170901'
EXEC [dbo].[Calc_ProductRemains] '20170901', '20171001'
EXEC [dbo].[Calc_ProductRemains] '20171001', '20171101'
EXEC [dbo].[Calc_ProductRemains] '20171101', '20171201'
EXEC [dbo].[Calc_ProductRemains] '20171201', '20180101'
EXEC [dbo].[Calc_ProductRemains] '20180101', '20180201'
EXEC [dbo].[Calc_ProductRemains] '20180201', '20180301'
EXEC [dbo].[Calc_ProductRemains] '20180301', '20180401'
EXEC [dbo].[Calc_ProductRemains] '20180401', '20180501'
EXEC [dbo].[Calc_ProductRemains] '20180501', '20180601'
EXEC [dbo].[Calc_ProductRemains] '20180601', '20180701'

--truncate table product_calc_remains
SELECT count(*) FROM product_calc_remains
--SELECT count(*) FROM product_calc_remains