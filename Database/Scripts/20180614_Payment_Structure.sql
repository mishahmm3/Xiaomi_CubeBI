--�������: ������������.����������������, ��� ������� ��������: Enum469, ����������: ��������
IF OBJECT_ID(N'[dbo].[PaymentType]', N'U') IS NULL	
BEGIN
	CREATE TABLE [dbo].[PaymentType](
		[PaymentTypeId] [int] IDENTITY(1,1) NOT NULL,
		[Code] [varchar](250) NULL,
		[Name] [varchar](250) NULL,
		[row_id] [binary](16) NULL,
	 CONSTRAINT [PK_PaymentType] PRIMARY KEY CLUSTERED 
	(
		[PaymentTypeId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
END

--�������: ����������.����������������, ��� ������� ��������: Reference44, ����������: ��������
IF OBJECT_ID(N'[dbo].[PaymentVid]', N'U') IS NULL	
BEGIN
	CREATE TABLE [dbo].PaymentVid(
		PaymentVidId [int] IDENTITY(1,1) NOT NULL,
		[Code] [varchar](250) NULL,
		[Name] [varchar](250) NULL,
		[PaymentTypeId] int NOT NULL,
		[row_id] [binary](16) NULL,
		CONSTRAINT [PK_PaymentVid] PRIMARY KEY CLUSTERED 
		(
			PaymentVidId
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]

	ALTER TABLE [dbo].PaymentVid  WITH CHECK ADD  CONSTRAINT [FK_PaymentVid_PaymentType] FOREIGN KEY([PaymentTypeId])
	REFERENCES [dbo].[PaymentType] ([PaymentTypeId])
	
	ALTER TABLE [dbo].PaymentVid CHECK CONSTRAINT [FK_PaymentVid_PaymentType]
END
GO


--�������: ��������.������.������, ��� ������� ��������: Document273.VT5869, ����������: ��������������
IF OBJECT_ID(N'[dbo].[CheckPayment]', N'U') IS NULL	
BEGIN
	CREATE TABLE [dbo].CheckPayment(
		CheckPaymentId [int] IDENTITY(1,1) NOT NULL,
		check_header_id int NOT NULL,
		PaymentVidId int NOT NULL,
		Summa [float] NULL,
		CommisionPercent [float] NULL,
		CommisionSumma [float] NULL,
		[row_id] [binary](16) NULL,
		CONSTRAINT [PK_CheckPayment] PRIMARY KEY CLUSTERED 
		(
			CheckPaymentId
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]

	ALTER TABLE [dbo].CheckPayment  WITH CHECK ADD  CONSTRAINT [FK_CheckPayment_check_header_id] FOREIGN KEY([check_header_id])
	REFERENCES [dbo].[check_headers] (Id)
	
	ALTER TABLE [dbo].CheckPayment CHECK CONSTRAINT [FK_CheckPayment_check_header_id]

	ALTER TABLE [dbo].CheckPayment  WITH CHECK ADD  CONSTRAINT [FK_CheckPayment_PaymentVid] FOREIGN KEY([PaymentVidId])
	REFERENCES [dbo].PaymentVid (PaymentVidId)
	
	ALTER TABLE [dbo].CheckPayment CHECK CONSTRAINT [FK_CheckPayment_PaymentVid]

END
GO

