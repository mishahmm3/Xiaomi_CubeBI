﻿DECLARE @Prev varchar(100) = 'X010'
DECLARE @New varchar(100) = 'X011'

-- Проверка на то, что предыдующий скрипт был выполнен на БД
IF NOT EXISTS(SELECT * FROM ScriptLog WHERE [Name] = @Prev)
BEGIN
	RAISERROR('Предыдущий скрипт "%s" не был выполнен', 20, 1, @Prev) WITH LOG
	RETURN
END

-- Проверка на то, что текущий скрипт еще не выполнялся
IF EXISTS(SELECT * FROM ScriptLog WHERE [Name]=@New)
BEGIN
	RAISERROR('Текущий скрипт "%s" уже был выполнен', 20, 1, @New) WITH LOG
	RETURN
END

--***************************************************************************************
-- Значимая часть скрипта 
--***************************************************************************************

--Надо 
truncate table check_details
truncate table check_bonuses
truncate table check_discounts
truncate table CheckPayment
delete from check_headers

 IF NOT EXISTS ( SELECT * FROM sys.columns WHERE [object_id] = object_id(N'dbo.check_details') AND [name] = 'operation_id' )
BEGIN
	ALTER TABLE dbo.check_details 
		ADD operation_id int NULL
END
GO

IF NOT EXISTS ( SELECT * FROM sys.columns WHERE [object_id] = object_id(N'dbo.check_headers') AND [name] = 'seller_id' )
BEGIN
	ALTER TABLE dbo.check_headers 
		ADD seller_id int NULL
END
GO

ALTER TABLE [dbo].check_headers  WITH CHECK ADD  CONSTRAINT [FK_check_headers_users] FOREIGN KEY([seller_id])
REFERENCES [dbo].[users] ([id])
GO

ALTER TABLE [dbo].check_headers CHECK CONSTRAINT [FK_check_headers_users]
GO


--***************************************************************************************
-- Регистрация текущего стрипта как выполненного в БД
--***************************************************************************************
GO
DECLARE @New varchar(100) = 'X011'
INSERT INTO [ScriptLog]([Name])
VALUES (@New)
GO

--delete from [ScriptLog] where  [Name] = 'X010'
--SELECT * FROM [ScriptLog]








