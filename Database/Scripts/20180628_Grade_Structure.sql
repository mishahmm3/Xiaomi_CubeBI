-- ������ 
IF OBJECT_ID(N'[dbo].[Grade]', N'U') IS NULL	
BEGIN
	CREATE TABLE [dbo].Grade(
		GradeId [int] IDENTITY(1,1) NOT NULL,
		Code varchar(100) NOT NULL,
		Name varchar(250) NOT NULL,
		CONSTRAINT [PK_Grade] PRIMARY KEY CLUSTERED 
		(
			GradeId
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
		CONSTRAINT [UC_Grade_Code] UNIQUE NONCLUSTERED 
		(
			Code ASC
		)
		WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
		) ON [PRIMARY]
END
GO

-- ������ �� ��������� � ������ �������
IF OBJECT_ID(N'[dbo].[GradeShop]', N'U') IS NULL	
BEGIN
	CREATE TABLE [dbo].GradeShop(
		GradeShopId [int] IDENTITY(1,1) NOT NULL,
		PeriodTypeId int NOT NULL,
		DateFrom datetime NOT NULL,
		GradeId int NOT NULL,
		ShopId int NOT NULL,
		CONSTRAINT [PK_GradeShop] PRIMARY KEY CLUSTERED 
		(
			GradeShopId
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
		CONSTRAINT [UC_GradeShop_Code] UNIQUE NONCLUSTERED 
		(
			PeriodTypeId ASC,
			DateFrom ASC,
			GradeId ASC,
			ShopId ASC
		)
		WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
		) ON [PRIMARY]

	ALTER TABLE [dbo].GradeShop  WITH CHECK ADD  CONSTRAINT [FK_GradeShop_PeriodType] FOREIGN KEY(PeriodTypeId)
	REFERENCES [dbo].PeriodType (PeriodTypeId)
	
	ALTER TABLE [dbo].GradeShop CHECK CONSTRAINT [FK_GradeShop_PeriodType]

	ALTER TABLE [dbo].GradeShop  WITH CHECK ADD  CONSTRAINT [FK_GradeShop_Grade] FOREIGN KEY(GradeId)
	REFERENCES [dbo].Grade (GradeId)
	
	ALTER TABLE [dbo].GradeShop CHECK CONSTRAINT [FK_GradeShop_Grade]

	ALTER TABLE [dbo].GradeShop  WITH CHECK ADD  CONSTRAINT [FK_GradeShop_Shop] FOREIGN KEY(ShopId)
	REFERENCES [dbo].Shops (Id)
	
	ALTER TABLE [dbo].GradeShop CHECK CONSTRAINT [FK_GradeShop_Shop]
END
GO

--������� GradeWidthPlan(GradeWidthPlanId, PeriodTypeId, DateFrom, GradeId, ProductGroupId, Value)
IF OBJECT_ID(N'[dbo].[GradeWidthPlan]', N'U') IS NULL	
BEGIN
	CREATE TABLE [dbo].GradeWidthPlan(
		GradeWidthPlanId [int] IDENTITY(1,1) NOT NULL,
		PeriodTypeId int NOT NULL,
		DateFrom datetime NOT NULL,
		GradeId int NOT NULL,
		ProductGroupId int NOT NULL,
		Value int
		CONSTRAINT [PK_GradeWidthPlan] PRIMARY KEY CLUSTERED 
		(
			GradeWidthPlanId
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
		CONSTRAINT [UC_GradeWidthPlan_Code] UNIQUE NONCLUSTERED 
		(
			PeriodTypeId ASC,
			DateFrom ASC,
			GradeId ASC,
			ProductGroupId ASC
		)
		WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
		) ON [PRIMARY]

	ALTER TABLE [dbo].GradeWidthPlan  WITH CHECK ADD  CONSTRAINT [FK_GradeWidthPlan_PeriodType] FOREIGN KEY(PeriodTypeId)
	REFERENCES [dbo].PeriodType (PeriodTypeId)
	
	ALTER TABLE [dbo].GradeWidthPlan CHECK CONSTRAINT [FK_GradeWidthPlan_PeriodType]

	ALTER TABLE [dbo].GradeWidthPlan  WITH CHECK ADD  CONSTRAINT [FK_GradeWidthPlan_Grade] FOREIGN KEY(GradeId)
	REFERENCES [dbo].Grade (GradeId)
	
	ALTER TABLE [dbo].GradeWidthPlan CHECK CONSTRAINT [FK_GradeWidthPlan_Grade]

	ALTER TABLE [dbo].GradeWidthPlan  WITH CHECK ADD  CONSTRAINT [FK_GradeWidthPlan_ProductGroup] FOREIGN KEY(ProductGroupId)
	REFERENCES [dbo].product_groups (Id)
	
	ALTER TABLE [dbo].GradeWidthPlan CHECK CONSTRAINT [FK_GradeWidthPlan_ProductGroup]
END
GO

--������� GradeDepthPlan(GradeDepthPlanId, PeriodTypeId, DateFrom, GradeId, ProductGroupId, Value)
IF OBJECT_ID(N'[dbo].[GradeDepthPlan]', N'U') IS NULL	
BEGIN
	CREATE TABLE [dbo].GradeDepthPlan(
		GradeDepthPlanId [int] IDENTITY(1,1) NOT NULL,
		PeriodTypeId int NOT NULL,
		DateFrom datetime NOT NULL,
		GradeId int NOT NULL,
		ProductId int NOT NULL,
		Value int
		CONSTRAINT [PK_GradeDepthPlan] PRIMARY KEY CLUSTERED 
		(
			GradeDepthPlanId
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
		CONSTRAINT [UC_GradeDepthPlan_Code] UNIQUE NONCLUSTERED 
		(
			PeriodTypeId ASC,
			DateFrom ASC,
			GradeId ASC,
			ProductId ASC
		)
		WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
		) ON [PRIMARY]

	ALTER TABLE [dbo].GradeDepthPlan  WITH CHECK ADD  CONSTRAINT [FK_GradeDepthPlan_PeriodType] FOREIGN KEY(PeriodTypeId)
	REFERENCES [dbo].PeriodType (PeriodTypeId)
	
	ALTER TABLE [dbo].GradeDepthPlan CHECK CONSTRAINT [FK_GradeDepthPlan_PeriodType]

	ALTER TABLE [dbo].GradeDepthPlan  WITH CHECK ADD  CONSTRAINT [FK_GradeDepthPlan_Grade] FOREIGN KEY(GradeId)
	REFERENCES [dbo].Grade (GradeId)
	
	ALTER TABLE [dbo].GradeDepthPlan CHECK CONSTRAINT [FK_GradeDepthPlan_Grade]

	ALTER TABLE [dbo].GradeDepthPlan  WITH CHECK ADD  CONSTRAINT [FK_GradeDepthPlan_Product] FOREIGN KEY(ProductId)
	REFERENCES [dbo].products (Id)
	
	ALTER TABLE [dbo].GradeDepthPlan CHECK CONSTRAINT [FK_GradeDepthPlan_Product]
END
GO


