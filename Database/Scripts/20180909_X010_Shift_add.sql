﻿DECLARE @Prev varchar(100) = 'X009'
DECLARE @New varchar(100) = 'X010'

-- Проверка на то, что предыдующий скрипт был выполнен на БД
IF NOT EXISTS(SELECT * FROM ScriptLog WHERE [Name] = @Prev)
BEGIN
	RAISERROR('Предыдущий скрипт "%s" не был выполнен', 20, 1, @Prev) WITH LOG
	RETURN
END

-- Проверка на то, что текущий скрипт еще не выполнялся
IF EXISTS(SELECT * FROM ScriptLog WHERE [Name]=@New)
BEGIN
	RAISERROR('Текущий скрипт "%s" уже был выполнен', 20, 1, @New) WITH LOG
	RETURN
END

--***************************************************************************************
-- Значимая часть скрипта 
--***************************************************************************************
--drop table Shift
IF OBJECT_ID(N'[dbo].[Shift]', N'U') IS NULL	
BEGIN
	CREATE TABLE [dbo].Shift(
		ShiftId [int] IDENTITY(1,1) NOT NULL CONSTRAINT [PK_Shift] PRIMARY KEY CLUSTERED,
		[Code] [varchar](255) NOT NULL,
		[Name] [varchar](255) NOT NULL,
		[HourFrom] [int] NOT NULL,
		[HourTo] [int] NOT NULL,
		CONSTRAINT [UC_Shift_Code] UNIQUE NONCLUSTERED 
		(
			Code
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo',
		@level1type=N'TABLE',@level1name=N'Shift',
		@value=N'Смена в магазине'
END

IF NOT EXISTS(SELECT * FROM Shift WHERE code = 'I')
BEGIN
	INSERT INTO Shift(Code, Name, HourFrom, HourTo)
	SELECT 'I', 'Утренняя смена', 8, 14
	UNION ALL
	SELECT 'II', 'Дневная смена', 14, 20
	UNION ALL
	SELECT 'III', 'Вечерняя смена', 20, 12
	UNION ALL
	SELECT 'В', 'Выходной', 0, 24
END

IF OBJECT_ID(N'[dbo].[Shifts]', N'U') IS NOT NULL	
BEGIN
	DROP TABLE Shifts
END


IF OBJECT_ID(N'[dbo].[staff_plans]', N'U') IS NOT NULL	
BEGIN
	DROP TABLE staff_plans
END

IF OBJECT_ID(N'[dbo].[StaffPlan]', N'U') IS NOT NULL	
BEGIN
	DROP TABLE StaffPlan

	CREATE TABLE [dbo].[StaffPlan](
		[id] [int] IDENTITY(1,1) NOT NULL,
		[shop_id] [int] NOT NULL,
		[user_id] [int] NOT NULL,
		[role_id] [int] NOT NULL,
		[shift_id] [int] NOT NULL,
		[date] [datetime] NOT NULL,
		[created_at] [datetime] NULL,
		[updated_at] [datetime] NULL,
		[deleted_at] [datetime] NULL,
	 CONSTRAINT [PK_shop_shifts] PRIMARY KEY CLUSTERED 
	(
		[shop_id] ASC,
		[user_id] ASC,
		[role_id] ASC,
		[shift_id] ASC,
		[date] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[StaffPlan]  WITH CHECK ADD  CONSTRAINT [FK_shop_shifts_roles] FOREIGN KEY([role_id])
	REFERENCES [dbo].[roles] ([id])

	ALTER TABLE [dbo].[StaffPlan] CHECK CONSTRAINT [FK_shop_shifts_roles]

	ALTER TABLE [dbo].[StaffPlan]  WITH CHECK ADD  CONSTRAINT [FK_shop_shifts_shifts] FOREIGN KEY([shift_id])
	REFERENCES [dbo].[shift] ([ShiftId])

	ALTER TABLE [dbo].[StaffPlan] CHECK CONSTRAINT [FK_shop_shifts_shifts]

	ALTER TABLE [dbo].[StaffPlan]  WITH CHECK ADD  CONSTRAINT [FK_shop_shifts_shops] FOREIGN KEY([shop_id])
	REFERENCES [dbo].[shops] ([id])

	ALTER TABLE [dbo].[StaffPlan] CHECK CONSTRAINT [FK_shop_shifts_shops]

	ALTER TABLE [dbo].[StaffPlan]  WITH CHECK ADD  CONSTRAINT [FK_shop_shifts_users] FOREIGN KEY([user_id])
	REFERENCES [dbo].[users] ([id])

	ALTER TABLE [dbo].[StaffPlan] CHECK CONSTRAINT [FK_shop_shifts_users]
END

--***************************************************************************************
-- Регистрация текущего стрипта как выполненного в БД
--***************************************************************************************
GO
DECLARE @New varchar(100) = 'X010'
INSERT INTO [ScriptLog]([Name])
VALUES (@New)
GO

--delete from [ScriptLog] where  [Name] = 'X010'
--SELECT * FROM [ScriptLog]








