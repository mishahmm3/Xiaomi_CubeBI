DECLARE @Prev varchar(100) = 'X006'
DECLARE @New varchar(100) = 'X007'

-- �������� �� ��, ��� ����������� ������ ��� �������� �� ��
IF NOT EXISTS(SELECT * FROM ScriptLog WHERE [Name] = @Prev)
BEGIN
	RAISERROR('���������� ������ "%s" �� ��� ��������', 20, 1, @Prev) WITH LOG
	RETURN
END

-- �������� �� ��, ��� ������� ������ ��� �� ����������
IF EXISTS(SELECT * FROM ScriptLog WHERE [Name]=@New)
BEGIN
	RAISERROR('������� ������ "%s" ��� ��� ��������', 20, 1, @New) WITH LOG
	RETURN
END

--***************************************************************************************
-- �������� ����� ������� 
--***************************************************************************************
IF OBJECT_ID(N'[dbo].[ShopEncashmentNorma]', N'U') IS NOT NULL	
DROP TABLE ShopEncashmentNorma

IF OBJECT_ID(N'[dbo].[ShopEncashment]', N'U') IS NOT NULL	
DROP TABLE ShopEncashment

-- ������ �� ����������
IF OBJECT_ID(N'[dbo].[ShopEncashment]', N'U') IS NULL	
BEGIN
	CREATE TABLE [dbo].ShopEncashment(
		[ShopEncashmentId] [int] IDENTITY(1,1) NOT NULL CONSTRAINT [PK_ShopEncashment] PRIMARY KEY CLUSTERED,
		DateFrom [datetime] NOT NULL,
		ShopId int NULL,
		EncashmentValue [float],
		RemainsValue [float],
		 CONSTRAINT [UC_ShopEncashment_Main] UNIQUE NONCLUSTERED 
			(
				DateFrom ASC,
				ShopId ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
		) ON [PRIMARY]

	ALTER TABLE [dbo].ShopEncashment  WITH CHECK ADD  CONSTRAINT [FK_ShopEncashment_Shop] FOREIGN KEY(ShopId)
	REFERENCES [dbo].Shops ([id])
	
	ALTER TABLE [dbo].ShopEncashment CHECK CONSTRAINT [FK_ShopEncashment_Shop]

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ����������' , @level0type=N'SCHEMA',@level0name=N'dbo', 
			@level1type=N'TABLE',@level1name=N'ShopEncashment', @level2type=N'COLUMN',@level2name=N'EncashmentValue'
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� �� � �������� �� ������ ���' , @level0type=N'SCHEMA',@level0name=N'dbo', 
			@level1type=N'TABLE',@level1name=N'ShopEncashment', @level2type=N'COLUMN',@level2name=N'RemainsValue'

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ����������' , @level0type=N'SCHEMA',@level0name=N'dbo',
			@level1type=N'TABLE',@level1name=N'ShopEncashment'
END 
--***************************************************************************************
-- ����������� �������� ������� ��� ������������ � ��
--***************************************************************************************
GO
DECLARE @New varchar(100) = 'X007'
INSERT INTO [ScriptLog]([Name])
VALUES (@New)
GO

--delete from [ScriptLog] where  [Name] = 'X007'
--SELECT * FROM [ScriptLog]








