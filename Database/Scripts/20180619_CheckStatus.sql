--������ ����
IF OBJECT_ID(N'[dbo].[CheckStatus]', N'U') IS NULL	
BEGIN
	CREATE TABLE [dbo].[CheckStatus](
		[CheckStatusId] [int] IDENTITY(1,1) NOT NULL,
		[Code] [varchar](250) NULL,
		[Name] [varchar](250) NULL,
		[row_id] [binary](16) NULL,
	 CONSTRAINT [PK_CheckStatus] PRIMARY KEY CLUSTERED 
	(
		[CheckStatusId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
END

IF NOT EXISTS ( SELECT * FROM sys.columns WHERE [object_id] = object_id(N'dbo.check_headers') AND [name] = 'CheckStatusId' )
BEGIN
	ALTER TABLE dbo.check_headers 
		ADD CheckStatusId  int NULL

	ALTER TABLE [dbo].check_headers  WITH CHECK ADD  CONSTRAINT [FK_check_headers_CheckStatus] FOREIGN KEY(CheckStatusId)
	REFERENCES [dbo].CheckStatus (CheckStatusId)
END
