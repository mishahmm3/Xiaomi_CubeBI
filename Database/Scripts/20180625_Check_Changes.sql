--��� �������� �� ����
IF OBJECT_ID(N'[dbo].[CheckOperationVid]', N'U') IS NULL	
BEGIN
	CREATE TABLE [dbo].CheckOperationVid(
		[CheckOperationVidId] [int] IDENTITY(1,1) NOT NULL,
		[Code] [varchar](250) NULL,
		[Name] [varchar](250) NULL,
		[row_id] [binary](16) NULL,
	 CONSTRAINT [PK_CheckOperationVid] PRIMARY KEY CLUSTERED 
	(
		[CheckOperationVidId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
END

IF NOT EXISTS ( SELECT * FROM sys.columns WHERE [object_id] = object_id(N'dbo.check_headers') AND [name] = 'CheckOperationVidId' )
BEGIN
	ALTER TABLE dbo.check_headers 
		ADD CheckOperationVidId  int NULL

	ALTER TABLE [dbo].check_headers  WITH CHECK ADD  CONSTRAINT [FK_check_headers_CheckOperationV] FOREIGN KEY(CheckOperationVidId)
	REFERENCES [dbo].CheckOperationVid (CheckOperationVidId)
END


IF NOT EXISTS ( SELECT * FROM sys.columns WHERE [object_id] = object_id(N'dbo.check_headers') AND [name] = 'Number' )
BEGIN
	ALTER TABLE dbo.check_headers 
		ADD Number varchar(250) NULL
END

IF NOT EXISTS ( SELECT * FROM sys.columns WHERE [object_id] = object_id(N'dbo.check_headers') AND [name] = 'CheckOperationVidId' )
BEGIN
	ALTER TABLE dbo.check_headers 
		ADD CheckOperationVidId int NULL
END

