﻿IF OBJECT_id(N'[dbo].[file_logs]', N'U') IS NOT NULL	
BEGIN
	DELETE FROM file_logs
	DROP TABLE [dbo].file_logs
END 

IF OBJECT_id(N'[dbo].[FileType]', N'U') IS NOT NULL	
BEGIN
	DELETE FROM FileType
	DROP TABLE [dbo].FileType
END 

IF OBJECT_id(N'[dbo].[FileStatus]', N'U') IS NOT NULL	
BEGIN
	DELETE FROM FileStatus
	DROP TABLE [dbo].FileStatus
END 

IF OBJECT_id(N'[dbo].[file_statuses]', N'U') IS NOT NULL	
BEGIN
	DELETE FROM file_statuses
	DROP TABLE [dbo].file_statuses
END 

-------------------------------------------------------------------------
IF OBJECT_id(N'[dbo].[file_types]', N'U') IS NULL	
BEGIN
	CREATE TABLE [dbo].file_types(
		id [int] identity(1, 1) NOT NULL,
		code varchar(100) NOT NULL,
		name varchar(100) NOT NULL,
		CONSTRAINT [PK_file_types] PRIMARY KEY CLUSTERED 
		(
			id
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
		CONSTRAINT [UC_file_types_code] UNIQUE NONCLUSTERED 
		(
			code ASC
		)
		WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
		) ON [PRIMARY]
END
GO

IF NOT EXISTS(SELECT * FROM file_types WHERE code = 'Visit')
BEGIN
	INSERT INTO file_types(code, name)
	SELECT 'Visit', 'Посещения клиентов'
END

IF NOT EXISTS(SELECT * FROM file_types WHERE code = 'Shift')
BEGIN
	INSERT INTO file_types(code, name)
	SELECT 'Shift', 'Смены'
END

IF NOT EXISTS(SELECT * FROM file_types WHERE code = 'KSeason')
BEGIN
	INSERT INTO file_types(code, name)
	SELECT 'KSeason', 'Коэффициенты сезонности'
END

IF NOT EXISTS(SELECT * FROM file_types WHERE code = 'Grade')
BEGIN
	INSERT INTO file_types(code, name)
	SELECT 'Grade', 'Грейды'
END

IF NOT EXISTS(SELECT * FROM file_types WHERE code = 'GradeWidth')
BEGIN
	INSERT INTO file_types(code, name)
	SELECT 'GradeWidth', 'Данные по ширине'
END


IF NOT EXISTS(SELECT * FROM file_types WHERE code = 'GradeDepth')
BEGIN
	INSERT INTO file_types(code, name)
	SELECT 'GradeDepth', 'Данные по глубине'
END

IF NOT EXISTS(SELECT * FROM file_types WHERE code = 'ProductGroup')
BEGIN
	INSERT INTO file_types(code, name)
	SELECT 'ProductGroup', 'Группы продуктов'
END


IF NOT EXISTS(SELECT * FROM file_types WHERE code = 'ShopStaff')
BEGIN
	INSERT INTO file_types(code, name)
	SELECT 'ShopStaff', 'Штатное расписание магазина'
END

--------------------------------------------------------------
--
--------------------------------------------------------------
IF OBJECT_id(N'[dbo].[file_status]', N'U') IS NULL	
BEGIN
	CREATE TABLE [dbo].file_status(
		id [int] identity(1, 1) NOT NULL,
		code varchar(100) NOT NULL,
		name varchar(100) NOT NULL,
		CONSTRAINT [PK_file_status] PRIMARY KEY CLUSTERED 
		(
			id
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
		CONSTRAINT [UC_file_status_code] UNIQUE NONCLUSTERED 
		(
			code ASC
		)
		WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
		) ON [PRIMARY]
END
GO

IF NOT EXISTS(SELECT * FROM file_status WHERE code = 'Created')
BEGIN
	INSERT INTO file_status(code, name)
	SELECT 'Created', 'Создан'
END

-- 
IF NOT EXISTS(SELECT * FROM file_status WHERE code = 'Ready')
BEGIN
	INSERT INTO file_status(code, name)
	SELECT 'Ready', 'Готов к обработке'
END

-- 
IF NOT EXISTS(SELECT * FROM file_status WHERE code = 'Processed')
BEGIN
	INSERT INTO file_status(code, name)
	SELECT 'Processed', 'Обработан'
END

--------------------------------------------------------------
--
--------------------------------------------------------------
IF OBJECT_id(N'[dbo].[file_logs]', N'U') IS NULL	
BEGIN
	CREATE TABLE [dbo].[file_logs](
		[id] [int] idENTITY(1,1) NOT NULL,
		[file_name] [varchar](255) NULL,
		[upload_date] [datetime] NULL,
		[parse_date] [datetime] NULL,
		[path] [varchar](max) NULL,
		[description] [varchar](max) NULL,
		[created_at] [datetime] NULL,
		[updated_at] [datetime] NULL,
		[deleted_at] [datetime] NULL,
		[original_file_name] [varchar](255) NULL,
		[content] ntext NULL,
		[file_status_id] [int] NULL,
		[file_type_id] [int] NULL,
	 CONSTRAINT [PK_file_logs] PRIMARY KEY CLUSTERED 
	(
		[id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


	ALTER TABLE [dbo].[file_logs]  WITH CHECK ADD  CONSTRAINT [FK_file_logs_file_status] FOREIGN KEY([file_status_id])
	REFERENCES [dbo].[file_status] ([id])

	ALTER TABLE [dbo].[file_logs] CHECK CONSTRAINT [FK_file_logs_file_status]

	ALTER TABLE [dbo].[file_logs]  WITH CHECK ADD  CONSTRAINT [FK_file_logs_file_types] FOREIGN KEY([file_type_id])
	REFERENCES [dbo].[file_types] ([id])

	ALTER TABLE [dbo].[file_logs] CHECK CONSTRAINT [FK_file_logs_file_types]

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'file_logs', @level2type=N'COLUMN',@level2name=N'id'
END











