﻿DECLARE @Prev varchar(100) = 'X007'
DECLARE @New varchar(100) = 'X008'

-- Проверка на то, что предыдующий скрипт был выполнен на БД
IF NOT EXISTS(SELECT * FROM ScriptLog WHERE [Name] = @Prev)
BEGIN
	RAISERROR('Предыдущий скрипт "%s" не был выполнен', 20, 1, @Prev) WITH LOG
	RETURN
END

-- Проверка на то, что текущий скрипт еще не выполнялся
IF EXISTS(SELECT * FROM ScriptLog WHERE [Name]=@New)
BEGIN
	RAISERROR('Текущий скрипт "%s" уже был выполнен', 20, 1, @New) WITH LOG
	RETURN
END

--***************************************************************************************
-- Значимая часть скрипта 
--***************************************************************************************
 IF NOT EXISTS ( SELECT * FROM sys.columns WHERE [object_id] = object_id(N'dbo.cross_checks') AND [name] = 'ShopId' )
BEGIN
	ALTER TABLE dbo.cross_checks 
		ADD ShopId int NULL
END
GO

--***************************************************************************************
-- Регистрация текущего стрипта как выполненного в БД
--***************************************************************************************
DECLARE @New varchar(100) = 'X008'
INSERT INTO [ScriptLog]([Name])
VALUES (@New)
GO

--delete from [ScriptLog] where  [Name] = 'X004'
--SELECT * FROM [ScriptLog]








