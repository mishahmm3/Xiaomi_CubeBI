﻿DECLARE @Prev varchar(100) = 'X004'
DECLARE @New varchar(100) = 'X005'

-- Проверка на то, что предыдующий скрипт был выполнен на БД
IF NOT EXISTS(SELECT * FROM ScriptLog WHERE [Name] = @Prev)
BEGIN
	RAISERROR('Предыдущий скрипт "%s" не был выполнен', 20, 1, @Prev) WITH LOG
	RETURN
END

-- Проверка на то, что текущий скрипт еще не выполнялся
IF EXISTS(SELECT * FROM ScriptLog WHERE [Name]=@New)
BEGIN
	RAISERROR('Текущий скрипт "%s" уже был выполнен', 20, 1, @New) WITH LOG
	RETURN
END

--***************************************************************************************
-- Значимая часть скрипта 
--***************************************************************************************
 IF NOT EXISTS ( SELECT * FROM sys.columns WHERE [object_id] = object_id(N'dbo.Parameter') AND [name] = 'EntityCode' )
BEGIN
	ALTER TABLE dbo.Parameter 
		ADD EntityCode varchar(100) NULL
END

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Код сущности, к которой привязан параметр' , @level0type=N'SCHEMA',@level0name=N'dbo', 
		@level1type=N'TABLE',@level1name=N'Parameter', @level2type=N'COLUMN',@level2name=N'EntityCode'

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Параметры привязанные ко временному периоду и какому-нибудь объекту (магазинб город и т.д.)' , @level0type=N'SCHEMA',@level0name=N'dbo',
		@level1type=N'TABLE',@level1name=N'Parameter'
GO

IF NOT EXISTS ( SELECT * FROM sys.columns WHERE [object_id] = object_id(N'dbo.ParameterPeriodValue') AND [name] = 'EntityId' )
BEGIN
	ALTER TABLE dbo.ParameterPeriodValue 
		ADD EntityId int NULL
END

IF EXISTS(SELECT * FROM  sys.objects WHERE Name = 'UC_PeriodValueDetail_NoDoubles')
BEGIN
	PRINT '  Drop FK constraint dbo.[file_logs].[UC_PeriodValueDetail_NoDoubles]';
	ALTER TABLE [dbo].ParameterPeriodValue DROP CONSTRAINT [UC_PeriodValueDetail_NoDoubles]
END


-- Таблица рассчитанных по дням параметров
IF OBJECT_ID(N'[dbo].[ParameterDay]', N'U') IS NOT NULL	
DROP TABLE ParameterDay

CREATE TABLE [dbo].ParameterDay(
	ParameterDayId [int] IDENTITY(1,1) NOT NULL,
	DateFrom datetime, 
	ParameterId [int] NOT NULL,
	EntityId int,
	Value float, 
	CONSTRAINT [PK_ParameterDay] PRIMARY KEY CLUSTERED 
	(
	ParameterDayId ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	 CONSTRAINT [UC_ParameterDay_Main] UNIQUE NONCLUSTERED 
	(
		DateFrom ASC,
		ParameterId ASC,
		EntityId ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]

) ON [PRIMARY]

--***************************************************************************************
-- Регистрация текущего стрипта как выполненного в БД
--***************************************************************************************

DECLARE @New varchar(100) = 'X005'
INSERT INTO [ScriptLog]([Name])
VALUES (@New)
GO

--delete from [ScriptLog] where  [Name] = 'X004'
--SELECT * FROM [ScriptLog]








