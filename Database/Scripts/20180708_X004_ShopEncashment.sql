DECLARE @Prev varchar(100) = 'X003'
DECLARE @New varchar(100) = 'X004'

-- �������� �� ��, ��� ����������� ������ ��� �������� �� ��
IF NOT EXISTS(SELECT * FROM ScriptLog WHERE [Name] = @Prev)
BEGIN
	RAISERROR('���������� ������ "%s" �� ��� ��������', 20, 1, @Prev) WITH LOG
	RETURN
END

-- �������� �� ��, ��� ������� ������ ��� �� ����������
IF EXISTS(SELECT * FROM ScriptLog WHERE [Name]=@New)
BEGIN
	RAISERROR('������� ������ "%s" ��� ��� ��������', 20, 1, @New) WITH LOG
	RETURN
END

IF OBJECT_ID(N'[dbo].[CashDeskEncashment]', N'U') IS NOT NULL	
BEGIN
	DROP TABLE CashDeskEncashment
END

--***************************************************************************************
-- �������� ����� ������� 
--***************************************************************************************
-- ������ �� ����������
IF OBJECT_ID(N'[dbo].[ShopEncashment]', N'U') IS NULL	
BEGIN
	CREATE TABLE [dbo].ShopEncashment(
		[ShopEncashmentId] [int] IDENTITY(1,1) NOT NULL,
		DateFrom [datetime] NOT NULL,
		ShopId int NULL,
		Summa [float],
		Remains [float],
		[Summa_row_id] [binary](16) NULL,
		[Remains_row_id] [binary](16) NULL,
		CONSTRAINT [PK_ShopEncashment] PRIMARY KEY CLUSTERED 
		(
			[ShopEncashmentId] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]

	ALTER TABLE [dbo].ShopEncashment  WITH CHECK ADD  CONSTRAINT [FK_ShopEncashment_Shop] FOREIGN KEY(ShopId)
	REFERENCES [dbo].Shops ([id])
	
	ALTER TABLE [dbo].ShopEncashment CHECK CONSTRAINT [FK_ShopEncashment_Shop]

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ����������' , @level0type=N'SCHEMA',@level0name=N'dbo', 
			@level1type=N'TABLE',@level1name=N'ShopEncashment', @level2type=N'COLUMN',@level2name=N'Summa'
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� �� � �������� �� ������ ���' , @level0type=N'SCHEMA',@level0name=N'dbo', 
			@level1type=N'TABLE',@level1name=N'ShopEncashment', @level2type=N'COLUMN',@level2name=N'Remains'

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ����������' , @level0type=N'SCHEMA',@level0name=N'dbo',
			@level1type=N'TABLE',@level1name=N'ShopEncashment'
END 

-- ������ �� ����������
IF OBJECT_ID(N'[dbo].[ShopEncashmentNorma]', N'U') IS NULL	
BEGIN
	CREATE TABLE [dbo].ShopEncashmentNorma(
		[ShopEncashmentNormaId] [int] IDENTITY(1,1) NOT NULL,
		DateFrom [datetime] NOT NULL,
		ShopId int NULL,
		Norma [float],
		[row_id] [binary](16) NULL,
		CONSTRAINT [PK_ShopEncashmentNorma] PRIMARY KEY CLUSTERED 
		(
			[ShopEncashmentNormaId] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]

	ALTER TABLE [dbo].ShopEncashmentNorma  WITH CHECK ADD  CONSTRAINT [FK_ShopEncashmentNorma_Shop] FOREIGN KEY(ShopId)
	REFERENCES [dbo].Shops ([id])
	
	ALTER TABLE [dbo].ShopEncashmentNorma CHECK CONSTRAINT [FK_ShopEncashmentNorma_Shop]

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', 
			@level1type=N'TABLE',@level1name=N'ShopEncashmentNorma', @level2type=N'COLUMN',@level2name=N'Norma'

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� �� ����������' , @level0type=N'SCHEMA',@level0name=N'dbo',
			@level1type=N'TABLE',@level1name=N'ShopEncashmentNorma'

END
GO

--***************************************************************************************
-- ����������� �������� ������� ��� ������������ � ��
DECLARE @New varchar(100) = 'X004'
INSERT INTO [ScriptLog]([Name])
VALUES (@New)
GO

--delete from [ScriptLog] where  [Name] = 'X004'
--SELECT * FROM [ScriptLog]







