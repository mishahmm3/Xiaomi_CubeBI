--�������: �����������������.���������������, ��� ������� ��������: AccumRg9039, ����������: ��������
IF OBJECT_ID(N'[dbo].[ProductUnload]', N'U') IS NULL	
BEGIN
	CREATE TABLE [dbo].ProductUnload(
		[ProductUnloadId] [int] IDENTITY(1,1) NOT NULL,
		DateFrom [datetime] NOT NULL,
		WarehouseId int NOT NULL,
		ProductId int NOT NULL,
		CountUnload [float],
		CONSTRAINT [PK_ProductUnload] PRIMARY KEY CLUSTERED 
		(
			DateFrom ASC,
			WarehouseId ASC,
			ProductId ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]

	ALTER TABLE [dbo].ProductUnload  WITH CHECK ADD  CONSTRAINT [FK_ProductUnload_Warehouses] FOREIGN KEY(WarehouseId)
	REFERENCES [dbo].[Warehouses] ([id])
	
	ALTER TABLE [dbo].ProductUnload CHECK CONSTRAINT [FK_ProductUnload_Warehouses]

	ALTER TABLE [dbo].ProductUnload  WITH CHECK ADD  CONSTRAINT [FK_ProductUnload_Product] FOREIGN KEY(ProductId)
	REFERENCES [dbo].[Products] ([id])
	
	ALTER TABLE [dbo].ProductUnload CHECK CONSTRAINT [FK_ProductUnload_Product]

END
GO



