--�������: �����������������.���������������, ��� ������� ��������: AccumRg9039, ����������: ��������
IF OBJECT_ID(N'[dbo].[CashDeskEncashment]', N'U') IS NULL	
BEGIN
	CREATE TABLE [dbo].CashDeskEncashment(
		[CashDeskEncashmentId] [int] IDENTITY(1,1) NOT NULL,
		DateFrom [datetime] NOT NULL,
		CashDeskId int NULL,
		Summa [float],
		[row_id] [binary](16) NULL,
		CONSTRAINT [PK_CashDeskEncashment] PRIMARY KEY CLUSTERED 
		(
			[CashDeskEncashmentId] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]

	ALTER TABLE [dbo].CashDeskEncashment  WITH CHECK ADD  CONSTRAINT [FK_CashDeskEncashment_CashDesk] FOREIGN KEY(CashDeskId)
	REFERENCES [dbo].Cash_desks ([id])
	
	ALTER TABLE [dbo].CashDeskEncashment CHECK CONSTRAINT [FK_CashDeskEncashment_CashDesk]

END
GO



