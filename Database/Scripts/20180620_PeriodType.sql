-- ��� �������
IF OBJECT_ID(N'[dbo].[PeriodType]', N'U') IS NULL	
BEGIN
	CREATE TABLE [dbo].[PeriodType](
		[PeriodTypeId] [int] IDENTITY(1,1) NOT NULL,
		[Code] varchar(100) NOT NULL,
		[Name]  varchar(250) NOT NULL,
		[Description] varchar(250) NULL,
	 CONSTRAINT [PK_Period] PRIMARY KEY CLUSTERED 
	(
		[PeriodTypeId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	 CONSTRAINT [UC_Period_Code] UNIQUE NONCLUSTERED 
	(
		[Code] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	 CONSTRAINT [UC_Period_Name] UNIQUE NONCLUSTERED 
	(
		[Name] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
END


IF OBJECT_ID(N'[dbo].[Parameter]', N'U') IS NULL	
BEGIN
	CREATE TABLE [dbo].[Parameter](
		[ParameterId] [int] IDENTITY(1,1) NOT NULL,
		[Code] varchar(100) NOT NULL,
		[Name] varchar(250) NOT NULL,
		--[ShortName] [nvarchar](100) NULL,
		--[MeasurementUnitId] [int] NOT NULL,
		--[ParameterTypeId] [int] NOT NULL,
		--[IsActive] [dbo].[Bool] NOT NULL,
		--[Description] [dbo].[DescriptionType] NULL,
		--[CustomName] [dbo].[NameType] NULL,
		--[DisplayName]  AS (coalesce([CustomName],[Name])),
	 CONSTRAINT [PK_Parameter] PRIMARY KEY CLUSTERED 
	(
		[ParameterId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	 CONSTRAINT [UC_Parameter_Code] UNIQUE NONCLUSTERED 
	(
		[ParameterId] ASC,
		[Code] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	) ON [PRIMARY]

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Parameter', @level2type=N'COLUMN',@level2name=N'Name'
END
GO

IF OBJECT_ID(N'[dbo].[ParameterPeriodValue]', N'U') IS NULL	
BEGIN
	CREATE TABLE [dbo].[ParameterPeriodValue](
		[ParameterPeriodValueId] [int] IDENTITY(1,1) NOT NULL,
		[ParameterId] [int] NOT NULL,
		[PeriodTypeId] [int] NOT NULL,
		[DateFrom] [datetime] NOT NULL,
		[Value] float NOT NULL,
	 CONSTRAINT [PK_PeriodValueDetail] PRIMARY KEY CLUSTERED 
	(
		[ParameterPeriodValueId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY],
	 CONSTRAINT [UC_PeriodValueDetail_NoDoubles] UNIQUE NONCLUSTERED 
	(
		[ParameterId] ASC,
		[PeriodTypeId] ASC,
		[DateFrom] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[ParameterPeriodValue]  WITH CHECK ADD  CONSTRAINT [FK_PeriodValueDetail_Period] FOREIGN KEY([PeriodTypeId])
	REFERENCES [dbo].[PeriodType] ([PeriodTypeId])

	ALTER TABLE [dbo].[ParameterPeriodValue] CHECK CONSTRAINT [FK_PeriodValueDetail_Period]

	ALTER TABLE [dbo].[ParameterPeriodValue]  WITH CHECK ADD  CONSTRAINT [FK_PeriodValueDetail_Parameter] FOREIGN KEY([ParameterId])
	REFERENCES [dbo].Parameter([ParameterId])

	ALTER TABLE [dbo].[ParameterPeriodValue] CHECK CONSTRAINT [FK_PeriodValueDetail_Parameter]

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ParameterPeriodValue', @level2type=N'COLUMN',@level2name=N'ParameterPeriodValueId'
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ParameterPeriodValue', @level2type=N'COLUMN',@level2name=N'ParameterId'
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������� - ����, �����, ��� ���' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ParameterPeriodValue', @level2type=N'COLUMN',@level2name=N'PeriodTypeId'
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ParameterPeriodValue', @level2type=N'COLUMN',@level2name=N'DateFrom'
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ParameterPeriodValue', @level2type=N'COLUMN',@level2name=N'Value'
END 
GO

-- ������� ������������ �� ���� ������� �������
IF OBJECT_ID(N'[dbo].[TariffCalc]', N'U') IS NOT NULL	
DROP TABLE TariffCalc

CREATE TABLE [dbo].TariffCalc(
	TariffCalcId [int] IDENTITY(1,1) NOT NULL,
	DateFrom datetime, 
	KSeason float, 
	CONSTRAINT [PK_Tariff] PRIMARY KEY CLUSTERED 
	(
	TariffCalcId ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	 CONSTRAINT [UC_TariffCalc_DateFrom] UNIQUE NONCLUSTERED 
	(
		DateFrom ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]

) ON [PRIMARY]

--IF NOT EXISTS ( SELECT * FROM sys.columns WHERE [object_id] = object_id(N'dbo.check_headers') AND [name] = 'CheckStatusId' )
--BEGIN
--	ALTER TABLE dbo.check_headers 
--		ADD CheckStatusId  int NULL

--	ALTER TABLE [dbo].check_headers  WITH CHECK ADD  CONSTRAINT [FK_check_headers_CheckStatus] FOREIGN KEY(CheckStatusId)
--	REFERENCES [dbo].CheckStatus (CheckStatusId)
--END

IF OBJECT_ID(N'coeffs', N'U') IS NOT NULL	
BEGIN
	DROP TABLE coeffs
END
IF OBJECT_ID(N'coeff_types', N'U') IS NOT NULL	

BEGIN
	DROP TABLE coeff_types
END

