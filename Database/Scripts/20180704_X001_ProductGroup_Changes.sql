DECLARE @Prev varchar(100) = 'X000'
DECLARE @New varchar(100) = 'X001'

-- �������� �� ��, ��� ����������� ������ ��� �������� �� ��
IF NOT EXISTS(SELECT * FROM ScriptLog WHERE [Name] = @Prev)
BEGIN
	RAISERROR('���������� ������ "%s" �� ��� ��������', 20, 1, @Prev) WITH LOG
	RETURN
END

-- �������� �� ��, ��� ������� ������ ��� �� ����������
IF EXISTS(SELECT * FROM ScriptLog WHERE [Name]=@New)
BEGIN
	RAISERROR('������� ������ "%s" ��� ��� ��������', 20, 1, @New) WITH LOG
	RETURN
END
DELETE FROM product_group_plans
DELETE FROM product_group_plan_types
DELETE FROM cross_checks
DELETE FROM product_group_products
DELETE FROM product_groups

IF OBJECT_ID(N'[dbo].[product_group_plans]', N'U') IS NOT NULL	
BEGIN
	DROP TABLE product_group_plans
END

IF OBJECT_ID(N'[dbo].[product_group_plan_types]', N'U') IS NOT NULL	
BEGIN
	DROP TABLE product_group_plan_types
END

IF NOT EXISTS(SELECT * FROM  sys.objects WHERE Name = 'UC_product_groups_Name')
BEGIN
	ALTER TABLE [dbo].product_groups ADD  CONSTRAINT UC_product_groups_Name UNIQUE NONCLUSTERED 
	(
		[Name] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END

-- ����������� �������� ������� ��� ������������ � ��
INSERT INTO [ScriptLog]([Name])
VALUES (@New)
GO
