﻿IF not exists (SELECT * FROM Grade)
BEGIN
	DECLARE @First int = 0
	WHILE @First <= 20
	BEGIN
		INSERT INTO Grade(Code, Name)
		SELECT CAST(@First AS varchar(10)), 'Грейд ' + CAST(@First AS varchar(10))
		SET @First = @First + 1
	END
END
