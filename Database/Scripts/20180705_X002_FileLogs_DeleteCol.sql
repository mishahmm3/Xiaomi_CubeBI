DECLARE @Prev varchar(100) = 'X001'
DECLARE @New varchar(100) = 'X002'

-- �������� �� ��, ��� ����������� ������ ��� �������� �� ��
IF NOT EXISTS(SELECT * FROM ScriptLog WHERE [Name] = @Prev)
BEGIN
	RAISERROR('���������� ������ "%s" �� ��� ��������', 20, 1, @Prev) WITH LOG
	RETURN
END

-- �������� �� ��, ��� ������� ������ ��� �� ����������
IF EXISTS(SELECT * FROM ScriptLog WHERE [Name]=@New)
BEGIN
	RAISERROR('������� ������ "%s" ��� ��� ��������', 20, 1, @New) WITH LOG
	RETURN
END
--***************************************************************************************
-- �������� ����� ������� 
--***************************************************************************************

IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'dbo.FK_file_logs_file_types') AND parent_object_id = OBJECT_ID(N'dbo.file_logs'))
BEGIN
	PRINT '  Drop FK constraint dbo.[file_logs].FK_file_logs_file_types';
	ALTER TABLE [dbo].[file_logs] DROP CONSTRAINT [FK_file_logs_file_types]
END

-- Alter column (add/delete)
IF EXISTS ( SELECT * FROM sys.columns
					WHERE  [object_id] = object_id(N'file_logs')
						AND [name] = 'status' )
BEGIN
	ALTER TABLE file_logs
		DROP COLUMN  status
END

-- Alter column (add/delete)
IF EXISTS ( SELECT * FROM sys.columns
					WHERE  [object_id] = object_id(N'file_logs')
						AND [name] = 'File_type_id' )
BEGIN
	ALTER TABLE file_logs
		DROP COLUMN  File_type_id
END

IF OBJECT_ID(N'[dbo].[file_types]', N'U') IS NOT NULL	
BEGIN
	DROP TABLE file_types
END

--***************************************************************************************
-- ����������� �������� ������� ��� ������������ � ��
INSERT INTO [ScriptLog]([Name])
VALUES (@New)
GO




