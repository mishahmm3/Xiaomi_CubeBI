IF OBJECT_ID(N'[dbo].[MarketActionDiscount]', N'U') IS NOT NULL	
BEGIN
	DROP TABLE MarketActionDiscount
END
GO

IF OBJECT_ID(N'[dbo].[market_actions]', N'U') IS NOT NULL	
BEGIN
	DROP TABLE market_actions
END
GO

IF OBJECT_ID(N'[dbo].[MarketAction]', N'U') IS NULL	
BEGIN
	CREATE TABLE [dbo].[MarketAction](
		[MarketActionId] [int] IDENTITY(1,1) NOT NULL,
		[Name] [varchar](255) NULL,
		[DateFrom] [datetime] NULL,
		[DateTo] [datetime] NULL,
		[Description] [varchar](max) NULL,
		[Created_at] [datetime] NULL,
		[Updated_at] [datetime] NULL,
		[Deleted_at] [datetime] NULL,
		[row_id] [binary](16) NULL,
	 CONSTRAINT [PK_MarketAction] PRIMARY KEY CLUSTERED 
	(
		[MarketActionId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO

IF OBJECT_ID(N'[dbo].[MarketActionDiscount]', N'U') IS NULL	
BEGIN
	CREATE TABLE [dbo].[MarketActionDiscount](
		[MarketActionDiscountId] [int] IDENTITY(1,1) NOT NULL,
		[MarketActionId] [int] NOT NULL,
		[discount_id] [int] NOT NULL,
		[shop_id] [int] NOT NULL,
		[created_at] [datetime] NULL,
		[updated_at] [datetime] NULL,
		[deleted_at] [datetime] NULL,
		CONSTRAINT [PK_MarketActionDiscount] PRIMARY KEY CLUSTERED 
		(
			[MarketActionId] ASC,
			[discount_id] ASC,
			[shop_id] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]

	ALTER TABLE [dbo].MarketActionDiscount  WITH CHECK ADD  CONSTRAINT [FK_MarketActionDiscount_discounts] FOREIGN KEY([discount_id])
	REFERENCES [dbo].[discounts] ([id])
	
	ALTER TABLE [dbo].[MarketActionDiscount] CHECK CONSTRAINT [FK_MarketActionDiscount_discounts]

	ALTER TABLE [dbo].[MarketActionDiscount]  WITH CHECK ADD  CONSTRAINT [FK_MarketActionDiscount_MarketAction] FOREIGN KEY(MarketActionId)
	REFERENCES [dbo].[MarketAction] ([MarketActionId])

	ALTER TABLE [dbo].[MarketActionDiscount] CHECK CONSTRAINT [FK_MarketActionDiscount_MarketAction]

	ALTER TABLE [dbo].[MarketActionDiscount]  WITH CHECK ADD  CONSTRAINT [FK_MarketActionDiscount_shops] FOREIGN KEY([shop_id])
	REFERENCES [dbo].[shops] ([id])

	ALTER TABLE [dbo].[MarketActionDiscount] CHECK CONSTRAINT [FK_MarketActionDiscount_shops]
END
GO

--�������: �����������������.�������������, ��� ������� ��������: AccumRg8696, ����������: ��������
IF OBJECT_ID(N'[dbo].[BonusPoint]', N'U') IS NULL	
BEGIN
	CREATE TABLE [dbo].BonusPoint(
		[BonusPointId] [int] IDENTITY(1,1) NOT NULL,
		DateFrom [datetime] NOT NULL,
		CardId int NOT NULL,
		CountCharged [float],
		CountOff [float],
		CONSTRAINT [PK_BonusPoint] PRIMARY KEY CLUSTERED 
		(
			DateFrom ASC,
			CardId ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]

	ALTER TABLE [dbo].BonusPoint  WITH CHECK ADD  CONSTRAINT [FK_BonusPoint_cards] FOREIGN KEY(CardId)
	REFERENCES [dbo].[cards] ([id])
	
	ALTER TABLE [dbo].BonusPoint CHECK CONSTRAINT [FK_BonusPoint_cards]
END
GO

