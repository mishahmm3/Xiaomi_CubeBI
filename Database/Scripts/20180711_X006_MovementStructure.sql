DECLARE @Prev varchar(100) = 'X005'
DECLARE @New varchar(100) = 'X006'

-- �������� �� ��, ��� ����������� ������ ��� �������� �� ��
IF NOT EXISTS(SELECT * FROM ScriptLog WHERE [Name] = @Prev)
BEGIN
	RAISERROR('���������� ������ "%s" �� ��� ��������', 20, 1, @Prev) WITH LOG
	RETURN
END

-- �������� �� ��, ��� ������� ������ ��� �� ����������
IF EXISTS(SELECT * FROM ScriptLog WHERE [Name]=@New)
BEGIN
	RAISERROR('������� ������ "%s" ��� ��� ��������', 20, 1, @New) WITH LOG
	RETURN
END

--***************************************************************************************
-- �������� ����� ������� 
--***************************************************************************************
--drop table MovementDoc
IF OBJECT_ID(N'[dbo].[MovementDoc]', N'U') IS NULL	
BEGIN
	CREATE TABLE [dbo].MovementDoc(
		MovementDocId [int] IDENTITY(1,1) NOT NULL CONSTRAINT [PK_MovementDoc] PRIMARY KEY CLUSTERED,
		DateFrom datetime, 
		DocNumber varchar(100) NULL,
		IsPosted bit NOT NULL,
		WarehouseFromId int,
		WarehouseToId int,
		[row_id] [binary](16) NULL,
		CONSTRAINT [UC_MovementDoc_DocNumber] UNIQUE NONCLUSTERED 
		(
			DateFrom,
			DocNumber,
			IsPosted
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo',
		@level1type=N'TABLE',@level1name=N'MovementDoc',
		@value=N'��������-��������� ��� ����������� �������'

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo', 
		@level1type=N'TABLE',@level1name=N'MovementDoc',
		@level2type=N'COLUMN',@level2name=N'DocNumber',
		@value=N'����� ���������'
		 
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo', 
		@level1type=N'TABLE',@level1name=N'MovementDoc',
		@level2type=N'COLUMN',@level2name=N'WarehouseFromId',
		@value=N'C����-�����������'

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo', 
		@level1type=N'TABLE',@level1name=N'MovementDoc',
		@level2type=N'COLUMN',@level2name=N'WarehouseToId',
		@value=N'C����-����������'
END

IF OBJECT_ID(N'[dbo].[MovementDocProduct]', N'U') IS NULL	
BEGIN
	CREATE TABLE [dbo].MovementDocProduct(
		MovementDocProductId [int] IDENTITY(1,1) NOT NULL CONSTRAINT [PK_MovementDocProduct] PRIMARY KEY CLUSTERED,
		MovementDocId int NOT NULL, 
		ProductId int,
		ProductCount float,
		CONSTRAINT [UC_MovementDocProduct_Main] UNIQUE NONCLUSTERED 
		(
			MovementDocId,
			ProductId
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo',
		@level1type=N'TABLE',@level1name=N'MovementDocProduct',
		@value=N'������ ���������-���������: ������ �� �������'

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo', 
		@level1type=N'TABLE',@level1name=N'MovementDocProduct',
		@level2type=N'COLUMN',@level2name=N'MovementDocId',
		@value=N'������ �� ��������'
		 
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo', 
		@level1type=N'TABLE',@level1name=N'MovementDocProduct',
		@level2type=N'COLUMN',@level2name=N'ProductId',
		@value=N'�����'

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo', 
		@level1type=N'TABLE',@level1name=N'MovementDocProduct',
		@level2type=N'COLUMN',@level2name=N'ProductCount',
		@value=N'���-�� ������'

	ALTER TABLE [dbo].MovementDocProduct  WITH CHECK ADD  CONSTRAINT [FK_MovementDocProduct_MovementDoc] FOREIGN KEY(MovementDocId)
	REFERENCES [dbo].MovementDoc ([MovementDocid])
	
	ALTER TABLE [dbo].MovementDocProduct CHECK CONSTRAINT [FK_MovementDocProduct_MovementDoc]

	ALTER TABLE [dbo].MovementDocProduct  WITH CHECK ADD  CONSTRAINT [FK_MovementDocProduct_Product] FOREIGN KEY(ProductId)
	REFERENCES [dbo].Products (id)
	
	ALTER TABLE [dbo].MovementDocProduct CHECK CONSTRAINT [FK_MovementDocProduct_Product]
END

---------------------------------------------------------
--drop table MovementDocFromTo
IF OBJECT_ID(N'[dbo].[MovementDocFromTo]', N'U') IS NULL	
BEGIN
	CREATE TABLE [dbo].MovementDocFromTo(
		MovementDocFromToId [int] IDENTITY(1,1) NOT NULL CONSTRAINT [PK_MovementDocFromTo] PRIMARY KEY CLUSTERED,
		MovementDocId int NOT NULL, 
		ProductId int,
		FromDateFrom datetime, 
		ToDateFrom datetime,
		FromPlanCount float,
		FromFactCount float,
		ToPlanCount float,
		ToFactCount float,
		CONSTRAINT [UC_MovementDocFromTo_Main] UNIQUE NONCLUSTERED 
		(
			MovementDocId,
			ProductId
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo',
		@level1type=N'TABLE',@level1name=N'MovementDocFromTo',
		@value=N'������ ���������-���������: ����� � ����� �������� � ��������'

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo', 
		@level1type=N'TABLE',@level1name=N'MovementDocFromTo',
		@level2type=N'COLUMN',@level2name=N'MovementDocId',
		@value=N'������ �� ��������'
		 
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo', 
		@level1type=N'TABLE',@level1name=N'MovementDocFromTo',
		@level2type=N'COLUMN',@level2name=N'ProductId',
		@value=N'�����'

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo', 
		@level1type=N'TABLE',@level1name=N'MovementDocFromTo',
		@level2type=N'COLUMN',@level2name=N'FromPlanCount',
		@value=N'�������� �� ������� (����)'

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo', 
		@level1type=N'TABLE',@level1name=N'MovementDocFromTo',
		@level2type=N'COLUMN',@level2name=N'FromFactCount',
		@value=N'�������� �� ������� (����)'

	ALTER TABLE [dbo].MovementDocFromTo  WITH CHECK ADD  CONSTRAINT [FK_MovementDocFromTo_MovementDoc] FOREIGN KEY(MovementDocId)
	REFERENCES [dbo].MovementDoc ([MovementDocid])
	
	ALTER TABLE [dbo].MovementDocFromTo CHECK CONSTRAINT [FK_MovementDocFromTo_MovementDoc]

	ALTER TABLE [dbo].MovementDocFromTo  WITH CHECK ADD  CONSTRAINT [FK_MovementDocFromTo_Product] FOREIGN KEY(ProductId)
	REFERENCES [dbo].Products (id)
	
	ALTER TABLE [dbo].MovementDocFromTo CHECK CONSTRAINT [FK_MovementDocFromTo_Product]
END
GO
--***************************************************************************************
-- ����������� �������� ������� ��� ������������ � ��
--***************************************************************************************
GO
DECLARE @New varchar(100) = 'X006'
INSERT INTO [ScriptLog]([Name])
VALUES (@New)
GO

--delete from [ScriptLog] where  [Name] = 'X004'
--SELECT * FROM [ScriptLog]








