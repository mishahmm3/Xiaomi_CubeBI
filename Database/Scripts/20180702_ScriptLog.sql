DECLARE @New varchar(100) = 'X000'

IF OBJECT_ID(N'[dbo].[ScriptLog]', N'U') IS NULL	
BEGIN

	CREATE TABLE [ScriptLog](
		[ScriptId] [int] IDENTITY(1,1) NOT NULL,
		[Name] varchar(250) NOT NULL,
		[Created] [datetime] NOT NULL,
		[CreatedBy] varchar(250)  NOT NULL,
	 CONSTRAINT [PK_Script_Data] PRIMARY KEY CLUSTERED 
	(
		[ScriptId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	 CONSTRAINT [UC_Script_Name] UNIQUE NONCLUSTERED 
	(
		[Name] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [ScriptLog] ADD  CONSTRAINT [getdate100]  DEFAULT (getdate()) FOR [Created]

	ALTER TABLE [ScriptLog]ADD  CONSTRAINT [CurrentUserLogin4]  DEFAULT (suser_sname()) FOR [CreatedBy]

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������� �������' , @level0type=N'SCHEMA',@level0name=N'dbo', 
		@level1type=N'TABLE',@level1name=N'ScriptLog', @level2type=N'COLUMN',@level2name=N'ScriptId'

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� (�����) �������' , @level0type=N'SCHEMA',@level0name=N'dbo', 
		@level1type=N'TABLE',@level1name=N'ScriptLog', @level2type=N'COLUMN',@level2name=N'Name'

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ������ ���������� � �������' , @level0type=N'SCHEMA',@level0name=N'dbo', 
		@level1type=N'TABLE',@level1name=N'ScriptLog', @level2type=N'COLUMN',@level2name=N'Created'

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', 
		@level1type=N'TABLE',@level1name=N'ScriptLog', @level2type=N'COLUMN',@level2name=N'CreatedBy'

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������, ������� ��� �������� �� ��' , @level0type=N'SCHEMA',@level0name=N'dbo',
		@level1type=N'TABLE',@level1name=N'ScriptLog'
END


-- ����������� �������� ������� ��� ������������ � ��
INSERT INTO [ScriptLog]([Name])
VALUES (@New)
GO